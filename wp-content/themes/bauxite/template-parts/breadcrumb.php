<?php

$breadcrumb_common_meta 		= get_post_meta( get_the_id (), 'common_heading', true );

if (isset($breadcrumb_common_meta['common_breadcrumb_enable'])) {
	$common_breadcrumb_enable       = $breadcrumb_common_meta['common_breadcrumb_enable'];


	$common_breadcrumb_background   = $breadcrumb_common_meta['common_breadcrumb_background'];
	$common_breadcrumb_background             = wp_get_attachment_image_src( $common_breadcrumb_background , 'full' );

	if ($common_breadcrumb_enable == 1) {?>

	<section class="page-title-box" style="background: url(<?php echo $common_breadcrumb_background[0];?>) no-repeat; background-size: cover;">
		<div class="container">
			<div class="row">
				<div class="col-12">

					<ul>
						<?php echo get_breadcrumb(); ?>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!-- .page-title-box -->
	<?php }
}
?>
