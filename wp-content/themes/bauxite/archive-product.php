<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package RWS_AEP
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="archive-page-section">
				<div class="container">
					<div id="content" class="site-content">
						<section class="news-section">
							<div class="container">

								<div class="row">

									<!-- #secondary -->
									<div id="primary" class="col-12 content-area">
										<div class="article-list">
											<div class="row">
												<?php
												if ( have_posts() ) : ?>
												<?php
												/* Start the Loop */
												while ( have_posts() ) : the_post();?>
												<div class="col-3">
													<div class="site-news-item">
														<figure class="img-overlay">
															<img src="<?php 
															if (has_post_thumbnail( )) {
																the_post_thumbnail_url('news-post-size');
															}else{
																echo "http://via.placeholder.com/360x358" ;
															}
															?>" alt="sample42" />
															<i class="fa fa-compress" aria-hidden="true"></i>
															<a href="#"></a>
														</figure>
														<div class="post">

															<!-- .post-meta -->
															<header class="entry-header">
																<h4 class="entry-title"><a href="<?php echo the_permalink(); ?>">
																	<?php the_title(); ?>
																</a></h4>
															</header>
															<div class="entry-content">
																<p>		
																	<?php the_excerpt(); ?>
																</p>
															</div>
														</div>
														<!-- .post -->
													</div>
													<!-- .site-news-item -->
												</div>
											<?php endwhile;?>

										</div>
										<!-- .article-list -->
									</div>


									<?php rws_custom_pagination();?>
									<?php wp_reset_postdata(); ?>

									<!-- .pagination -->
								</div>
								<!-- #primary -->
							</div>
						</div>
					</section>
					<!-- .news-section -->

					<!-- . signup-section -->
				</div>
			</div>
		</section>
		<!-- #content -->
	<?php else: ?>
		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>	
	<?php 
	$enable_news_letter				 	= cs_get_option( 'enable_news_letter' );
	if( 1 == $enable_news_letter ):
		$rws_np_news_letter_section 	= get_post_meta( 98, 'rws_np_news_letter_section', true );
	if (isset($rws_np_news_letter_section['np_news_letter_enable'])) {
		$np_news_letter_enable 			= $rws_np_news_letter_section['np_news_letter_enable'];
		?>
		<?php if( 1 == $np_news_letter_enable ): ?>
			<!-- .site-news -->
			<section class="signup-section" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/images/img/subscribe-bg.jpg) no-repeat; background-size: cover">
				<div class="container">
					<div class="signup-content">
						<header class="section-header">
							<h2 class="section-title">email newsletter</h2>
						</header>
						<?php
						$news_letter_form = cs_get_option( 'news_letter_form' );
						echo do_shortcode( $news_letter_form) ?>
					</div>
				</div>
			</section>
			<!-- . signup-section -->
		<?php endif; 
	}?>
<?php endif; ?>

</main>
</div>
</div>
<?php
get_footer();