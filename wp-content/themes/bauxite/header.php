<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> <!-- Required meta tags -->

	<title>

		<?php $title = get_bloginfo('title');
		echo $title;
		if (empty($title)) {
			echo "Vimetco";				
		}
		?>

	</title>
	<?php wp_head();?>

</head>
<body <?php body_class();?>>
	<div id="page" class="site">
		<header id="masthead" class="site-banner">
			<div class="header-top">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<header class="entry-header">
								<h6 class="entry-title">

									<?php $title = get_bloginfo('title');
									echo $title;
									if (empty($title)) {
										echo "Vimetco";				
									}								
									?>
								</h6>
							</header>
						</div>
					</div>
				</div>
			</div>
			<!-- .header-top -->
			<div class="header-group">
				<div class="container">
					<div class="row">
						<div class="col-4">
							<div class="site-branding">
								<a href="<?php echo home_url( "/"); ?>">
									<img src="<?php
									$custom_logo_id = get_theme_mod( 'custom_logo' );
									$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
									echo $image[0];
									?>" alt="site-logo">
								</a>
							</div>
							<!-- .site-branding -->
						</div>
						<div class="col-4">
							<div class="header-item">
								<?php $rws_header_address_enable 	= cs_get_option( 'rws_header_address_enable' );
								$site_header_address1 				= cs_get_option( 'site_header_address1' );
								$site_header_address2 				= cs_get_option( 'site_header_address2' );
								$site_header_address_image 			= cs_get_option( 'site_header_address_image' );
								$header_image 						= wp_get_attachment_image_src( $site_header_address_image , 'full' );
								if( 1 == $rws_header_address_enable ): ?>
								<figure>
									<img src="<?php
									if (!empty($header_image)) {?>

									<?php
									echo $header_image[0]; ?> ,
									<?php
								}
								?>" alt="location-icon">
								<p>
									<?php
									if (!empty($site_header_address1)) {?>

									<?php
									echo $site_header_address1; ?>

									<?php
								}
								?>
								<?php
								if (!empty($site_header_address2)) {
									?>

									<?php
									echo ", <br> " . $site_header_address2; ?> 

									<?php
								}
								?>
							</p>

						<?php endif; ?>
					</figure>

				</div>
				<!-- .header-item -->
			</div>
			<div class="col-2">
				<div class="header-item">

					<?php $rws_header_phone_enable 	= cs_get_option( 'rws_header_phone_enable' );
					$site_header_phone1 			= cs_get_option( 'site_header_phone1' );
					$site_header_phone2				= cs_get_option( 'site_header_phone2' );
					$site_header_phone_image 		= cs_get_option( 'site_header_phone_image' );
					$header_phone_image 			= wp_get_attachment_image_src( $site_header_phone_image , 'full' );
					if( 1 == $rws_header_phone_enable ): ?>
					<figure>
						<img src="<?php
						if (!empty($header_phone_image)) {
							echo $header_phone_image[0];
						}
						?>
						" alt="call-icon">
						<a href="tel:#">

							<p>
								<?php
								if (!empty($site_header_phone1)) {
									echo $site_header_phone1;
								}
								?>
							</p>
							<p>
								<?php
								if (!empty($site_header_phone2)) {
									echo $site_header_phone2;
								}
								?>
							</p>
						</a>


					<?php endif; ?>
				</figure>

			</div>
			<!-- .header-item -->
		</div>
		<div class="col-2">
			<div class="header-item">

				<?php $rws_header_email_enable	= cs_get_option( 'rws_header_email_enable' );
				$site_header_email 				= cs_get_option( 'site_header_email' );
				$site_header_email_image 		= cs_get_option( 'site_header_email_image' );
				$site_header_email_image 		= wp_get_attachment_image_src( $site_header_email_image , 'full' );
				if( 1 == $rws_header_email_enable ): ?>
				<figure>
					<img src="<?php
					if (!empty($site_header_email_image)) {
						echo $site_header_email_image[0];
					}
					?>" alt="envelop-icon">
					<a href="mailto:#">
						<?php
						if (!empty($site_header_email)) {
							echo $site_header_email;
						}
						?>
					</a>


				<?php endif; ?>
			</figure>


		</div>
		<!-- .header-item -->
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="nav-border">
			<div id="menu" class="default full-nav">
				<nav id="site-navigation" class="main-navigation">

					<?php
					if (has_nav_menu('primary')) {

						wp_nav_menu(array(
							'theme_location' => 'primary',
							'container' => false,
							));
					}
					?>
				</nav>
			</div>
		</div>

		<!-- #site-navigation -->
	</div>
</div>
</div>
</div>
<!-- .header-group -->
</header> <!-- #masthead -->
<section class="site-banner">
	<div class="owl-carousel owl-theme">
		<?php
		$banner_common_meta 		= get_post_meta( get_the_id (), 'common_heading', true );
		if (isset($banner_common_meta['banner_slider_enable'])):
			$banner_slider_enable 	= $banner_common_meta['banner_slider_enable'];

		if( 1 						== $banner_slider_enable ):
			$slider_images 			= $banner_common_meta['slider_images'];

		if( !empty( $slider_images )):
			foreach ($slider_images as $value):
				// var_dump($value);
				$slider_img_url 	= $value['slider_img'];
			$slider_img_url 		= wp_get_attachment_image_src( $slider_img_url , 'full' );
			$slider 				= $slider_img_url[0];
			$slider_desc 			= $value['slider_desc'];
			?>
			<div class="item" style="background: url(<?php echo $slider; ?>)no-repeat; background-size: cover;">
				<div class="container">
					<div class="row">
						<div class="col-6"></div>
						<div class="col-6">
							<div class="banner-content">
								<header class="entry-header">
									<h3 class="entry-title"> <?php echo $slider_desc; ?></h3>
								</header>
								<?php
								if (isset($value['common_slider_button_enable'])) {
									$common_slider_button_enable 	= $value['common_slider_button_enable'];
									if( $common_slider_button_enable== 1 ):
										$slider_button_url 			= $value['slider_button_url'];
									$slider_button_label 			= $value['slider_button_label'];?>
									<a href="<?php echo $slider_button_url; ?>" class="btn link-btn"><?php echo ( $slider_button_label) ? $slider_button_label : 'View'; ?> <i class="fa fa-arrow-right"></i>
									</a>
								<?php endif;
							} ?>

						</div>
						<!-- .banner-content -->
					</div>
				</div>
			</div>
		</div>
	<?php endforeach;
	endif;
	endif;
	endif; ?>
	<!-- .item -->
</section><!-- .owl-carousel -->
<?php
if (!is_front_page()) {
	get_template_part( 'template-parts/breadcrumb' );
}
?>
