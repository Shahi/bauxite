<?php 
/*
Template Name: News Template
*/

get_header(); 
$rws_np_news_letter_section 	= get_post_meta( get_the_id (), 'rws_np_news_letter_section', true );

?>

<div id="content" class="site-content">
	<section class="news-section">
		<div class="container">
			<div class="row">
				<div id="secondary" class="col-3 side-bar">
					<aside class="news-year">

						<?php dynamic_sidebar( 'news_archive' );?>

					</aside>
				</div>
				<!-- #secondary -->
				<div id="primary" class="col-9 content-area">
					<main id="main" class="site-main" role="main">
						<div class="article-list">
							<div class="row">

								<?php
								$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								$args = new WP_Query( array(
									'post_type' => 'post',
									'paged' => $paged,
									'cat' => -5,
									)							          
								);
								if ( $args->have_posts() ) :
									/* Start the Loop */
								while ( $args->have_posts() ) : $args->the_post();?>

								<div class="col-4">
									<div class="site-news-item">
										<a href="<?php echo the_permalink(); ?>">

											<figure class="img-overlay">
												<img src="<?php 
												if (has_post_thumbnail( )) {
													the_post_thumbnail_url('news-post-size');
												}else{
													echo "http://via.placeholder.com/360x358" ;
												}
												?>" alt="sample42" />
												<i class="fa fa-compress" aria-hidden="true"></i>

											</figure>
										</a>
										<div class="post">
											<div class="post-meta">
												<div class="date">

													<?php echo get_the_date( 'd M Y' ) ?>
													
												</div>
											</div>
											<!-- .post-meta -->
											<header class="entry-header">
												<h4 class="entry-title">
													<a href="<?php echo the_permalink(); ?>">

														<?php the_title(); ?>

													</a></h4>
												</header>
											</div>
											<!-- .post -->

										</div>
										<!-- .site-news-item -->
									</div>

								<?php endwhile;?>

							</div>
							<!-- .article-list -->
						</div>

						<?php custom_pagination($args->max_num_pages, "", $paged); ?>
						<?php wp_reset_postdata(); ?>
					<?php else: ?>

						<div>
							<p>

								<?php _e('Sorry, no posts matched your criteria.'); ?>

							</p>
						</div>

					<?php endif; ?>								<!-- .pagination -->
				</main>
			</div>
			<!-- #primary -->
		</div>
	</div>
</section>
<!-- .news-section -->

<!-- . signup-section -->
</div>
<!-- #content -->


<?php 
$enable_news_letter				 	= cs_get_option( 'enable_news_letter' );
if( 1 == $enable_news_letter ):
	if (isset($rws_np_news_letter_section['np_news_letter_enable'])) {
		$np_news_letter_enable 			= $rws_np_news_letter_section['np_news_letter_enable'];
		if( 1 == $np_news_letter_enable ): ?>
		<!-- .site-news -->
		<section class="signup-section" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/images/img/subscribe-bg.jpg) no-repeat; background-size: cover">
			<div class="container">
				<div class="signup-content">
					<header class="section-header">
						<h2 class="section-title">Email newsletter</h2>
					</header>

					<?php
					$news_letter_form = cs_get_option( 'news_letter_form' );
					echo do_shortcode( $news_letter_form) ?>

				</div>
			</div>
		</section>
		<!-- . signup-section -->
	<?php endif; 
}?>
<?php endif; 

?>

<?php
get_footer();