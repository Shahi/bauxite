<?php 
/*
Template Name: Policy Template
*/
get_header();
$policy_meta 		= get_post_meta( get_the_id (), 'rws_policy_section', true );
$pp_policy_title = $policy_meta['pp_policy_title'];
$pp_policy_content = $policy_meta['pp_policy_content'];
?>
<div id="content" class="site-content">

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="policy-section">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</section>
		</main>
		<!-- #main -->
	</div>
	<!-- #primary -->
	<section class="signup-section" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/images/img/subscribe-bg.jpg) no-repeat; background-size: cover">
		<div class="container">
			<div class="signup-content">
				<header class="section-header">
					<h2 class="section-title">email newslatter</h2>
				</header>
				<!-- MailChimp for WordPress v4.1.14 - https://wordpress.org/plugins/mailchimp-for-wp/ -->
				<form class="mc4wp-form">
					<div class="mc4wp-form-fields">
						<p class="mc4wp-email-box">
							<input value="" placeholder="Email Address" name="EMAIL" type="email">
						</p>
						<p class="signup-button">
							<input value="SUBSCRIBE" name="" type="submit">
						</p>
						<label style="display: none !important;">Leave this field empty if you're human: 
							<input name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" type="text">
						</label>
						<input name="_mc4wp_timestamp" value="1516773487" type="hidden">
						<input name="_mc4wp_form_id" value="151" type="hidden">
						<input name="_mc4wp_form_element_id" value="mc4wp-form-1" type="hidden">
					</div>
					<div class="mc4wp-response"></div>
				</form>
				<!-- / MailChimp for WordPress Plugin -->
			</div>
		</div>
	</section>
	<!-- . signup-section -->
</div>

<?php get_footer(  );?>