<?php 
/*
Template Name: Operation-Mining Template
*/
get_header();
global $rws_options;
$rws_operation_section              = get_post_meta( get_the_id (), 'rws_operation_section', true );

$op_mining_lease_title              = $rws_operation_section['op_mining_lease_title'];
$op_mining_lease_content            = $rws_operation_section['op_mining_lease_content'];
$op_mining_lease_button_enable      = $rws_operation_section['op_mining_lease_button_enable'];
$op_mining_lease_label              = $rws_operation_section['op_mining_lease_label'];
$op_mining_lease_url                = $rws_operation_section['op_mining_lease_url'];
$op_mining_lease_icon               = $rws_operation_section['op_mining_lease_icon'];


$op_production_chain_title          = $rws_operation_section['op_production_chain_title'];
$op_production_chain_content        = $rws_operation_section['op_production_chain_content'];

$op_exploration_title               = $rws_operation_section['op_exploration_title'];
$op_exploration_content             = $rws_operation_section['op_exploration_content'];
$op_exploration_image_enable        = $rws_operation_section['op_exploration_image_enable'];
$op_exploration_images               = $rws_operation_section['op_exploration_image'];

// var_dump($op_exploration_images);

$op_exploration_button_enable       = $rws_operation_section['op_exploration_button_enable'];
$op_exploration_label               = $rws_operation_section['op_exploration_label'];
$op_exploration_url                 = $rws_operation_section['op_exploration_url'];
$op_exploration_icon                = $rws_operation_section['op_exploration_icon'];


$op_open_pit_mining_title           = $rws_operation_section['op_open_pit_mining_title'];
$op_open_pit_mining_content         = $rws_operation_section['op_open_pit_mining_content'];
$op_open_pit_mining_image_enable    = $rws_operation_section['op_open_pit_mining_image_enable'];
$op_open_pit_mining_images          = $rws_operation_section['op_open_pit_mining_image'];

$op_open_pit_mining_button_enable   = $rws_operation_section['op_open_pit_mining_button_enable'];
$op_open_pit_mining_label           = $rws_operation_section['op_open_pit_mining_label'];
$op_open_pit_mining_url             = $rws_operation_section['op_open_pit_mining_url'];
$op_open_pit_mining_icon            = $rws_operation_section['op_open_pit_mining_icon'];


$op_benefication_title              = $rws_operation_section['op_benefication_title'];
$op_benefication_content            = $rws_operation_section['op_benefication_content'];
$op_benefication_image_enable       = $rws_operation_section['op_benefication_image_enable'];
$op_benefication_images             = $rws_operation_section['op_benefication_image'];

$op_benefication_button_enable      = $rws_operation_section['op_benefication_button_enable'];
$op_benefication_label              = $rws_operation_section['op_benefication_label'];
$op_benefication_url                = $rws_operation_section['op_benefication_url'];
$op_benefication_icon               = $rws_operation_section['op_benefication_icon'];


$op_haulage_title                   = $rws_operation_section['op_haulage_title'];
$op_haulage_content                 = $rws_operation_section['op_haulage_content'];
$op_haulage_image_enable            = $rws_operation_section['op_haulage_image_enable'];
$op_haulage_images                  = $rws_operation_section['op_haulage_image'];

$op_haulage_button_enable           = $rws_operation_section['op_haulage_button_enable'];
$op_haulage_label                   = $rws_operation_section['op_haulage_label'];
$op_haulage_url                     = $rws_operation_section['op_haulage_url'];
$op_haulage_icon                    = $rws_operation_section['op_haulage_icon'];


$op_stock_piling_title              = $rws_operation_section['op_stock_piling_title'];
$op_stock_piling_content            = $rws_operation_section['op_stock_piling_content'];
$op_stock_piling_image_enable       = $rws_operation_section['op_stock_piling_image_enable'];
$op_stock_piling_images             = $rws_operation_section['op_stock_piling_image'];

$op_stock_piling_button_enable      = $rws_operation_section['op_stock_piling_button_enable'];
$op_stock_piling_label              = $rws_operation_section['op_stock_piling_label'];
$op_stock_piling_url                = $rws_operation_section['op_stock_piling_url'];
$op_stock_piling_icon               = $rws_operation_section['op_stock_piling_icon'];


$op_drying_title                    = $rws_operation_section['op_drying_title'];
$op_drying_content                  = $rws_operation_section['op_drying_content'];
$op_drying_image_enable             = $rws_operation_section['op_drying_image_enable'];
$op_drying_images                   = $rws_operation_section['op_drying_image'];

$op_drying_button_enable            = $rws_operation_section['op_drying_button_enable'];
$op_drying_label                    = $rws_operation_section['op_drying_label'];
$op_drying_url                      = $rws_operation_section['op_drying_url'];
$op_drying_icon                     = $rws_operation_section['op_drying_icon'];


$op_transhiping_title               = $rws_operation_section['op_transhiping_title'];
$op_transhiping_content             = $rws_operation_section['op_transhiping_content'];
$op_transhiping_image_enable        = $rws_operation_section['op_transhiping_image_enable'];
$op_transhiping_images              = $rws_operation_section['op_transhiping_image'];

$op_transhiping_button_enable       = $rws_operation_section['op_transhiping_button_enable'];
$op_transhiping_label               = $rws_operation_section['op_transhiping_label'];
$op_transhiping_url                 = $rws_operation_section['op_transhiping_url'];
$op_transhiping_icon                = $rws_operation_section['op_transhiping_icon'];



$op_production_title                = $rws_operation_section['op_production_title'];
$op_production_content              = $rws_operation_section['op_production_content'];
$op_production_button_enable        = $rws_operation_section['op_production_button_enable'];
$op_production_label                = $rws_operation_section['op_production_label'];
$op_production_url                  = $rws_operation_section['op_production_url'];
$op_production_icon                 = $rws_operation_section['op_production_icon'];



$op_resource_and_reserve_title          = $rws_operation_section['op_resource_and_reserve_title'];
$op_resource_and_reserve_content        = $rws_operation_section['op_resource_and_reserve_content'];
$op_resource_and_reserve_button_enable  = $rws_operation_section['op_resource_and_reserve_button_enable'];
$op_resource_and_reserve_label          = $rws_operation_section['op_resource_and_reserve_label'];
$op_resource_and_reserve_url            = $rws_operation_section['op_resource_and_reserve_url'];
$op_resource_and_reserve_icon           = $rws_operation_section['op_resource_and_reserve_icon'];

$rws_operation_mining_news_section      = get_post_meta( get_the_id (), 'rws_operation_mining_news_section', true );
$op_mining_news_letter_enable           = $rws_operation_mining_news_section['op_mining_news_letter_enable'];
the_post();

?>


<div id="content" class="site-content">
  <div id="primary" class="content-area">
    <main id="main" class="site-main">
      <section class="operation-sub-part">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <header class="entry-header">
                <h3 class="entry-title">

                  <?php the_title(); ?>
                  <?php the_content(); ?>
                  


                </h3>
              </header>
              <ul>
                <li>

                  <?php echo $op_mining_lease_title; ?>

                </li>
                <li>     

                 <?php echo $op_production_chain_title ?>

               </li>
               <li>

                 <?php echo $op_production_title ?>
                 
               </li>
               <li>

                 <?php echo $op_resource_and_reserve_title ?>
                 
               </li>
             </ul>
             <div class="sub-part-content">
              <div id="mining">
                <header class="entry-header">
                  <h5 class="entry-title">

                    <?php echo $op_mining_lease_title; ?>
                    
                  </h5>
                </header>
                <div class="entry-content">

                  <?php echo wpautop($op_mining_lease_content); ?>

                  
                </div>

                <?php if (1 == $op_mining_lease_button_enable) { ?>

                <div class="pdf-link">

                  <?php if (!empty($op_mining_lease_icon)) {?>

                  <i class="<?php echo $op_mining_lease_icon ?>"></i>

                  <?php }else{?>

                  <img src="http://via.placeholder.com/32x32" alt="pdf">

                  <?php }?>

                  <a href="<?php echo $op_mining_lease_url; ?>">

                    <?php       if (!empty($op_mining_lease_label)){
                      echo $op_mining_lease_label; 
                    }else{
                      echo "Link";
                    } ?>

                  </a>
                </div>

                <?php }?>

              </div>
              <!-- #mining -->
              <div id="production-chain" class="production-chain-tab">
                <header class="entry-header">
                  <h5 class="entry-title">

                    <?php echo $op_production_chain_title ?>

                  </h5>
                </header>
                <div class="entry-content">

                  <?php echo wpautop($op_production_chain_content); ?>

                </div>
                <div id="parentVerticalTab">
                  <ul class="resp-tabs-list hor_1">

                    <li> <span>1</span> <?php echo $op_exploration_title ?></li>
                    <li> <span>2</span> <?php echo $op_open_pit_mining_title ?></li>
                    <li> <span>3</span> <?php echo $op_benefication_title ?></li>
                    <li> <span>4</span> <?php echo $op_haulage_title ?></li>
                    <li> <span>5</span><?php echo $op_stock_piling_title ?></li>
                    <li> <span>6</span> <?php echo $op_drying_title ?></li>
                    <li> <span>7</span> <?php echo $op_transhiping_title ?></li>

                  </ul>
                  <div class="resp-tabs-container hor_1">
                    <div class="production-item">
                      <header class="entry-header">
                        <h5 class="entry-title"> 1) 

                          <?php echo $op_exploration_title ?>: 

                        </h5>
                      </header>
                      <div class="entry-content">

                        <?php echo wpautop($op_exploration_content); ?>

                      </div>
                      <?php if (1 == $op_exploration_button_enable) { ?>

                      <div class="pdf-link">

                        <?php if (!empty($op_exploration_icon)) {?>

                        <i class="<?php echo $op_exploration_icon ?>"></i>

                        <?php }else{?>

                        <img src="http://via.placeholder.com/32x32" alt="pdf">

                        <?php }?>

                        <a href="<?php echo $op_exploration_url; ?>">

                          <?php if (!empty($op_exploration_label)){
                            echo $op_exploration_label; 
                          }else{
                            echo "Link";
                          } ?>

                        </a>
                      </div>

                      <?php }?>

                      <figure>

                        <?php if( !empty( $op_exploration_images )):
                        $op_exploration_images = explode(",", $op_exploration_images);
                        foreach ($op_exploration_images as $value):
                          $images = $value;
                        $op_exploration_image     = wp_get_attachment_image_src( $images , 'full' );?>

                        <img src="<?php echo $op_exploration_image[0] ?>" alt="">

                      <?php endforeach;
                      endif; ?>

                    </figure>
                  </div>
                  <!-- .production-item -->
                  <div class="production-item">
                    <header class="entry-header">
                      <h5 class="entry-title"> 2) 

                        <?php echo $op_open_pit_mining_title ?>: 

                      </h5>
                    </header>
                    <div class="entry-content">

                      <?php echo wpautop($op_open_pit_mining_content); ?> 

                    </div>

                    <?php if (1 == $op_open_pit_mining_button_enable) { ?>

                    <div class="pdf-link">

                      <?php if (!empty($op_open_pit_mining_icon)) {?>

                      <i class="<?php echo $op_open_pit_mining_icon ?>"></i>

                      <?php }else{?>
                      <img src="http://via.placeholder.com/32x32" alt="pdf">
                      <?php }?>

                      <a href="<?php echo $op_open_pit_mining_url; ?>">

                        <?php if (!empty($op_open_pit_mining_label)){
                          echo $op_open_pit_mining_label; 
                        }else{
                          echo "Link";
                        }
                      } ?>

                    </a>
                    <figure>

                     <?php        if( !empty( $op_open_pit_mining_images )):
                     $op_open_pit_mining_images = explode(",", $op_open_pit_mining_images);
                     foreach ($op_open_pit_mining_images as $value):
                      $images = $value;
                    $op_open_pit_mining_image     = wp_get_attachment_image_src( $images , 'full' );
                    ?>

                    <img src="<?php echo $op_open_pit_mining_image[0] ?>" alt="">
                  <?php endforeach;
                  endif; ?>

                </figure>
              </div>
              <!-- .production-item -->
              <div class="production-item">
                <header class="entry-header">
                  <h5 class="entry-title"> 3) 

                    <?php echo $op_benefication_title ?>: 

                  </h5>
                </header>
                <div class="entry-content">

                  <?php echo wpautop($op_benefication_content); ?>

                </div>
                <?php if (1 == $op_benefication_button_enable) {?>

                <div class="pdf-link">

                  <?php if (!empty($op_benefication_icon)) {?>

                  <i class="<?php echo $op_benefication_icon ?>"></i>

                  <?php }else{?>

                  <img src="http://via.placeholder.com/32x32" alt="pdf">

                  <?php }?>

                  <a href="<?php echo $op_benefication_url; ?>">

                    <?php       if (!empty($op_benefication_label)){
                      echo $op_benefication_label; 
                    }else{
                      echo "Link";
                    }
                  }?> 

                </a>
                <figure>

                  <?php if( !empty( $op_benefication_images )):
                  $op_benefication_images = explode(",", $op_benefication_images);
                  foreach ($op_benefication_images as $value):
                    $images                 = $value;
                  $op_benefication_image    = wp_get_attachment_image_src( $images , 'full' );?>
                  <img src="<?php echo $op_benefication_image[0] ?>" alt="">
                <?php     endforeach;
                endif; ?>

              </figure>
            </div>
            <!-- .production-item -->
            <div class="production-item">
              <header class="entry-header">
                <h5 class="entry-title">4) 

                  <?php echo $op_haulage_title ?>: 

                </h5>
              </header>
              <div class="entry-content">

                <?php echo wpautop($op_haulage_content); ?>

              </div>
              <?php if (1 == $op_haulage_button_enable) { ?>

              <div class="pdf-link">
                <?php if (!empty($op_haulage_icon)) {?>

                <i class="<?php echo $op_haulage_icon ?>"></i>

                <?php }else{?>

                <img src="http://via.placeholder.com/32x32" alt="pdf">

                <?php }?>

                <a href="<?php echo $op_haulage_url; ?>">
                  <?php       if (!empty($op_haulage_label)){
                    echo $op_haulage_label; 
                  }else{
                    echo "Link";
                  }
                }?>

              </a>
              <figure>

               <?php if( !empty( $op_haulage_images )):
               $op_haulage_images = explode(",", $op_haulage_images);
               foreach ($op_haulage_images as $value):
                $images = $value;
              $op_haulage_image     = wp_get_attachment_image_src( $images , 'full' );?>

              <img src="<?php echo $op_haulage_image[0] ?>" alt="">
            <?php endforeach;
            endif; 
            ?> 

          </figure>
        </div>
        <!-- .production-item -->
        <div class="production-item">
          <header class="entry-header">
            <h5 class="entry-title">5) 

              <?php echo $op_stock_piling_title ?>: 

            </h5>
          </header>
          <div class="entry-content">

            <?php echo wpautop($op_stock_piling_content); ?> 

          </div>

          <?php if (1 == $op_stock_piling_button_enable) { ?>

          <div class="pdf-link">
            <?php if (!empty($op_stock_piling_icon)) {?>
            <i class="<?php echo $op_stock_piling_icon ?>"></i>
            <?php }else{?>
            <img src="http://via.placeholder.com/32x32" alt="pdf">
            <?php }?>

            <a href="<?php echo $op_stock_piling_url; ?>">
              <?php   if (!empty($op_stock_piling_label)){
                echo $op_stock_piling_label; 
              }else{
                echo "Link";
              }
            }
            ?>
          </a>
          <figure>
            <?php if( !empty( $op_stock_piling_images )):
            $op_stock_piling_images = explode(",", $op_stock_piling_images);
            foreach ($op_stock_piling_images as $value):
              $images = $value;
            $op_stock_piling_image     = wp_get_attachment_image_src( $images , 'full' );
            ?>
            <img src="<?php echo $op_stock_piling_image[0] ?>" alt="">
          <?php endforeach;
          endif; 
          ?>                                                </figure>
        </div>
        <!-- .production-item -->
        <div class="production-item">
          <header class="entry-header">
            <h5 class="entry-title">6) <?php echo $op_drying_title ?>:</h5>
          </header>
          <div class="entry-content">
            <?php echo wpautop($op_drying_content); ?>
          </div>
          <?php if (1 == $op_drying_button_enable) {
            ?>
            <div class="pdf-link">
              <?php if (!empty($op_drying_icon)) {?>
              <i class="<?php echo $op_drying_icon ?>"></i>
              <?php }else{?>
              <img src="http://via.placeholder.com/32x32" alt="pdf">
              <?php }?>

              <a href="<?php echo $op_drying_url; ?>">

                <?php if (!empty($op_drying_label)){
                  echo $op_drying_label; 
                }else{
                  echo "Link";
                }
              }?>

            </a>
            <figure>

              <?php if( !empty( $op_drying_images )):
              $op_drying_images = explode(",", $op_drying_images);
              foreach ($op_drying_images as $value):
                $images = $value;
              $op_drying_image    = wp_get_attachment_image_src( $images , 'full' );?>
              <img src="<?php echo $op_drying_image[0] ?>" alt="">
            <?php endforeach;
            endif; ?> 

          </figure>
        </div>
        <!-- .production-item -->
        <div class="production-item">
          <header class="entry-header">
            <h5 class="entry-title">7) 

              <?php echo $op_transhiping_title ?>: 

            </h5>
          </header>
          <div class="entry-content">

            <?php echo wpautop($op_transhiping_content); ?>

          </div>

          <?php if (1 == $op_transhiping_button_enable) {?>

          <div class="pdf-link">

            <?php if (!empty($op_transhiping_icon)) {?>

            <i class="<?php echo $op_transhiping_icon ?>"></i>
            <?php }else{?>

            <img src="http://via.placeholder.com/32x32" alt="pdf">

            <?php }?>

            <a href="<?php echo $op_transhiping_url; ?>">
              <?php           if (!empty($op_transhiping_label)){
                echo $op_transhiping_label; 
              }else{
                echo "Link";
              }
            } ?>

          </a>
          <figure>

            <?php if( !empty( $op_transhiping_images )):
            $op_transhiping_images  = explode(",", $op_transhiping_images);
            foreach ($op_transhiping_images as $value):
              $images                 = $value;
            $op_transhiping_image     = wp_get_attachment_image_src( $images , 'full' );
            ?>

            <img src="<?php echo $op_transhiping_image[0] ?>" alt="">

          <?php endforeach;
          endif; ?>                                               

        </figure>
      </div>
      <!-- .production-item -->
    </div>
  </div>



</div>
<!-- #production-chain -->
<div id="productio01">
  <header class="entry-header">
    <h5 class="entry-title">

      <?php echo $op_production_title ?>

    </h5>
  </header>

  <?php echo wpautop($op_production_content); ?>

</div>
<!-- #productio01 -->

<div id="productio01" class="product-table">

  <header class="entry-header">
    <h5 class="entry-title">

      <?php echo $op_resource_and_reserve_title ?>
      

    </h5>
  </header>

  <div class="table-data">
    <!-- .table-headeing -->
    <?php echo wpautop($op_resource_and_reserve_content); ?>

  </div>
  <!-- .table-data -->

  <!-- #productio01 -->
</div>
</div>
</div>
</div>
</section>
</main>
<!-- #main -->
</div>
<!-- #primary -->
<?php
$enable_news_letter                 = cs_get_option( 'enable_news_letter' );
if( 1 == $enable_news_letter ):
  if( 1 == $op_mining_news_letter_enable ): ?>

<!-- .site-news -->
<section class="signup-section" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/images/img/subscribe-bg.jpg) no-repeat; background-size: cover">
  <div class="container">
    <div class="signup-content">
      <header class="section-header">
        <h2 class="section-title">Email newsletter</h2>
      </header>

      <?php $news_letter_form = cs_get_option( 'news_letter_form' );
      echo do_shortcode( $news_letter_form) ?>

    </div>
  </div>
</section>

<?php endif; ?>
<?php endif; ?>
<!-- . signup-section -->
</div>
<!-- #content -->


<?php 

get_footer(  ); 