<?php 
/*
Template Name: Contact Template
*/
get_header();
global $rws_options;
$contactpage_meta		        = get_post_meta( get_the_id (), 'rws_contact_section', true );

$rws_general_form_enable    = $contactpage_meta['rws_general_form_enable'];
$cp_general_form_title      = $contactpage_meta['cp_general_form_title'];
$cp_general_form_shortcode  = $contactpage_meta['cp_general_form_shortcode'];

$rws_enquiry_form_enable    = $contactpage_meta['rws_enquiry_form_enable'];
$cp_enquiry_form_title      = $contactpage_meta['cp_enquiry_form_title'];
$cp_enquiry_form_shortcode  = $contactpage_meta['cp_enquiry_form_shortcode'];

$rws_contact_persons_enable = $contactpage_meta['rws_contact_persons_enable'];
$cp_sales_person_title      = $contactpage_meta['cp_sales_person_title'];
$cp_contact_person1_name    = $contactpage_meta['cp_contact_person1_name'];
$cp_contact_person_phone1   = $contactpage_meta['cp_contact_person_phone1'];

$cp_contact_person2_name    = $contactpage_meta['cp_contact_person2_name'];
$cp_contact_person_phone2   = $contactpage_meta['cp_contact_person_phone2'];

$cp_contact_details         = $contactpage_meta['cp_contact_details'];
$cp_contact_details_title   = $contactpage_meta['cp_contact_details_title'];
$cp_contact_country_icon    = $contactpage_meta['cp_contact_country_icon'];
$cp_contact_country         = $contactpage_meta['cp_contact_country'];
$cp_contact_address1        = $contactpage_meta['cp_contact_address1'];
$cp_contact_address2        = $contactpage_meta['cp_contact_address2'];

$cp_contact_po_box_enable   = $contactpage_meta['cp_contact_po_box_enable'];
$cp_contact_po_box_number   = $contactpage_meta['cp_contact_po_box_number'];

$cp_contact_website_enable  = $contactpage_meta['cp_contact_website_enable'];
$cp_contact_website_icon    = $contactpage_meta['cp_contact_website_icon'];
$cp_contact_website         = $contactpage_meta['cp_contact_website'];

$cp_contact_email_enable    = $contactpage_meta['cp_contact_email_enable'];
$cp_contact_email_icon      = $contactpage_meta['cp_contact_email_icon'];
$cp_contact_email           = $contactpage_meta['cp_contact_email'];

$cp_contact_phone_enable    = $contactpage_meta['cp_contact_phone_enable'];
$cp_contact_phone_icon      = $contactpage_meta['cp_contact_phone_icon'];
$cp_contact_phone1          = $contactpage_meta['cp_contact_phone1'];
$cp_contact_phone2          = $contactpage_meta['cp_contact_phone2'];

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="site-contact">
      <div class="container">
        <div class="row">
          <div class="col-4">

            <?php if( 1 == $cp_contact_details ): ?>

              <header class="entry-header">
                <h3 class="entry-title">

                  <?php echo $cp_contact_details_title; ?>
                  
                </h3>
              </header>

            <?php endif; ?>

            <div class="contact-info">
              <ul>

                <?php if( 1 == $cp_contact_details ): ?>

                 <li>
                  <div class="icon-box"> 
                    <i class="<?php echo $cp_contact_country_icon ?>" aria-hidden="true"></i> 
                  </div>
                  <div class="content-box">

                    <p>

                      <?php echo $cp_contact_address1; ?>
                      <?php 
                      if (!empty($cp_contact_address2)) {
                       echo ",<br>" . $cp_contact_address2;
                     }
                     if (!empty($cp_contact_country)) {
                       echo $cp_contact_country ; 
                     }?>

                     <br> 

                     <?php if( 1 == $cp_contact_po_box_enable ): 

                     echo "P.O. Box " . $cp_contact_po_box_number; 

                     endif; ?>

                   </p>
                 </div>
               </li>

             <?php endif; 
             if( 1 == $cp_contact_website_enable ): ?>

             <li>
              <div class="icon-box"> 
                <i class="<?php echo $cp_contact_website_icon ?>" aria-hidden="true"></i>
              </div>
              <div class="content-box">
                <a href="<?php echo $cp_contact_website; ?>">

                  <?php echo $cp_contact_website; ?> 

                </a>
              </div>
            </li>
          <?php endif; 
          if( 1 == $cp_contact_email_enable ): ?>

          <li>
            <div class="icon-box"> <i class="<?php echo $cp_contact_email_icon; ?>
              " aria-hidden="true"></i> </div>
              <div class="content-box">
                <a href="mailto:#">

                  <?php echo $cp_contact_email; ?>
                  
                </a>
              </div>
            </li>

          <?php endif; 
          if( 1 == $cp_contact_phone_enable ): ?>


          <li>
            <div class="icon-box"> 
              <i class="<?php echo $cp_contact_phone_icon ?>" aria-hidden="true"></i> 
            </div>
            <div class="content-box">
              <a href="tel:#">

                <?php echo $cp_contact_phone1; ?></a> <br><a href="tel:#"><?php echo $cp_contact_phone2 ?>

              </a>
            </div>
          </li> 

        <?php endif; ?>

      </ul>
    </div>
    <!-- .contact-info -->
  </div> 
  <div class="col-8">
    <div class="contact-form">
      <header class="entry-header">
        <h3 class="entry-title">Get in Touch</h3>
      </header>
      <div id="parentHorizontalTab">
        <ul class="resp-tabs-list hor_1">

          <?php 
          if ($rws_general_form_enable == 1) {?>

          <li>

            <?php echo $cp_general_form_title; ?>

          </li>
          <?php } ?>
          <?php if ($rws_enquiry_form_enable == 1) {?>

          <li>

            <?php echo $cp_enquiry_form_title; ?>

          </li>
          <?php }  ?>

        </ul>
        <div class="resp-tabs-container hor_1">

         <?php if ($rws_general_form_enable == 1) {?>

         <div class="contact-form-item">
          <div role="form" class="wpcf7" id="wpcf7-f98-p2-o2" lang="en-US" dir="ltr">
            <div class="screen-reader-response"></div>

            <?php echo do_shortcode( $cp_general_form_shortcode ); ?>

          </div>
        </div>

        <?php } ?>
        <?php if ($rws_enquiry_form_enable == 1) {?>

        <div class="sales-contact-item">
          <div class="sales-person-item">
            <header class="entry-header">
              <h4 class="entry-title">

                <?php if (!empty($cp_sales_person_title)) {
                 echo $cp_sales_person_title;
               }?>   

             </h4>
           </header>
           <div class="sales-info">
            <ul>
              <li>
                <div class="sales-item">

                  <?php if (!empty($cp_contact_person1_name)):?>

                    <header class="entry-header">
                      <h4 class="entry-title">

                        <?php echo $cp_contact_person1_name; ?>

                      </h4>
                    </header>

                  <?php endif; ?>

                  <div class="icon-box"> 
                    <i class="fa fa-phone-square" aria-hidden="true"></i> 
                  </div>

                  <?php if (!empty($cp_contact_person_phone1)):?>

                    <div class="content-box">
                      <a href="tel:#">  

                       <?php echo $cp_contact_person_phone1; ?>

                     </a>
                   </div>

                 <?php endif; ?>

               </div>
             </li>
             <li>
              <div class="sales-item">

                <?php if (!empty($cp_contact_person2_name)): ?> 

                  <header class="entry-header">
                    <h4 class="entry-title">
                     <?php echo $cp_contact_person2_name; ?>
                   </h4>
                 </header>

               <?php endif; ?>   

               <div class="icon-box"> 
                 <i class="fa fa-phone-square" aria-hidden="true"></i> 
               </div>

               <?php if (!empty($cp_contact_person_phone2)):?>

                 <div class="content-box">
                  <a href="tel:#">
                   <?php echo $cp_contact_person_phone2; ?>
                 </a>
               </div> 

             <?php endif; ?>  


           </div>
         </li>
       </ul>
     </div>
     <!-- .contact-info -->
   </div>

   <div role="form" class="wpcf7" id="wpcf7-f98-p2-o2" lang="en-US" dir="ltr">
    <div class="screen-reader-response">
    </div>

    <?php echo do_shortcode( $cp_enquiry_form_shortcode ); ?>

  </div>
</div>

<?php }  ?>

</div>
</div>

</div>
</div>
</div>
</div>
</section>
<!-- .site-contact -->
</main>
<!-- #main -->
</div>
<!-- #primary -->
<?php get_footer();