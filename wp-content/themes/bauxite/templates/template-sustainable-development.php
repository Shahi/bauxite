<?php 
/*
Template Name: Sustainable Development
*/
get_header(); 

$rws_sustainable_section      = get_post_meta( get_the_id (), 'rws_sustainable_section', true );
$sp_listing_group             = $rws_sustainable_section['sp_listing_group'];
$rws_sp_news_letter_section   = get_post_meta( get_the_id (), 'rws_sp_news_letter_section', true );
$sp_news_letter_enable        = $rws_sp_news_letter_section['sp_news_letter_enable'];
?>
<!-- .page-title-box -->
<div id="content" class="site-content">
  <section class="sustainable-section">

    <div class="container">
      <div class="row">
        <div id="primary" class="col-8 content-area">
          <main id="main" class="site-main">
            <header class="entry-header">
              <h3 class="entry-title">

                <?php the_title(); ?>

              </h3>
            </header>
            <div class="sustainable-sub-heading">
              <ul>

               <?php 
               if( !empty( $sp_listing_group )):
                 foreach ($sp_listing_group as $value):

                  $sp_listing_title = $value['sp_listing_title'];

                ?>
                <li>

                  <?php echo $sp_listing_title ?>

                </li>
                <?php 
                endforeach; 
                endif?>

              </ul>
            </div>
            <!-- .sustainable-sub-heading -->
            <div class="sustainable-post">

              <?php if( !empty( $sp_listing_group )):
              foreach ($sp_listing_group as $value):

                $sp_listing_title = $value['sp_listing_title'];
              $sp_listing_content = $value['sp_listing_content'];
              $sp_listing_gallery = $value['sp_listing_gallery'];
              $sp_listing_gallery = explode(",", $sp_listing_gallery);
              ?>

              <article>
                <div class="article-post">
                  <div class="entry-header">
                    <h5 class="entry-title">

                      <?php echo $sp_listing_title; ?> 

                    </h5>
                  </div>
                  <div class="entry-content">

                    <?php echo wpautop($sp_listing_content); ?>

                  </div>

                  <?php if (isset($value['sp_enable_pdf_button'])) {
                    $sp_enable_pdf_button = $value['sp_enable_pdf_button'];
                    $sp_pdf_link_label    = $value['sp_pdf_link_label'];
                    $sp_pdf_link_url      = $value['sp_pdf_link_url'];
                    $sp_pdf_link_icon     = $value['sp_pdf_link_icon'];
                    $sp_pdf_link_icon     = wp_get_attachment_image_src( $sp_pdf_link_icon , 'full' );

                    if (1 == $sp_enable_pdf_button) {
                      ?>

                      <div class="pdf-link">
                        <?php 
                        if (!empty($sp_pdf_link_icon[0])) {?>
                        <img src="<?php echo $sp_pdf_link_icon[0];?>" alt="pdf">
                        <?php }else{?>
                        <img src="http://via.placeholder.com/32x32" alt="pdf">
                        <?php }?>

                        <a href="<?php echo $sp_pdf_link_url; ?>"><?php 
                          if (!empty($sp_pdf_link_label)){
                            echo $sp_pdf_link_label; 
                          }else{
                            echo "PDF Link";
                          }
                          ?>
                        </a>
                      </div>

                      <?php }
                    }?>


                    <?php if( $sp_listing_gallery ):
                    foreach ($sp_listing_gallery as $value):

                      $images = $value;
                    $images     = wp_get_attachment_image_src( $images , 'full' );?>
                    <?php if( $images ):?>

                      <div class="row">
                        <div class="col-6">
                          <img src=" <?php echo $images[0] ;?>" alt="">
                        </div>
                      </div>

                      <?php 
                      endif; 
                      endforeach;
                      endif; ?>

                    </div>
                  </article>

                <?php endforeach;
                endif; ?>

              </div>

              <!-- .sustainable-section -->
            </main>
            <!-- #main -->
          </div>
          <!-- #primary -->
          <?php 
          if ($sp_news_letter_enable == 1) {?>

          <div id="secondary" class="col-4 side-bar">
            <aside class="sidebar">

              <?php get_sidebar('right'); ?> 
              
            </aside>
          </div>
          <?php           
        }
        ?>

      </div>
    </div>
  </section>
</div>
<!-- #content -->
<?php get_footer( );?>

