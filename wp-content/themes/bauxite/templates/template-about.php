<?php 
/*
Template Name: About Template
*/
get_header(); ?>


<?php 

$rws_about_section_meta 		= get_post_meta( get_the_id (), 'rws_about_section', true );

$about_history_enable			= $rws_about_section_meta['about_history_enable'];
$about_history_title			= $rws_about_section_meta['about_history_title'];
$about_history_content			= $rws_about_section_meta['about_history_content'];
$about_history_image			= $rws_about_section_meta['about_history_image'];
$about_history_image 			= wp_get_attachment_image_src( $about_history_image , 'full' );

$about_company_structure_enable	= $rws_about_section_meta['about_company_structure_enable'];
$about_company_structure_image	= $rws_about_section_meta['about_company_structure_image'];
// $about_company_structure_image 	= wp_get_attachment_image_src( $about_company_structure_image , 'full' );
// var_dump($about_company_structure_image);



$about_location_enable			= $rws_about_section_meta['about_location_enable'];
$about_company_location_image	= $rws_about_section_meta['about_company_location_image'];
$about_company_location_image 	= wp_get_attachment_image_src( $about_company_location_image , 'full' );


$about_location_details_enable	= $rws_about_section_meta['about_location_details_enable'];
$about_location_title			= $rws_about_section_meta['about_location_title'];
$about_location_content			= $rws_about_section_meta['about_location_content'];

$about_contact_title			= $rws_about_section_meta['about_contact_title'];

$about_contact_address1			= $rws_about_section_meta['about_contact_address1'];
$about_contact_address2			= $rws_about_section_meta['about_contact_address2'];
$about_contact_email			= $rws_about_section_meta['about_contact_email'];
$about_contact_phone			= $rws_about_section_meta['about_contact_phone'];



$rws_about_news_letter_section 	= get_post_meta( get_the_id (), 'rws_about_news_letter_section', true );
$about_news_letter_enable		= $rws_about_news_letter_section['about_news_letter_enable'];
?>


<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<?php 
		if ($about_history_enable == 1) {?>
		
		<section class="abt-info">
			<div class="container">
				<div class="row">
					<div class="col-7">
						<figure><img src="<?php echo $about_history_image[0] ?>" alt="abt-info"></figure>
					</div>
					<div class="col-5">
						<header class="entry-header">
							<h3 class="entry-title"><?php echo $about_history_title; ?></h3>
						</header>
						<div class="entry-content">

							<?php 
							$content = wpautop($about_history_content);

							$contents = custom_cut($content,550); 
							echo $contents;
							?>

						</div>
						<a href="#popup-content-wrapper" class="btn link-btn popup-content-wrapper_open">
							View
							<i class="fa fa-arrow-right"></i>
						</a>
					</div>
				</div>
			</div>
		</section>
		<div id="popup-content-wrapper" class="">
			<h3 class="entry-title"><?php echo $about_history_title; ?></h3>
			<p><?php echo  wpautop($about_history_content); ?></p>

			<a class="close-btn popup-content-wrapper_close"></a>
		</div>
		<?php }
		?>
		<!-- .abt-info -->
		<section class="abt-site-info">
			
			<?php 
			if ($about_location_details_enable xor $about_location_enable == 1) {
				if ($about_location_enable == 1) {?>

				<div class="container">
					<div class="row">
						<div class="col-9">
							<div class="abt-site-info-item">
								<header class="entry-header">
									<h3 class="entry-title"></h3>
								</header>
								<aside class="widget site-address">
									<header class="entry-header">
										<h4 class="entry-title"></h4>
									</header>
									<div class="abt-map">
										<img src="<?php echo $about_company_location_image[0] ?>">
									</div>
									<ul>
										<li>
											<span class="footer-icon"><i class="fa fa-map-marker" aria-hidden="true"></i>
											</span>
										</li>

										<li><a href="mailto:#"><span class="footer-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span> </a></li>
										<li>
											<a href="tel:#"><span class="footer-icon"><i class="fa fa-phone-square" aria-hidden="true"></i></span> </a>
										</li>
									</ul>
								</aside>
								<div class="entry-content"></p>
								</div>
							</div>

						</div>
						<div class="col-3">

							<!-- .abt-site-info-item -->
						</div>
					</div>
				</div>

				<?php }else{
					?>
					<div class="container">
						<div class="row">
							<div class="col-12">
								<div class="abt-site-info-item">
									<header class="entry-header">

										<h3 class="entry-title">

											<?php echo $about_location_title ;?>
											
										</h3>
									</header>
									<aside class="widget site-address">
										<header class="entry-header">
											<h4 class="entry-title">

												<?php echo $about_contact_title ;?>
												
											</h4>
										</header>
										<ul>
											<li>
												<span class="footer-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span><?php echo $about_contact_address1;
												if (!empty($about_contact_address2)) {
													echo ", " . $about_contact_address2 ;
												} ?>

											</li>

											<li>
												<a href="mailto:#"><span class="footer-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span> 

													<?php echo $about_contact_email; ?>

												</a>
											</li>
											<li>
												<a href="tel:#"><span class="footer-icon"><i class="fa fa-phone-square" aria-hidden="true"></i></span>

													<?php echo $about_contact_phone; ?>

												</a>
											</li>
										</ul>
									</aside>
									<div class="entry-content">
										<p>

											<?php echo  wpautop($about_contact_content); ?>

										</p>
									</div>

								</div>
								<!-- .abt-site-info-item -->
							</div>
						</div>
					</div>
					<?php }
				}elseif($about_location_details_enable && $about_location_enable == 1){?>
				<div class="container">
					<div class="row">
						<div class="col-7">
						</div>
						<div class="col-5">
							<div class="abt-site-info-item">
								<header class="entry-header">
									<h4 class="entry-title">

										<?php echo $about_location_title ;?>
										
									</h4>
								</header>
								<div class="entry-content">

									<?php echo wpautop($about_location_content); ?>

								</div>
								<aside class="widget site-address">
									<header class="entry-header">
										<h4 class="entry-title">

											<?php echo $about_contact_title ;?>
											
										</h4>
									</header>
									<ul>
										<li>
											<span class="footer-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>

											<?php echo $about_contact_address1?>
											<?php if (!empty($about_contact_address2)) {
												echo "," . $about_contact_address2 ;
											} ?>

										</li>

										<li>
											<a href="mailto:#"><span class="footer-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span> 

												<?php echo $about_contact_email; ?>

											</a>
										</li>
										<li>
											<a href="tel:#"><span class="footer-icon"><i class="fa fa-phone-square" aria-hidden="true"></i></span>

												<?php echo $about_contact_phone; ?>

											</a>
										</li>
									</ul>
								</aside>


							</div>
							<!-- .abt-site-info-item -->
						</div>
					</div>
				</div>
				<div class="abt-map">
					<img src="<?php echo $about_company_location_image[0] ?>">
				</div>

				<?php }else{

				}?>

			</section>
			<!-- .abt-site-info -->
			<?php if ($about_company_structure_enable == 1) {?>

			<section class="company-structure">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<header class="entry-header">
								<h3 class="entry-title">Company Structure</h3>
							</header>
							<figure>
								<img src="<?php echo $about_company_structure_image ?>" alt="company-structure-img">
							</figure>
						</div>
					</div>
				</div>
			</section>

			<?php } 
			$args = new WP_Query( array(
				'post_type' => 'team',
				'tax_query' => array(
					array(
						'taxonomy' => 'team-category',
						'field' => 'slug', 
						'terms' => 'board-of-director'
						)
					)
				) );

				if ( $args->have_posts() ) :?>
				<section class="site-team">
					<div class="container">
						<header class="entry-header">
							<h3 class="entry-title">Board Of Director</h3>
						</header>
						<div class="row">
							<div class="owl-carousel owl-theme">

								<?php while ( $args->have_posts() ) :
								$args->the_post();
								$rws_cpt_team_meta 				= get_post_meta( get_the_id (), 'rws_cpt_team_section', true );
								$about_team_post				= $rws_cpt_team_meta['about_team_post'];
								$about_team_fb_contact			= $rws_cpt_team_meta['about_team_fb_contact'];
								$about_team_instagram_contact	= $rws_cpt_team_meta['about_team_instagram_contact'];
								$about_team_twitter_contact		= $rws_cpt_team_meta['about_team_twitter_contact'];
								$about_team_Linked_in_contact	= $rws_cpt_team_meta['about_team_Linked_in_contact'];
								?>

								<div class="item">
									<div class="team-item">
										<figure class="overlay">
											<img src="<?php if (has_post_thumbnail( )) {
												the_post_thumbnail_url('about-post-size');
											}else{
												echo "http://via.placeholder.com/358x350";
											}
											?>" alt="team-img" />
											<figcaption>

												<a href="<?php echo the_permalink(); ?>" class="btn link-btn"> View Detail </a>
												<ul>

													<?php if (!empty($about_team_fb_contact)) {?>

													<li>
														<a href="<?php echo $about_team_fb_contact; ?>">
															<i class="fa fa-facebook" aria-hidden="true"></i>
														</a>
													</li>

													<?php } ?>

													<?php if (!empty($about_team_instagram_contact)) {?>

													<li>
														<a href="<?php echo $about_team_instagram_contact; ?>">
															<i class="fa fa-instagram" aria-hidden="true"></i>
														</a>
													</li>

													<?php } ?>

													<?php if (!empty($about_team_twitter_contact)) {?>

													<li>
														<a href="<?php echo $about_team_twitter_contact; ?>">
															<i class="fa fa-twitter" aria-hidden="true"></i>
														</a>
													</li>

													<?php } ?>

													<?php if (!empty($about_team_Linked_in_contact)) {?>

													<li>
														<a href="<?php echo $about_team_Linked_in_contact; ?>">
															<i class="fa fa-linkedin" aria-hidden="true"></i>
														</a>
													</li>

													<?php } ?>


												</ul>

											</figcaption>
										</figure>
										<!-- .overlay  -->
										<div class="team-name">
											<header class="entry-header">
												<h4 class="entry-title">
													<a href="<?php echo the_permalink(); ?>"> 

														<?php the_title();?> 

														<span class="team-designation">

															<?php echo  wpautop($about_team_post); ?>

														</span>
													</a> 
												</h4>
											</header>
										</div>
										<!-- .team-name -->
									</div>
									<!-- .team-item -->
								</div>

							<?php endwhile; 
							wp_reset_query();
							wp_reset_postdata();?>

						</div>
					</div>
				</div>
			</section>
		<?php endif; 
		?>
		<!-- .Board Of director -->

		<?php
		$args = new WP_Query( array(
			'post_type' => 'team',
			'tax_query' => array(
				array(
					'taxonomy' => 'team-category',
					'field' => 'slug', 
					'terms' => 'team-members'
					)
				)

			) );

			if ( $args->have_posts() ) :?>
			<section class="site-team management-team">
				<div class="container">
					<header class="entry-header">
						<h3 class="entry-title">Management Team</h3>
					</header>
					<div class="row">
						<div class="owl-carousel owl-theme">

							<?php while ( $args->have_posts() ) :
							$args->the_post();
							$rws_cpt_team_meta 				= get_post_meta( get_the_id (), 'rws_cpt_team_section', true );
							$about_team_post				= $rws_cpt_team_meta['about_team_post'];
							$about_team_fb_contact			= $rws_cpt_team_meta['about_team_fb_contact'];
							$about_team_instagram_contact	= $rws_cpt_team_meta['about_team_instagram_contact'];
							$about_team_twitter_contact		= $rws_cpt_team_meta['about_team_twitter_contact'];
							$about_team_Linked_in_contact	= $rws_cpt_team_meta['about_team_Linked_in_contact'];
							?>

							<div class="item">
								<div class="team-item">
									<figure class="overlay">
										<img src="<?php if (has_post_thumbnail( )) {
											the_post_thumbnail_url('about-post-size');
										}else{
											echo "http://via.placeholder.com/358x350";
										}
										?>" alt="team-img" />

										<figcaption>

											<a href="<?php echo the_permalink(); ?>" class="btn link-btn"> View Detail </a>

											<ul>

												<?php if (!empty($about_team_fb_contact)) {?>

												<li>
													<a href="<?php echo $about_team_fb_contact; ?>">
														<i class="fa fa-facebook" aria-hidden="true"></i>
													</a>
												</li>

												<?php };
												if (!empty($about_team_instagram_contact)) {?>
												<li>
													<a href="<?php echo $about_team_instagram_contact; ?>">
														<i class="fa fa-instagram" aria-hidden="true"></i>
													</a>
												</li>

												<?php } ?>
												<?php if (!empty($about_team_twitter_contact)) {?>

												<li>
													<a href="<?php echo $about_team_twitter_contact; ?>">
														<i class="fa fa-twitter" aria-hidden="true"></i>
													</a>
												</li>

												<?php } ?>

												<?php if (!empty($about_team_Linked_in_contact)) {?>
												<li>
													<a href="<?php echo $about_team_Linked_in_contact; ?>">
														<i class="fa fa-linkedin" aria-hidden="true"></i>
													</a>
												</li>

												<?php } ?>


											</ul>

										</figcaption>
									</figure>
									<!-- .overlay  -->
									<div class="team-name">
										<header class="entry-header">
											<h4 class="entry-title">

												<a href="<?php echo the_permalink(); ?>"> 

													<?php the_title(); ?> 

													<span class="team-designation">

														<?php echo wpautop($about_team_post); ?>

													</span>
												</a> </h4>
											</header>
										</div>
										<!-- .team-name -->
									</div>
									<!-- .team-item -->
								</div>

							<?php endwhile; 
							wp_reset_query();
							wp_reset_postdata();?>

						</div>
					</div>
				</div>
			</section>
		<?php endif; 
		?>
		<!-- .site-team -->

		<?php 
		$enable_news_letter 			= cs_get_option( 'enable_news_letter' );
		if( 1 							== $enable_news_letter ):
			?>
		<?php if( 1 					== $about_news_letter_enable ): ?>
			<!-- .site-news -->
			<section class="signup-section" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/images/img/subscribe-bg.jpg) no-repeat; background-size: cover">
				<div class="container">
					<div class="signup-content">
						<header class="section-header">
							<h2 class="section-title">email newsletter</h2>
						</header>

						<?php $news_letter_form = cs_get_option( 'news_letter_form' );
						echo do_shortcode( $news_letter_form) ?>

					</div>
				</div>
			</section>
			<!-- . signup-section -->
		<?php endif; ?>
	<?php endif; ?>

	<!-- . signup-section -->
</main>
<!-- #main -->

</div>
<!-- #primary -->		
<?php get_footer();?>
