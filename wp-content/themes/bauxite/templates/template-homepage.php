<?php 
/*
Template Name: Homepage Template
*/
get_header();?>



<div id="primary" class="content-area">
	<main id="main" class="site-main">	
		<?php
		$welcome_meta 		= get_post_meta( get_the_id (), 'rws_homepage_section', true );
		$rws_welcome_enable = $welcome_meta['rws_welcome_enable'];
		$hp_welcome_title 	= $welcome_meta[ 'hp_welcome_title' ];
		$hp_welcome_desc 	= $welcome_meta[ 'hp_welcome_desc' ];
		$hp_welcome_image	= $welcome_meta[ 'hp_welcome_image' ];
		$hp_welcome_image 	= wp_get_attachment_image_src( $hp_welcome_image , 'full' );
		$hp_welcome_image 	= $hp_welcome_image[0];

		$hp_welcome_btn_enable = $welcome_meta[ 'hp_welcome_btn_enable' ];
		?>
		<?php if( 1 == $rws_welcome_enable ):?>
			<section class="site-description">
				<div class="container">
					<div class="row">
						<div class="col-6">
							<div class="description-item">
								<header class="entry-header">
									<h3 class="entry-title"><?php echo $hp_welcome_title ?></h3>
								</header>
								<div class="entry-content">
									<p>
										<?php $content = wpautop($hp_welcome_desc);
										$contents = custom_cut($content,550); 
										echo $contents;?>

									</p>
								</div>

								<?php if( 1 == $hp_welcome_btn_enable ): 
								$hp_welcome_btn_url = $welcome_meta[ 'hp_welcome_btn_url' ];
								$hp_welcome_btn_label = $welcome_meta[ 'hp_welcome_btn_label' ];?>

								<a href="#popup-content-wrapper" class="btn link-btn popup-content-wrapper_open">

									<?php echo ( $hp_welcome_btn_label) ? $hp_welcome_btn_label : 'View'; ?> 

									<i class="fa fa-arrow-right"></i>
								</a>
								<?php 
								endif; 
								?>
							</div>
							<!-- .description-item -->
						</div>

						<div class="col-7">
							<div class="description-img">

								<?php if (!empty($hp_welcome_image)):?>
									
									<img src="<?php echo $hp_welcome_image ?>" alt="about-img">

								<?php endif; ?>

							</div>
							<!-- .description-img -->
						</div>

						<?php 
						endif; 
						?>
					</div>
				</div>
				<div id="popup-content-wrapper" class="">
					<h3 class="entry-title">

						<?php echo $hp_welcome_title ?>

					</h3>

					<p>

						<?php echo wpautop($hp_welcome_desc); ?>
						
					</p>

					<a class="close-btn popup-content-wrapper_close"></a>
				</div>
			</section>

			<?php
			$home_counter_meta 			= get_post_meta( get_the_id (), 'rws_homepage_section', true );
			$hp_counter_enable			= $home_counter_meta['hp_counter_enable'];
			$hp_counter_group			= $home_counter_meta['hp_counter_group'];
			if (isset($home_counter_meta['hp_counter_background'])) {
				$hp_counter_background	= $home_counter_meta['hp_counter_background'];
				$hp_counter_background 	= wp_get_attachment_image_src( $hp_counter_background , 'full' );
				$hp_counter_background 	= $hp_counter_background[0];
			}

			if( 1 == $hp_counter_enable ): ?>

			<?php if( !empty( $hp_counter_group )):?>

				<!-- .site-description -->
				<section class="number-counter" id="counter" style="background: url(<?php echo $hp_counter_background ?>)no-repeat; background-size: cover;">
					<div class="container">
						<div class="row">

							<?php foreach ($hp_counter_group as $value):
							$hp_counter_title 	= $value['hp_counter_title'];
							$hp_counter_amount	= $value['hp_counter_amount'];
							$hp_counter_image	= $value['hp_counter_image'];
							$hp_counter_image 	= wp_get_attachment_image_src( $hp_counter_image , 'side-image-size' );
							$hp_image 			= $hp_counter_image[0];
							?>

							<div class="col-4">
								<figure>

									<?php if (!empty($hp_image)):?>

										<img src="<?php echo $hp_image; ?>" alt="team-icon">

									<?php endif; ?>

								</figure>
								<div class="count">
									<div class="counter-value" data-count="<?php echo $hp_counter_amount; ?>">0</div>
									<span class="number-title">

										<?php echo $hp_counter_title; ?> 

									</span>
								</div>
							</div>
							<?php 
							endforeach;
							?>
						</div>
					</div>
				</section>
			<?php endif; 

			endif; ?> 
			<!-- .number-counter -->
			<?php
			$information_meta 				= get_post_meta( get_the_id (), 'rws_homepage_section', true );
			$hp_information_side_image		= $information_meta['hp_site_information_side_image'];
			$hp_information_side_image 		= wp_get_attachment_image_src( $hp_information_side_image , 'full' );
			$hp_side_image 					= $hp_information_side_image[0];
			$hp_information_bottom_image	= $information_meta['hp_site_information_bottom_image'];
			$hp_information_bottom_image 	= wp_get_attachment_image_src( $hp_information_bottom_image , 'full' );
			$hp_bottom_image 				= $hp_information_bottom_image[0];

			$hp_site_information_enable		= $information_meta['hp_site_information_enable'];

			$hp_site_mission_enable			= $information_meta['hp_site_mission_enable'];
			$hp_site_mission_title			= $information_meta['hp_site_mission_title'];
			$hp_site_mission_content		= $information_meta['hp_site_mission_content'];

			$hp_site_mission_button_enable	= $information_meta['hp_site_mission_button_enable'];
			$hp_site_mission_button_label	= $information_meta['hp_site_mission_button_label'];
			$hp_site_mission_button_url		= $information_meta['hp_site_mission_button_url'];

			$hp_site_vision_enable			= $information_meta['hp_site_vision_enable'];
			$hp_site_vision_title			= $information_meta['hp_site_vision_title'];
			$hp_site_vision_content			= $information_meta['hp_site_vision_content'];

			$hp_site_vision_button_enable	= $information_meta['hp_site_vision_button_enable'];
			$hp_site_vision_button_label	= $information_meta['hp_site_vision_button_label'];
			$hp_site_vision_button_url		= $information_meta['hp_site_vision_button_url'];

			$hp_site_values_enable			= $information_meta['hp_site_values_enable'];
			$hp_site_values_title			= $information_meta['hp_site_values_title'];
			$hp_site_values_content			= $information_meta['hp_site_values_content'];

			$hp_site_values_button_enable	= $information_meta['hp_site_values_button_enable'];
			$hp_site_values_button_label	= $information_meta['hp_site_values_button_label'];
			$hp_site_values_button_url		= $information_meta['hp_site_values_button_url'];


			if( 1 == $hp_site_information_enable ): 
				?>

			<section class="site-core-value">
				<div class="container">
					<div class="row">
						<div class="col-9">
							<ul class="accordion">
								<li class="active">
									<header class="entry-header">
										<h3 class="entry-title">

											<?php echo $hp_site_vision_title; ?>

											<span class="btnAccordionToggle">-</span>
										</h3>
									</header>
									<div class="entry-content">

										<?php  echo wpautop($hp_site_vision_content);
										if( 1 == $hp_site_vision_button_enable ): ?>

										<a href="<?php echo $hp_site_vision_button_url; ?>" class="btn link-btn"><?php echo ( $hp_site_vision_button_label) ? $hp_site_vision_button_label : 'View all'; ?> <i class="fa fa-arrow-right"></i></a>

									<?php endif; ?>
								</div>
							</li>
							<li>
								<header class="entry-header">
									<h3 class="entry-title">

										<?php echo $hp_site_mission_title; ?>

										<span class="btnAccordionToggle">+</span>
									</h3>
								</header>
								<div class="entry-content">

									<?php  echo  wpautop($hp_site_mission_content); 
									if( 1 == $hp_site_mission_button_enable ): ?>

									<a href="<?php echo $hp_site_mission_button_url; ?>" class="btn link-btn">

										<?php echo ( $hp_site_mission_button_label) ? $hp_site_mission_button_label : 'View all'; ?> 

										<i class="fa fa-arrow-right"></i>
									</a>

								<?php endif; ?>
							</div>
						</li>

						<li>
							<header class="entry-header">
								<h3 class="entry-title">

									<?php echo $hp_site_values_title; ?>

									<span class="btnAccordionToggle">+</span>
								</h3>
							</header>
							<div class="entry-content">
								<?php  echo wpautop($hp_site_values_content);
								if( 1 == $hp_site_values_button_enable ): ?>

								<a href="<?php echo $hp_site_values_button_url; ?>" class="btn link-btn"><?php echo ( $hp_site_values_button_label) ? $hp_site_values_button_label : 'View all'; ?> 
									<i class="fa fa-arrow-right"></i>
								</a>

							<?php endif; ?>
						</div>
					</li>
				</ul>
			</div>
			<div class="col-3">
				<figure>

					<?php if (!empty($hp_side_image)):?>

						<img src="<?php echo $hp_side_image ?>" alt="tab-img">

					<?php endif; ?>

				</figure>

			</div>
		</div>
	</div>
	<figure class="core-value-bg-img">
		<?php if (!empty($hp_side_image)):?>

			<img src="<?php echo $hp_bottom_image ?>" alt="bg-img">

		<?php endif?> 

	</figure>
</section>
<?php endif; ?>

<!-- .site-core-value -->
<?php
$latest_news_meta = get_post_meta( get_the_id (), 'rws_homepage_section', true );

$rws_latest_news_enable 		= $latest_news_meta['rws_latest_news_enable'];
$hp_latest_news_title 			= $latest_news_meta['hp_latest_news_title'];
$hp_latest_news_desc 			= $latest_news_meta['hp_latest_news_desc'];
$hp_latest_news_button_enable 	= $latest_news_meta['hp_latest_news_button_enable'];
$hp_latest_news_button_label 	= $latest_news_meta['hp_latest_news_button_label'];
$hp_latest_news_button_url 		= $latest_news_meta['hp_latest_news_button_url'];


?>
<?php if( 1 == $rws_latest_news_enable ): ?>

	<section class="site-news">
		<div class="container">
			<div class="row">
				<div class="col-5">
					<div class="theiaStickySidebar">
						<div class="news-heading">
							<header class="entry-header">
								<h3 class="entry-title">

									<?php echo $hp_latest_news_title; ?>
									
								</h3>
							</header>
							<div class="entry-content">
								<p>

									<?php echo wpautop($hp_latest_news_desc); ?>
									
								</p>
							</div>
							<?php if( 1 == $hp_latest_news_button_enable ): ?>

								<a href="<?php echo $hp_latest_news_button_url; ?>" class="btn link-btn">

									<?php echo ( $hp_latest_news_button_label) ? $hp_latest_news_button_label : 'View all'; ?> 

									<i class="fa fa-arrow-right"></i>
								</a>

								<?php 
								endif; 
								?>

							</div>
						</div>
					</div>
					<div class="col-7">
						<div class="theiaStickySidebar">
							<div class="row">

								<?php
								$args = new WP_Query( array(
									'post_type' => 'post',
									'posts_per_page' => 6,
									'cat' => 6 ,
									)
								);
								if ($args->have_posts()) {

									while ( $args->have_posts() ) :
										$args->the_post();
									?>	

									<div class="col-6">
										<div class="site-news-item">
											<figure class="img-overlay snip1205">

												<?php 
												if (has_post_thumbnail( )) {
													the_post_thumbnail( 'home-post-size');
												}else{?>

												<img src="http://via.placeholder.com/360x358" /> 

												<?php }?>

												<i class="fa fa-compress" aria-hidden="true"></i>
												<a href="<?php the_permalink();?>"></a>

											</figure>
											<div class="post">
												<div class="post-meta">
													<div class="date">

														<?php echo get_the_date( 'd M Y' ); ?>

													</div>
													<!-- .post-meta -->
													<header class="entry-header">
														<h4 class="entry-title">
															<a href="<?php the_permalink();?>">

																<?php the_title(); ?>

															</a>
														</h4>
													</header>
												</div>
												<!-- .post -->

											</div>
											<!-- .site-news-item -->
										</div>
									</div>
								<?php endwhile;
							}
							wp_reset_postdata();
							wp_reset_query();
							?>

						</div>
					</div>
				</div>
			</section>
		<?php endif; ?>

		<?php 
		$rws_homepage_news_letter_section 	= get_post_meta( get_the_id (), 'rws_homepage_news_letter_section', true );
		$homepage_news_letter_enable 		= $rws_homepage_news_letter_section['homepage_news_letter_enable'];
		$enable_news_letter				 	= cs_get_option( 'enable_news_letter' );
		if( 1 == $enable_news_letter ): 
			if( 1 == $homepage_news_letter_enable ): ?>
		<!-- .site-news -->

		<section class="signup-section" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/images/img/subscribe-bg.jpg) no-repeat; background-size: cover">
			<div class="container">
				<div class="signup-content">
					<header class="section-header">
						<h2 class="section-title">email newsletter</h2>
					</header>

					<?php $news_letter_form = cs_get_option( 'news_letter_form' );
					echo do_shortcode( $news_letter_form) ?>

				</div>
			</div>
		</section>
		<!-- . signup-section -->

	<?php endif; ?>
<?php endif; ?>


</main>
<!-- #main -->
</div>
<script>

	var a = 0;
	jQuery(window).scroll(function($) {

		var oTop = jQuery('#counter').offset().top - window.innerHeight;
		if (a == 0 && jQuery(window).scrollTop() > oTop) {
			jQuery('.counter-value').each(function() {
				var $this = jQuery(this),
				countTo = $this.attr('data-count');
				jQuery({
					countNum: $this.text()
				}).animate({
					countNum: countTo
				},

				{

					duration: 2000,
					easing: 'swing',
					step: function() {
						$this.text(Math.floor(this.countNum));
					},
					complete: function() {
						$this.text(this.countNum);
                                //alert('finished');
                            }

                        });
			});
			a = 1;
		}

	});

</script>
<!-- #primary -->

<?php get_footer();