<?php 
/*
Template Name: Operation-Precious Template
*/
get_header(  );

$rws_operation_precious_section 				= get_post_meta( get_the_id (), 'rws_operation_precious_section', true );

$op_precious_exploration_permits_title 			= $rws_operation_precious_section['op_precious_exploration_permits_title'];
$op_precious_exploration_permits_content 		= $rws_operation_precious_section[ 'op_precious_exploration_permits_content' ];
$op_precious_exploration_permits_image 			= $rws_operation_precious_section['op_precious_exploration_permits_image'];
$op_precious_exploration_permits_button_enable 	= $rws_operation_precious_section[ 'op_precious_exploration_permits_button_enable' ];
$op_precious_exploration_permits_button_label 	= $rws_operation_precious_section[ 'op_precious_exploration_permits_button_label' ];
$op_precious_exploration_permits_button_url 	= $rws_operation_precious_section['op_precious_exploration_permits_button_url'];
$op_precious_exploration_permits_button_icon 	= $rws_operation_precious_section['op_precious_exploration_permits_button_icon'];


$op_precious_operations_title 					= $rws_operation_precious_section['op_precious_operations_title'];

$op_precious_operations_tab1_title 				= $rws_operation_precious_section['op_precious_operations_tab1_title'];
$op_precious_operations_tab1_content 			= $rws_operation_precious_section[ 'op_precious_operations_tab1_content' ];
$op_precious_operations_tab1_image 				= $rws_operation_precious_section['op_precious_operations_tab1_image'];
$op_precious_operations_tab1_button_enable 		= $rws_operation_precious_section[ 'op_precious_operations_tab1_button_enable' ];
$op_precious_operations_tab1_button_label 		= $rws_operation_precious_section[ 'op_precious_operations_tab1_button_label' ];
$op_precious_operations_tab1_button_url 		= $rws_operation_precious_section['op_precious_operations_tab1_button_url'];
$op_precious_operations_tab1_button_icon 	= $rws_operation_precious_section['op_precious_operations_tab1_button_icon'];


$op_precious_operations_tab2_title 				= $rws_operation_precious_section['op_precious_operations_tab2_title'];
$op_precious_operations_tab2_content 			= $rws_operation_precious_section[ 'op_precious_operations_tab2_content' ];
$op_precious_operations_tab2_image 				= $rws_operation_precious_section['op_precious_operations_tab2_image'];
$op_precious_operations_tab2_button_enable 		= $rws_operation_precious_section[ 'op_precious_operations_tab2_button_enable' ];
$op_precious_operations_tab2_button_label 		= $rws_operation_precious_section[ 'op_precious_operations_tab2_button_label' ];
$op_precious_operations_tab2_button_url 		= $rws_operation_precious_section['op_precious_operations_tab2_button_url'];
$op_precious_operations_tab1_button_icon 	= $rws_operation_precious_section['op_precious_operations_tab1_button_icon'];


$op_precious_operations_tab3_title 				= $rws_operation_precious_section['op_precious_operations_tab3_title'];
$op_precious_operations_tab3_content 			= $rws_operation_precious_section[ 'op_precious_operations_tab3_content' ];
$op_precious_operations_tab3_image 				= $rws_operation_precious_section['op_precious_operations_tab3_image'];
$op_precious_operations_tab3_button_enable 		= $rws_operation_precious_section[ 'op_precious_operations_tab3_button_enable' ];
$op_precious_operations_tab3_button_label 		= $rws_operation_precious_section[ 'op_precious_operations_tab3_button_label' ];
$op_precious_operations_tab3_button_url 		= $rws_operation_precious_section['op_precious_operations_tab3_button_url'];
$op_precious_operations_tab1_button_icon 	= $rws_operation_precious_section['op_precious_operations_tab1_button_icon'];


$op_precious_operations_tab4_title 				= $rws_operation_precious_section['op_precious_operations_tab4_title'];
$op_precious_operations_tab4_content 			= $rws_operation_precious_section[ 'op_precious_operations_tab4_content' ];
$op_precious_operations_tab4_image 				= $rws_operation_precious_section['op_precious_operations_tab4_image'];
$op_precious_operations_tab4_button_enable 		= $rws_operation_precious_section[ 'op_precious_operations_tab4_button_enable' ];
$op_precious_operations_tab4_button_label 		= $rws_operation_precious_section[ 'op_precious_operations_tab4_button_label' ];
$op_precious_operations_tab4_button_url 		= $rws_operation_precious_section['op_precious_operations_tab4_button_url'];
$op_precious_operations_tab1_button_icon 	= $rws_operation_precious_section['op_precious_operations_tab1_button_icon'];


?>



<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="operation-sub-part">
				<div class="container">
					<div class="row">
						<div class="col-8">
							<header class="entry-header">
								<h3 class="entry-title">
									<?php the_title(); ?>
								</h3>
							</header>
							<div class="sub-part-content operation-list-item">
								<div class="operation-item">
									<div class="article-post">
										<div class="entry-header">
											<h5 class="entry-title"><?php echo $op_precious_exploration_permits_title ?></h5>
										</div>
										<div class="entry-content">
											<?php echo wpautop($op_precious_exploration_permits_content); ?>
										</div>
										<?php if (1 == $op_precious_exploration_permits_button_enable) { ?>
										<div class="pdf-link">
											<?php 
											if (!empty($op_precious_exploration_permits_button_icon)) {?>
											<i class="<?php echo $op_precious_exploration_permits_button_icon;?>"></i>
											<?php }else{?>
											<img src="http://via.placeholder.com/32x32" alt="pdf">
											<?php }?>

											<a href="<?php echo $op_precious_exploration_permits_button_url; ?>">
												<?php 
												if (!empty($op_precious_exploration_permits_button_label)){
													echo $op_precious_exploration_permits_button_label; 
												}else{
													echo "PDF Link";
												}
												?>
											</a>
										</div>
										<?php }?>
									</div>
									<figure>
										<?php
										if( !empty( $op_precious_exploration_permits_image )):
											$op_precious_exploration_permits_images = explode(",", $op_precious_exploration_permits_image);
										foreach ($op_precious_exploration_permits_images as $value):
											$images = $value;
										$op_precious_exploration_permits_image     = wp_get_attachment_image_src( $images , 'full' );
										?>
										<img src="<?php echo $op_exploration_image[0] ?>" alt="">
										<?php
										endforeach;
										endif; 
										?>
									</figure>
								</div>


								<header class="entry-header">
									<h3 class="entry-title">
										<?php echo $op_precious_operations_title ?>
									</h3>
								</header>
								<ul>
									<li>
										<div class="operation-item">
											<div class="entry-header">
												<h5 class="entry-title"><?php echo $op_precious_operations_tab1_title ?></h5>
											</div>
											<div class="entry-content">
												<?php echo wpautop($op_precious_operations_tab1_content); ?>
											</div>
											<?php if (1 == $op_precious_operations_tab1_button_enable) { ?>
											<div class="pdf-link">
												<?php 
												if (!empty($op_precious_operations_tab1_button_icon)) {?>
												<i class="<?php echo $op_precious_operations_tab1_button_icon;?>"></i>
												<?php }else{?>
												<img src="http://via.placeholder.com/32x32" alt="pdf">
												<?php }?>

												<a href="<?php echo $op_precious_operations_tab1_button_url; ?>">
													<?php 
													if (!empty($op_precious_operations_tab1_button_label)){
														echo $op_precious_operations_tab1_button_label; 
													}else{
														echo "PDF Link";
													}
													?>
												</a>
											</div>
											<?php }?>
											<figure>
												<?php
												if( !empty( $op_precious_operations_tab1_image )):
													$op_precious_operations_tab1_images = explode(",", $op_precious_operations_tab1_image);
												foreach ($op_precious_operations_tab1_images as $value):
													$images = $value;
												$op_precious_operations_tab1_image     = wp_get_attachment_image_src( $images , 'full' );
												?>
												<img src="<?php echo $op_precious_operations_tab1_image[0] ?>" alt="">
												<?php
												endforeach;
												endif; 
												?>
											</figure>
										</div>
									</li>
									<li>
										<div class="operation-item">
											
											<div class="entry-header">
												<h5 class="entry-title"><?php echo $op_precious_operations_tab2_title ?></h5>
											</div>
											<div class="entry-content">
												<?php echo wpautop($op_precious_operations_tab2_content); ?>
											</div>
											<?php if (1 == $op_precious_operations_tab2_button_enable) { ?>
											<div class="pdf-link">
												<?php 
												if (!empty($op_precious_operations_tab2_button_icon)) {?>
												<i class="<?php echo $op_precious_operations_tab2_button_icon;?>"></i>
												<?php }else{?>
												<img src="http://via.placeholder.com/32x32" alt="pdf">
												<?php }?>

												<a href="<?php echo $op_precious_operations_tab2_button_url; ?>">
													<?php 
													if (!empty($op_precious_operations_tab2_button_label)){
														echo $op_precious_operations_tab2_button_label; 
													}else{
														echo "PDF Link";
													}
													?>
												</a>
											</div>
											<?php }?>
											<div class="row">		
												<?php
												if( !empty( $op_precious_operations_tab2_image )):
													$op_precious_operations_tab2_images = explode(",", $op_precious_operations_tab2_image);
												foreach ($op_precious_operations_tab2_images as $value):
													$images = $value;
												$op_precious_operations_tab2_image     = wp_get_attachment_image_src( $images , 'full' );
												?>
												<div class="col-4">
													<img src="<?php echo $op_precious_operations_tab2_image[0] ?>" alt="">
												</div>

												<?php
												endforeach;
												endif; 
												?>
											</div>

										</div>
									</li>
									<li>
										<div class="operation-item">

											<div class="entry-header">
												<h5 class="entry-title"><?php echo $op_precious_operations_tab3_title ?></h5>
											</div>
											<div class="entry-content">
												<?php echo wpautop($op_precious_operations_tab3_content); ?>
											</div>
											<?php if (1 == $op_precious_operations_tab3_button_enable) { ?>
											<div class="pdf-link">
												<?php 
												if (!empty($op_precious_operations_tab3_button_icon)) {?>
												<i class="<?php echo $op_precious_operations_tab3_button_icon;?>"></i>
												<?php }else{?>
												<img src="http://via.placeholder.com/32x32" alt="pdf">
												<?php }?>

												<a href="<?php echo $op_precious_operations_tab3_button_url; ?>">
													<?php 
													if (!empty($op_precious_operations_tab3_button_label)){
														echo $op_precious_operations_tab3_button_label; 
													}else{
														echo "PDF Link";
													}
													?>
												</a>
											</div>
											<?php }?>
											<div class="row">

												<?php
												if( !empty( $op_precious_operations_tab3_image )):
													$op_precious_operations_tab3_images = explode(",", $op_precious_operations_tab3_image);
												foreach ($op_precious_operations_tab3_images as $value):
													$images = $value;
												$op_precious_operations_tab3_image     = wp_get_attachment_image_src( $images , 'full' );
												?>
												<div class="col-4">
													<img src="<?php echo $op_precious_operations_tab3_image[0] ?>" alt="">
												</div>



												<?php
												endforeach;
												endif; 
												?>												
											</div>

										</div>
									</li>
									<li>
										<div class="operation-item">

											<div class="entry-header">
												<h5 class="entry-title"><?php echo $op_precious_operations_tab4_title ?></h5>
											</div>
											<div class="entry-content">
												<?php echo wpautop($op_precious_operations_tab4_content); ?>
											</div>
											<?php if (1 == $op_precious_operations_tab4_button_enable) { ?>
											<div class="pdf-link">
												<?php 
												if (!empty($op_precious_operations_tab4_button_icon)) {?>
												<i class="<?php echo $op_precious_operations_tab4_button_icon;?>"></i>
												<?php }else{?>
												<img src="http://via.placeholder.com/32x32" alt="pdf">
												<?php }?>

												<a href="<?php echo $op_precious_operations_tab4_button_url; ?>">
													<?php 
													if (!empty($op_precious_operations_tab4_button_label)){
														echo $op_precious_operations_tab4_button_label; 
													}else{
														echo "PDF Link";
													}
													?>
												</a>
											</div>
											<?php }?>
											<figure>
												<?php
												if( !empty( $op_precious_operations_tab4_image )):
													$op_precious_operations_tab4_images = explode(",", $op_precious_operations_tab4_image);
												foreach ($op_precious_operations_tab4_images as $value):
													$images = $value;
												$op_precious_operations_tab4_image     = wp_get_attachment_image_src( $images , 'full' );
												?>
												<img src="<?php echo $op_precious_operations_tab4_image[0] ?>" alt="">
												<?php
												endforeach;
												endif; 
												?>
											</figure>
										</div>
									</li>

								</ul>
							</div>

						</div>
						<div id="secondary" class="col-4 side-bar">
							<aside class="sidebar">
								<?php get_sidebar('right'); ?>      
							</aside>
						</div>
					</div>
				</section>
			</main>
		</div>

		

	</div>



	<!-- #content -->
	<?php get_footer(  );