<?php 
/*
Template Name: Product and sales Template
*/
get_header();

$rws_pands_section         = get_post_meta( get_the_id (), 'rws_pands_section', true );

$ps_news_letter_enable              = $rws_pands_section['ps_news_letter_enable'];
?>

<!-- .page-title-box -->
<div id="content" class="site-content">
  <div id="primary" class="content-area">
    <main id="main" class="site-main">
      <section class="product-section">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <header class="entry-header">
                <h3 class="entry-title">    
                  <?php the_title(); ?>
                </h3>
              </header>
              <div class="product-sub-heading">
                <?php the_content(); ?>

              </div>
              <!-- .sustainable-sub-heading -->
              <div class="product-post">
                <div class="row">
                  <?php
                  $args = new WP_Query( array(
                    'post_type' => 'product',
                    'posts_per_page' => 4,
                    )                       
                  );
                  if ( have_posts() ) {
                    while ( $args->have_posts() ) :
                      $args->the_post();
                    ?>

                    <div class="col-6">
                      <article>
                        <div class="article-img">
                          <?php 
                          if (has_post_thumbnail( )) {?>
                          <img src="<?php the_post_thumbnail_url('home-post-size');?>" alt="">

                          <?php }else{?>
                          <img src="http://via.placeholder.com/360x358" alt="">

                          <?php }
                          ?>

                        </div>
                        <div class="article-post">
                          <div class="entry-header">
                            <h5 class="entry-title">
                              <a href="<?php echo the_permalink(); ?>
                                "> 
                                <?php the_title(); ?>
                              </a>    
                            </h5>
                          </div>
                          <div class="entry-content">
                            <?php the_excerpt(); ?>

                          </div>
                          <a href="<?php echo the_permalink(); ?>"> Read More</a>
                        </div>
                      </article>
                    </div>
                    <?php 
                    endwhile;
                  } 
                  ?>


                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- .sustainable-section -->
    </main>
    <!-- #main -->
  </div>
  <!-- #primary -->

  <?php
  $enable_news_letter = cs_get_option( 'enable_news_letter' );
  if( 1 == $enable_news_letter ):
    ?>
  <?php if( 1 == $ps_news_letter_enable ): ?>
    <!-- .site-news -->
    <section class="signup-section" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/images/img/subscribe-bg.jpg) no-repeat; background-size: cover">
      <div class="container">
        <div class="signup-content">
          <header class="section-header">
            <h2 class="section-title">email newsletter</h2>
          </header>
          <?php
          $news_letter_form = cs_get_option( 'news_letter_form' );
          echo do_shortcode( $news_letter_form) ?>
        </div>
      </div>
    </section>
    <!-- . signup-section -->
  <?php endif; ?>
<?php endif; ?>
</div>
<!-- #content -->

<?php 
get_footer();