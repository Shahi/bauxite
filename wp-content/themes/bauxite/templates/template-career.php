<?php 
/*
Template Name: Career Template
*/
get_header();
global $rws_options;
$careerpage_meta                    = get_post_meta( get_the_id (), 'rws_career_section', true );
$rws_cp_news_letter_section         = get_post_meta( get_the_id (), 'rws_cp_news_letter_section', true );

$cp_news_letter_enable              = $rws_cp_news_letter_section['cp_news_letter_enable'];

$careerp_work_enable                = $careerpage_meta['careerp_work_enable'];
$careerp_work_title                 = $careerpage_meta['careerp_work_title'];
$careerp_work_content               = $careerpage_meta['careerp_work_content'];
$careerp_work_image                 = $careerpage_meta['careerp_work_image'];
$careerp_work_image                 = wp_get_attachment_image_src( $careerp_work_image , 'full' );


$careerp_intership_enable           = $careerpage_meta['careerp_intership_enable'];
$careerp_intership_title            = $careerpage_meta['careerp_intership_title'];
$careerp_intership_content          = $careerpage_meta['careerp_intership_content'];
$careerp_intership_image            = $careerpage_meta['careerp_intership_image'];
$careerp_intership_image            = wp_get_attachment_image_src( $careerp_intership_image , 'full' );


$careerp_apply_form_enable          = $careerpage_meta['careerp_apply_form_enable'];
$careerp_apply_form                 = $careerpage_meta['careerp_apply_form'];

$careerp_opportunity_section_enable = $careerpage_meta['careerp_opportunity_section_enable'];


$rws_cp_news_letter_section         = get_post_meta( get_the_id (), 'rws_cp_news_letter_section', true );
$cp_news_letter_enable              = $rws_cp_news_letter_section['cp_news_letter_enable'];

?>
<div id="content" class="site-content">
  <div id="primary" class="content-area">
    <main id="main" class="site-main">

      <?php if ($careerp_work_enable == 1) : ?>

        <section class="career-section">
          <div class="container">
            <div class="row">
             <div class="col-9">
               <div class="working-content">

                <header class="entry-header">
                  <h3 class="entry-title">  

                    <?php if (!empty($careerp_work_title)) :
                    echo $careerp_work_title;
                    endif; ?>

                  </h3>
                </header>
                <div class="entry-content">

                  <?php if (!empty($careerp_work_content)) :
                  echo wpautop($careerp_work_content);
                  endif; ?>

                </div>
                <!--<img src="img/team-01.jpg">-->
              </div>
            </div>
          </div>
        </div>
        <figure>
          <img src="<?php echo $careerp_work_image[0] ?>" alt="number-counter">
        </figure>
      </section>

    <?php  endif;  ?>

    <?php 
    if ($careerp_intership_enable == 1) {?>
    <section class="internship-section">
      <div class="container">
        <div class="row">
          <div class="col-4">
            <header class="entry-header">
             <h3 class="entry-title"> 

              <?php if (!empty($careerp_intership_title)) :
              echo $careerp_intership_title;
              endif; ?>

            </h3>        
          </header>

        </div>
        <div class="col-8">
          <div class="entry-content">

            <?php if (!empty($careerp_intership_content)) :
            echo  wpautop($careerp_intership_content);
            endif;
            ?> 

          </div>
        </div>
      </div>
    </div>
  </section>

  <img src="<?php 
  if (!empty($careerp_intership_image)) {
    echo $careerp_intership_image[0];
  }else{
    echo "http://via.placeholder.com/285x410";
  }
  ?>" alt="internship-img">
  <?php } ?> 
  <!-- .internship-section -->

  <?php if ($careerp_opportunity_section_enable == 1) {?>

  <section class="career-list-section">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <header class="entry-header">
            <h3 class="entry-title">

              <?php echo get_cat_name(5); ?>

            </h3>
          </header>
          <div class="entry-content">

            <?php echo category_description(5); ?>

          </div>
          <div class="job-listing">
            <div class="row">

              <?php
              $args = new WP_Query( array(
                'post_type' => 'post',
                'posts_per_page' => 4,
                'cat' => 5 ,
                )                      
              );

              if ( have_posts() ) {
                while ( $args->have_posts() ) :
                  $args->the_post();
                ?>

                <div class="col-6">
                  <div class="job-listing-item">
                    <div class="listing-item-img">
                      <img src="<?php the_post_thumbnail_url();?>" alt="job-listing-img">
                    </div>
                    <!-- .listing-item-img -->
                    <div class="listing-content">
                      <header class="entry-header">
                        <h5 class="entry-title"> 
                          <a href="<?php echo the_permalink(); ?>">

                            <?php the_title(); ?>

                          </a>
                        </h5>
                      </header>

                      <div class="entry-content">
                        <?php the_excerpt(); ?>

                      </div>

                    </div>
                    <!-- .listing-content -->
                  </div>
                  <!-- .job-listing-item -->
                </div>

              <?php endwhile;
              wp_reset_postdata();
              wp_reset_query();
            } ?>

          </div>
        </div>
        <!-- .job-listing -->
      </div>

    </div>
  </div>
</section>
<?php }
?>
<!-- .career-list-section -->
<?php if ($careerp_apply_form_enable == 1) { ?>

<section class="apply-form">
  <div class="container">
    <div class="row">
      <div class="col-8">
        <div class="apply-form-item">

          <div class="apply-form-wrapper">
            <header class="entry-header">
              <h4 class="entry-title">Apply Form</h4>
            </header>
            <div role="form" class="wpcf7" id="wpcf7-f98-p2-o2" lang="en-US" dir="ltr">
              <div class="screen-reader-response"></div>

              <?php echo do_shortcode($careerp_apply_form); ?>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php } ?>
<!-- .apply-form -->

</main>
<!-- #main -->
</div>
<!-- #primary -->
<?php 
$enable_news_letter = cs_get_option( 'enable_news_letter' );
if( 1 == $enable_news_letter ):
  if( 1 == $cp_news_letter_enable ): ?>
<!-- .site-news -->
<section class="signup-section" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/images/img/subscribe-bg.jpg) no-repeat; background-size: cover">
  <div class="container">
    <div class="signup-content">
      <header class="section-header">
        <h2 class="section-title">
          email newsletter
        </h2>
      </header>

      <?php  $news_letter_form = cs_get_option( 'news_letter_form' );
      echo do_shortcode( $news_letter_form) ?>

    </div>
  </div>
</section>
<!-- . signup-section -->
<?php endif; ?>
<?php endif; ?>

<!-- . signup-section -->
</div>
<!-- #content -->

<?php 
get_footer();