jQuery(document).ready(function() {

  jQuery('#page_template').change(function() {
    //jQuery('#common_heading').hide();
    jQuery('#rws_operation_section').hide();
    jQuery('#rws_operation_precious_section').hide();

    jQuery('#rws_operation_mining_news_section').hide();
    jQuery('#rws_operation_precious_news_section').hide();

    jQuery('#rws_homepage_section').hide();
    jQuery('#rws_homepage_news_letter_section').hide();

    jQuery('#rws_contact_section').hide();

    jQuery('#rws_about_section').hide();
    jQuery('#rws_about_news_letter_section').hide();

    

    jQuery('#rws_career_section').hide();
    jQuery('#rws_cp_news_letter_section').hide();

    
    jQuery('#rws_np_news_letter_section').hide();

    jQuery('#rws_sustainable_section').hide();
    jQuery('#rws_sp_news_letter_section').hide();
    
    jQuery('#rws_contact_breadcrumb_section').hide();

    jQuery('#rws_pands_section').hide();



    switch (jQuery(this).val()) {
      case 'templates/template-homepage.php':
      //jQuery('#common_heading').show();
      jQuery('#rws_homepage_section').show();      
      jQuery('#rws_homepage_news_letter_section').show();      
      break;


      case 'templates/template-contact.php':
      jQuery('#rws_contact_section').show();      
      jQuery('#rws_contact_breadcrumb_section').show();      
      break;

      case 'templates/template-operations-mining.php':
      jQuery('#rws_operation_section').show();      
      jQuery('#rws_operation_mining_news_section').show();      
      break;

      case 'templates/template-operations-precious.php':
      jQuery('#rws_operation_precious_section').show();
      jQuery('#rws_operation_precious_news_section').show();      
      break;

      case 'templates/template-about.php':
      jQuery('#rws_about_section').show();      
      jQuery('#rws_about_news_letter_section').show();      
      break;


      case 'templates/template-career.php':
      jQuery('#rws_career_section').show();      
      jQuery('#rws_cp_news_letter_section').show();      
      break;

      case 'templates/template-news.php':
      jQuery('#rws_np_news_letter_section').show();      
      break;  

      case 'templates/template-sustainable-development.php':
      jQuery('#rws_sustainable_section').show();      
      jQuery('#rws_sp_news_letter_section').show();      
      break;

      case 'templates/template-product-and-sales.php':
      jQuery('#rws_pands_section').show();      
      break;


      default:
      jQuery('#notice_section').show();
      break;
    }
  }).change();

  jQuery(function($)
  {
    function my_check_categories()
    {
      $('#rws_cp_vacancy_detail_section').hide();

      $('#categorychecklist input[type="checkbox"]').each(function(i,e)
      {

        var id = $(this).attr('id').match(/-([0-9]*)$/i);
        id = (id && id[1]) ? parseInt(id[1]) : null ;
        if ($.inArray(id, [5]) > -1 && $(this).is(':checked'))
        {

          $('#rws_cp_vacancy_detail_section').show();
        }
      });
    }
    $('#categorychecklist input[type="checkbox"]').live('click', my_check_categories);
    my_check_categories();
  });

});