jQuery(document).ready(function () {

    $('#popup-content-wrapper').popup({
        transition: 'all 0.3s',
        scrolllock: true,
    });

    //stcky-sidebar js
    jQuery(document).ready(function () {
        jQuery('.site-news .col-5, .site-news .col-7').theiaStickySidebar({
            // Settings
            additionalMarginTop: 60
        });

    });

    //meanmenu js
    jQuery('header nav').meanmenu();
    //banner js
    jQuery('.site-banner .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        autoplay: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })
    //accordion js
    var accordion = {
        init: function () {
            $(".accordion li:not(.active) .entry-content").hide();
            this.bindUIActions();
        },
        bindUIActions: function () {
            $("body").on("click", ".accordion li", accordion.handlers.accordionToggle);
        },
        handlers: {
            accordionToggle: function () {
                "use strict";
                var $li = $(this),
                    isThisActive = $li.hasClass("active"),
                    $active;
                if (isThisActive) {
                    $li.find(".btnAccordionToggle").text("+");
                    $li.removeClass("active").children(".entry-content").hide("fast");
                } else {
                    $active = $li.siblings(".active");
                    $active.find(".btnAccordionToggle").text("+");
                    $active.removeClass("active").children(".entry-content").hide("fast");
                    $li.addClass("active").children(".entry-content").show("fast");
                    $li.find(".btnAccordionToggle").text("-");
                }
            }
        }
    }

    $(document).ready(function () {
        accordion.init();
    });


    //stcky-navbar js
    $(function () {

        var menu = $('#menu'),
            pos = menu.offset();

        $(window).scroll(function () {
            if ($(this).scrollTop() > pos.top + menu.height() && menu.hasClass('full-nav')) {
                menu.fadeOut('fast', function () {
                    $(this).removeClass('full-nav').addClass('fixed').fadeIn('fast');
                });
            } else if ($(this).scrollTop() <= pos.top && menu.hasClass('fixed')) {
                menu.fadeOut('fast', function () {
                    $(this).removeClass('fixed').addClass('full-nav').fadeIn('fast');
                });
            }
        });

    });

    //pagination js
    $('a').on('click', function (e) {
        $(e).preventDefault();
    });

    $('.pagination li').on('click', function () {

        $(this).siblings().removeClass('active');
        $(this).addClass('active');

    })
    //Tab js
    $(document).ready(function () {
        //Horizontal Tab
        $('#parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        // Child Tab
        $('#ChildVerticalTab_1').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true,
            tabidentify: 'ver_1', // The tab groups identifier
            activetab_bg: '#c1c1c1', // background color for active tabs in this group
            inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
            active_border_color: '#c1c1c1', // border color for active tabs heads in this group
            active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
        });

        //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
    //Team js
    jQuery('.site-team .owl-carousel').owlCarousel({
        loop: true,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            767: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    })


});
