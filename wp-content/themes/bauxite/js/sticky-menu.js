$(function () {

    var menu = $('#menu'),
        pos = menu.offset();

    $(window).scroll(function () {
        if ($(this).scrollTop() > pos.top + menu.height() && menu.hasClass('full-nav')) {
            menu.fadeOut('fast', function () {
                $(this).removeClass('full-nav').addClass('fixed').fadeIn('fast');
            });
        } else if ($(this).scrollTop() <= pos.top && menu.hasClass('fixed')) {
            menu.fadeOut('fast', function () {
                $(this).removeClass('fixed').addClass('full-nav').fadeIn('fast');
            });
        }
    });

});
