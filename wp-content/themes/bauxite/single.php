<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package RWS_AEP
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="single-page archive-page-section">
				<div class="container">
					<div class="row">
						<?php
						while ( have_posts() ) : the_post();
						?>
						<div class="col-9">
							<header class="entry-header">
								<?php
								the_title( '<h3 class="entry-title">', '</h3>' );
								?>
							</header> 
							<?php
							if ( in_category( 'career-opportunity' ) ) {

							}else{?>

							<ul>
								<li>	
									<span class="byline"> 
										<span class="author vcard">
											<i class="fa fa-user"></i> 
											<a class="url fn n" href="https://demo.rigorousthemes.com/blog-lite/author/superuser/">
												<?php the_author( ); ?>
											</a>
										</span>
									</span>
								</li>
								<li>	
									<span class="posted-on">
										<i class="fa fa-calendar-o"></i> 
										<a href="https://demo.rigorousthemes.com/blog-lite/the-adventure-moment-in-the-mt-everest/" rel="bookmark">
											<time class="entry-date published" datetime="2016-07-11T11:00:52+00:00">
												<?php echo get_the_date( 'd M Y' ) ?>
											</time>

										</a>
									</span>
								</li>
							</ul>
							<?php									} ?>
							
							<figure>
								<?php 
								if (has_post_thumbnail( )) {?>
								<img src="<?php the_post_thumbnail_url();?>">
								<?php }
								?>
							</figure>
							<?php
							$post = $wp_query->post;

							if ( in_category( 'career-opportunity' ) ) {
								$rws_cp_vacancy_detail_section		= get_post_meta( get_the_id (), 'rws_cp_vacancy_detail_section', true );
								$rws_cp_vacancy_position_section	= $rws_cp_vacancy_detail_section['rws_cp_vacancy_position_section'];
								$rws_cp_vacancy_report_section		= $rws_cp_vacancy_detail_section['rws_cp_vacancy_report_section'];
								$rws_cp_vacancy_contract_section	= $rws_cp_vacancy_detail_section['rws_cp_vacancy_contract_section'];
								$rws_cp_vacancy_salary_section		= $rws_cp_vacancy_detail_section['rws_cp_vacancy_salary_section'];
								?>
								<div class="job-detail">
									<p>
										<strong>Position: </strong>
										<?php echo $rws_cp_vacancy_position_section;?>
									</p>
									<p>
										<strong>Report: </strong>
										<?php echo $rws_cp_vacancy_report_section;?>
									</p>
									<p>
										<strong>Contract: </strong>
										<?php echo $rws_cp_vacancy_contract_section;?>
									</p>
									<p>
										<strong>Salary: </strong>
										<?php echo $rws_cp_vacancy_salary_section;?>
									</p>
								</div>
								<?php } 
								else {

								}
								?>
								<?php
								if ( in_category( 'career-opportunity' ) ) 
								{
									?>
									<div class="job-description">
										<?php the_excerpt(); ?>
										<?php the_content(); ?>
									</div><!-- .entry-content -->
									<?php 
								} else{?>


								<div class="entry-content">
									<?php the_excerpt(); ?>
									<?php the_content(); ?>
								</div><!-- .entry-content -->

								<?php }
								?>

								<?php
								$post = $wp_query->post;
								if ( in_category( 'career-opportunity' ) ) {

								} 
								else {
									if ( comments_open() || get_comments_number() ) :
										comments_template();
									endif;							
								}
								?>

							</div>
						<?php endwhile;?>

					</div>
				</div>

			</section>

		</main>
	</div>
</div>

<?php
get_footer();