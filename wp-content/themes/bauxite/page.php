<?php
get_header(); 

?>
<div id="content" class="site-content">
	<section class="default-section">
		<div class="container">
			<div class="row">
				<div id="primary" class="col-8 content-area">
					<?php
					if ( have_posts() ):
						while ( have_posts() ) : the_post();

					?>
					<?php the_title('<h2>','</h2>'); ?>
					<?php 
					if(has_post_thumbnail()) {
						the_post_thumbnail();
					}
					the_content(); ?>
					<?php
					endwhile; 
					endif;
					?>
				</div>
				<!-- #primary -->
				<div id="secondary" class="col-4 side-bar">
					<aside class="sidebar">
						<?php get_sidebar('right'); ?>			
					</aside>
				</div>
			</div>
		</div>
	</section>
</div>
<?php
get_footer (); 