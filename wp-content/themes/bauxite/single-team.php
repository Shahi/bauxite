<?php
get_header(); 

if ( have_posts() ):
	while ( have_posts() ) : the_post();
$rws_cpt_team_meta 				= get_post_meta( get_the_id (), 'rws_cpt_team_section', true );
$about_team_post				= $rws_cpt_team_meta['about_team_post'];
$about_team_fb_contact			= $rws_cpt_team_meta['about_team_fb_contact'];
$about_team_instagram_contact	= $rws_cpt_team_meta['about_team_instagram_contact'];
$about_team_twitter_contact		= $rws_cpt_team_meta['about_team_twitter_contact'];
$about_team_Linked_in_contact	= $rws_cpt_team_meta['about_team_Linked_in_contact'];
?>

<div id="content" class="site-content">

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="director-single-section">
				<div class="container">
					<div class="row">
						<div class="col-4">
							<img src="<?php if (has_post_thumbnail( )) {
								the_post_thumbnail_url('about-post-size');
							}else{
								echo "http://via.placeholder.com/358x350";
							}
							?>" alt="team-img" />
						</div>
						<div class="col-8">
							<header class="entry-header">
								<h3 class="entry-title">
									<?php the_title(); ?>
									<span class="team-designation"><?php echo $about_team_post; ?></span>
								</h3>
							</header>
							<ul>
								<?php if (!empty($about_team_fb_contact)) {?>
								<li>
									<a href="<?php echo $about_team_fb_contact; ?>">
										<i class="fa fa-facebook" aria-hidden="true"></i>
									</a>
								</li>
								<?php } ?>
								<?php if (!empty($about_team_instagram_contact)) {?>
								<li>
									<a href="<?php echo $about_team_instagram_contact; ?>">
										<i class="fa fa-instagram" aria-hidden="true"></i>
									</a>
								</li>
								<?php } ?>
								<?php if (!empty($about_team_twitter_contact)) {?>

								<li>
									<a href="<?php echo $about_team_twitter_contact; ?>">
										<i class="fa fa-twitter" aria-hidden="true"></i>
									</a>
								</li>
								<?php } ?>

								<?php if (!empty($about_team_Linked_in_contact)) {?>
								<li>
									<a href="<?php echo $about_team_Linked_in_contact; ?>">
										<i class="fa fa-linkedin" aria-hidden="true"></i>
									</a>
								</li>
								<?php } ?>


							</ul>
							<div class="entry-content">
								<?php the_content();?>
							</div>
							

							
						</div>
						
					</div>
				</div>
				
			</section>
		</main>
	</div>
</div>

<?php 
endwhile;
endif;
get_footer (); 