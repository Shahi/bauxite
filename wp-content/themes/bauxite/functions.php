<?php
/* rws theme supports */
if(!function_exists( 'rws_theme_supports' ) ):

	function rws_theme_supports(){
		// for rss feeds in header
		add_theme_support( 'automatic-feed-links' );
		//generates the title tag
		add_theme_support( 'title-tag' );
		// support for the post thumbnails for posts and pages
		add_theme_support( 'post-thumbnails' );
		// menu for theme
		add_theme_support('menus');

		add_theme_support( 'custom-logo' );

		//registering the menus for the theme
		register_nav_menus( array(
			'primary' 			=> __( 'Primary Menu' ),
			'footer-links' 		=> __( 'Footer Links' ),
			) );

		/*=======================================
		=            thumbnail sizes            =
		=======================================*/
		add_image_size( 'side-image-size', 285, 410, true );
		add_image_size( 'news-post-size', 254.98, 253.53, true );
		add_image_size( 'home-post-size', 360, 358, true );
		add_image_size( 'about-post-size', 358, 350, true );
		// add_image_size( 'learn-more-legendary-events', 140, 112, true );

		// Switch default core markup for search form, comment form, and comments to output valid HTML5.
		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
			) );

		/* Global option for codestar */
		global $rws_options;
		// get all values from theme options
		$rws_options 	= cs_get_all_option();
		/* End of Global option */

	}
	add_action( 'after_setup_theme','rws_theme_supports' );
	endif;
	/* End of rws theme supports */


/**
 * Enqueue styles and scripts
 * @return void
 */
function rws_enqueue_scripts(){
	global $rws_options;
	wp_enqueue_style( 'fontawesome.css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',array(), '1.0.0', 'all' );
		// wp_enqueue_style( 'https://use.fontawesome.com/releases/v5.0.6/css/all.css',array(), '1.0.0', 'all' );

	//adding google fonts
	$query_args = array(
		'family' => 'Montserrat:200,300,400,500,600,700,800,900|Oswald:300,400,500,600,700|Open+Sans',
		);
	wp_register_style( 'rws-google_fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array() , null );
	wp_enqueue_style( 'rws-style-css', get_stylesheet_uri(), array('rws-google_fonts'), '1.0.0', 'all' );

	//enqueing styles
	wp_enqueue_style( 'owl.theme.css', get_template_directory_uri().'/css/owl.theme.default.min.css',array(), '1.0.0', 'all' );
	wp_enqueue_style( 'owl.carousel.css', get_template_directory_uri().'/css/owl.carousel.min.css',array(), '1.0.0', 'all' );
	wp_enqueue_style( 'meanmenu.css', get_template_directory_uri().'/css/meanmenu.css',array(), '1.0.0', 'all' );
	// wp_enqueue_style( 'style.css', get_template_directory_uri().'/style.css',array(), '1.0.0', 'all' );

	//enqueing scripts
	wp_enqueue_script( 'jquery.js', 'https://code.jquery.com/jquery-3.3.1.min.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'owl.carousel.js', get_template_directory_uri().'/js/owl.carousel.min.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'jquery.meanmenu.min.js', get_template_directory_uri().'/js/jquery.meanmenu.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'ResizeSensor.js', get_template_directory_uri().'/js/ResizeSensor.min.js', array( 'jquery' ), '1.0.0', true );
	// wp_enqueue_script( 'theia.sticky.js', get_template_directory_uri().'/js/theia-sticky-sidebar.min.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'rws-tabs-js', get_template_directory_uri().'/js/easyResponsiveTabs.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'pop-up-js', get_template_directory_uri().'/js/jquery.popupoverlay.js', array( 'jquery' ), '1.0.0', true );
	
	wp_enqueue_script( 'rws-custom-js', get_template_directory_uri().'/js/custom.js', array( 'jquery' ), '1.0.0', true );

}
add_action('wp_enqueue_scripts','rws_enqueue_scripts');
/* End of Enqueue styles and scripts */

//admin area scripts
function rws_admin_custom_scripts() {
	wp_enqueue_script( 'metaboxs-switch', get_template_directory_uri() . '/js/metaswitch.js', '', '1.0.0', true );
}
add_action( 'admin_enqueue_scripts', 'rws_admin_custom_scripts' );

function rws_artisthub_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'rws_artisthub_content_width', 640 );
}
add_action( 'after_setup_theme', 'rws_artisthub_content_width', 0 );

/**
 * Register Widget
 */
add_action( 'widgets_init', 'rws_register_sidebars' );
function rws_register_sidebars() {
	
	//widget support for a right sidebar
	register_sidebar(array(
		'name' => 'Right SideBar',
		'id' => 'right-sidebar',
		'description' => 'Widgets in this area will be shown on the right-hand side.',
		'before_widget' => '<div id="%1$s" class="widget">',
		'after_widget'  => '<div class="clear"></div></div>',
		'before_title' => '<h3 class="widget-title column-title">',
		'after_title' => '</h3>'
		));
	register_sidebar(array(
		'name' => 'News Archives',
		'id' => 'news_archive',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
		'order' => 'desc',
		)); 
	
}


/* Other Dependencies */
require_once(get_template_directory().'/inc/init.php');

/* End of Other Dependencies */


function addNotificationMessage($message, $code=400){
	$_SESSION['error'][] = array($message, $code);
}

function getNotificationMessages($remove = true){
	$temp = $_SESSION['error'];
	if($remove){
		$_SESSION['error'] = array();
	}
	return $temp;
}

//for pagination
function rws_custom_pagination( ){
	global $wp_query;
	$pagination_posts = paginate_links (array('base' => str_replace( PHP_INT_MAX, '%#%', esc_url( get_pagenum_link( PHP_INT_MAX ) ) ),
		'total'         => $wp_query->max_num_pages,
		'mid_size'      => 2,
		'type'          => 'array',
		'prev_text' => __('<< '),
		'next_text' => __(' >>'),
		) );
		if ( ! empty( $pagination_posts ) ) { ?>
		<ul class="pagination">
			<?php foreach ( $pagination_posts as  $key => $page_link ) { ?>
			<li<?php if ( strpos( $page_link, 'current' ) !== false ) { echo ' class="active"'; } ?>>
			<?php echo $page_link ?></li>
			<?php } ?>
		</ul>
		<?php }
	}
	  	//pagination function starts here
	function custom_pagination($numpages = '', $pagerange = '', $paged = '')
	{
		if (empty($pagerange)) {
			$pagerange = 2;
		}
		if (empty($paged)) {
			$paged = 1;
		}
		if ($numpages == '') {
			global $wp_query;
			$numpages = $wp_query->max_num_pages;
			if (!$numpages) {
				$numpages = 1;
			}
		}
		$pagination_args = array(
			'base' => get_pagenum_link(1) . '%_%',
			'format' => 'page/%#%',
			'total' => $numpages,
			'current' => $paged,
			'show_all' => False,
			'end_size' => 1,
			'mid_size' => $pagerange,
			'prev_next' => True,
			'prev_text' => __('<< '),
			'next_text' => __(' >>'),
			'type' => 'array',
			'add_args' => false,
			'add_fragment' => ''
			);
		$paginate_links = paginate_links($pagination_args);
		
		if ( ! empty( $paginate_links ) ) { ?>
		<ul class="pagination">
			<?php foreach ( $paginate_links as  $key => $page_link ) { ?>
			<li<?php if ( strpos( $page_link, 'current' ) !== false ) { echo ' class="active"'; } ?>>
			<?php echo $page_link ?></li>
			<?php } ?>
		</ul>
		<?php }
	}

//breadcrumb function
	function get_breadcrumb()
	{
    // Set variables for later use
		$home_link        = home_url('/');
		$home_text        = __( 'Home' );
		$link_before      = '<li>';
		$link_after       = '</li>';
		$link             = $link_before . '<a'. ' href="%1$s">%2$s</a>' . $link_after;
    $delimiter        = '<li>/</li> ';              // Delimiter between crumbs
    $before           = '<li>'; // Tag before the current crumb
    $after            = '</li>';                // Tag after the current crumb
    $page_addon       = '';                       // Adds the page number if the query is paged
    $breadcrumb_trail = '';
    $category_links   = '';

    /** 
     * Set our own $wp_the_query variable. Do not use the global variable version due to 
     * reliability
     */
    $wp_the_query   = $GLOBALS['wp_the_query'];
    $queried_object = $wp_the_query->get_queried_object();

    // Handle single post requests which includes single pages, posts and attatchments
    if ( is_singular() ) 
    {
        /** 
         * Set our own $post variable. Do not use the global variable version due to 
         * reliability. We will set $post_object variable to $GLOBALS['wp_the_query']
         */
        $post_object = sanitize_post( $queried_object );

        // Set variables 
        $title          = apply_filters( 'the_title', $post_object->post_title );
        $parent         = $post_object->post_parent;
        $post_type      = $post_object->post_type;
        $post_id        = $post_object->ID;
        $post_link      = $before . $title . $after;
        $parent_string  = '';
        $post_type_link = '';

        if ( 'post' === $post_type ) 
        {
            // Get the post categories
        	$categories = get_the_category( $post_id );
        	if ( $categories ) {
                // Lets grab the first category
        		$category  = $categories[0];

        		$category_links = get_category_parents( $category, true, $delimiter );
        		// $category_links = str_replace( '<a',   $link_before . '<a' . $link_attr, $category_links );
        		// $category_links = str_replace( '<a',   $link_before . '<a' . $link_attr, $category_links );
        		$category_links = '<li>' . $category_links . '</li>';
        	}
        }
        if ( 'product' === $post_type ) 
        {
            // Get the post categories
        	$categories = get_the_category( $post_id );
        	if ( $categories ) {
                // Lets grab the first category
        		$category  = $categories[0];


        		$post_type_link   = sprintf( $link, $archive_link, 'Product And Sales' );
        	}
        }


        if ( !in_array( $post_type, ['post', 'page', 'attachment','product'] ) )
        {
        	$post_type_object = get_post_type_object( $post_type );
        	$archive_link     = esc_url( get_post_type_archive_link( $post_type ) );

        	$post_type_link   = sprintf( $link, $archive_link, $post_type_object->labels->singular_name );
        }

        // Get post parents if $parent !== 0
        if ( 0 !== $parent ) 
        {
        	$parent_links = [];
        	while ( $parent ) {
        		$post_parent = get_post( $parent );

        		$parent_links[] = sprintf( $link, esc_url( get_permalink( $post_parent->ID ) ), get_the_title( $post_parent->ID ) );

        		$parent = $post_parent->post_parent;
        	}

        	$parent_links = array_reverse( $parent_links );

        	$parent_string = implode( $delimiter, $parent_links );
        }

        // Lets build the breadcrumb trail
        if ( $parent_string ) {
        	$breadcrumb_trail = $parent_string . $delimiter . $post_link;
        } else {
        	$breadcrumb_trail = $post_link;
        }

        if ( $post_type_link )
        	$breadcrumb_trail = $post_type_link . $delimiter . $breadcrumb_trail;

        if ( $category_links )
        	$breadcrumb_trail = $category_links . $breadcrumb_trail;
    }

    // Handle archives which includes category-, tag-, taxonomy-, date-, custom post type archives and author archives
    if( is_archive() )
    {
    	if (    is_category()
    		|| is_tag()
    		|| is_tax()
    		) {
            // Set the variables for this section
    		$term_object        = get_term( $queried_object );
    	$taxonomy           = $term_object->taxonomy;
    	$term_id            = $term_object->term_id;
    	$term_name          = $term_object->name;
    	$term_parent        = $term_object->parent;
    	$taxonomy_object    = get_taxonomy( $taxonomy );
    	$current_term_link  = '<li>' . $term_name . '</li>';
    	$parent_term_string = '';

    	if ( 0 !== $term_parent )
    	{
                // Get all the current term ancestors
    		$parent_term_links = [];
    		while ( $term_parent ) {
    			$term = get_term( $term_parent, $taxonomy );

    			$parent_term_links[] = sprintf( $link, esc_url( get_term_link( $term ) ), $term->name );

    			$term_parent = $term->parent;
    		}

    		$parent_term_links  = array_reverse( $parent_term_links );
    		$parent_term_string = implode( $delimiter, $parent_term_links );
    	}

    	if ( $parent_term_string ) {
    		$breadcrumb_trail = $parent_term_string . $delimiter . $current_term_link;
    	} else {
    		$breadcrumb_trail = $current_term_link;
    	}

    } elseif ( is_author() ) {

    	$breadcrumb_trail = __( 'Author archive for ') .  $before . $queried_object->data->display_name . $after;

    } elseif ( is_date() ) {
            // Set default variables
    	$year     = $wp_the_query->query_vars['year'];
    	$monthnum = $wp_the_query->query_vars['monthnum'];
    	$day      = $wp_the_query->query_vars['day'];

            // Get the month name if $monthnum has a value
    	if ( $monthnum ) {
    		$date_time  = DateTime::createFromFormat( '!m', $monthnum );
    		$month_name = $date_time->format( 'F' );
    	}

    	if ( is_year() ) {

    		$breadcrumb_trail = $before . $year . $after;

    	} elseif( is_month() ) {

    		$year_link        = sprintf( $link, esc_url( get_year_link( $year ) ), $year );

    		$breadcrumb_trail = $year_link . $delimiter . $before . $month_name . $after;

    	} elseif( is_day() ) {

    		$year_link        = sprintf( $link, esc_url( get_year_link( $year ) ),             $year       );
    		$month_link       = sprintf( $link, esc_url( get_month_link( $year, $monthnum ) ), $month_name );

    		$breadcrumb_trail = $year_link . $delimiter . $month_link . $delimiter . $before . $day . $after;
    	}

    } elseif ( is_post_type_archive() ) {

    	$post_type        = $wp_the_query->query_vars['post_type'];
    	$post_type_object = get_post_type_object( $post_type );

    	$breadcrumb_trail = $before . $post_type_object->labels->singular_name . $after;

    }
}   

    // Handle the search page
if ( is_search() ) {
	$breadcrumb_trail = __( 'Search query for: ' ) . $before . get_search_query() . $after;
}

    // Handle 404's
if ( is_404() ) {
	$breadcrumb_trail = $before . __( 'Error 404' ) . $after;
}

    // Handle paged pages
if ( is_paged() ) {
	$current_page = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : get_query_var( 'page' );
	$page_addon   = $before . sprintf( __( ' ( Page %s )' ), number_format_i18n( $current_page ) ) . $after;
}

$breadcrumb_output_link  = '';
$breadcrumb_output_link .= '<div class="breadcrumb">';
if (    is_home()
	|| is_front_page()
	) {
        // Do not show breadcrumbs on page one of home and frontpage
	if ( is_paged() ) {
		$breadcrumb_output_link .= '<a href="' . $home_link . '">' . $home_text . '</a>';
		$breadcrumb_output_link .= $page_addon;
	}
} else {
	$breadcrumb_output_link .= '<li><a href="' . $home_link . '" >' . $home_text . '</a></li>';
	$breadcrumb_output_link .= $delimiter;
	$breadcrumb_output_link .= $breadcrumb_trail;
	$breadcrumb_output_link .= $page_addon;
}
$breadcrumb_output_link .= '</div><!-- .breadcrumbs -->';

return $breadcrumb_output_link;
}

//custom post for product type
function custom_post_product()
{
	$labels = array(
		'name' => __('Product', 'post type general name'),
		'singular_name' => __('product', 'post type singular name'),
		'add_new' => __('Add New Product', 'product'),
		'add_new_item' => __('Add New Product'),
		'edit_item' => __('Edit Product'),
		'new_item' => __('Add New Product'),
		'all_items' => __('All Product Members'),
		'view_item' => __('View Product'),
		'search_items' => __('Search Product'),
		'not_found' => __('No Product found'),
		'not_found_in_trash' => __('No Product found in the Trash'),
		'parent_item_colon' => '',
		'menu_name' => __( 'Product And Sales' ),

		);

	$args = array(
		'hierarchical' => true,
		'labels' => $labels,
		'description' => 'Holds our product specific data',
		'public' => true,
		'menu_position' => 5,
		'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
		'has_archive' => true,
		'publicly_queryable' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'product'),

		);

	register_post_type('product', $args);

}
add_action('init', 'custom_post_product');

//require custom widget for news page
require_once(get_template_directory().'/template-parts/custom-widget-for-archive.php');

//code to enable svg upload in wp
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


//exclude post with category 5 in main wp loop
function my_custom_get_posts( $query ) {
	if ( is_admin() || ! $query->is_main_query() )
		return;

	if ( $query->is_archive() ) {
		$query->set( 'cat', array( -5 ) );
	}
}
add_action( 'pre_get_posts', 'my_custom_get_posts', 1 );


//custom post for team management type
function custom_post_team()
{
	$labels = array(
		'name' => __('Team Management', 'post type general name'),
		'singular_name' => __('Team', 'post type singular name'),
		'add_new' => __('Add New member', 'team'),
		'add_new_item' => __('Add New member'),
		'edit_item' => __('Edit member'),
		'new_item' => __('Add New team'),
		'all_items' => __('All Team Members'),
		'view_item' => __('View team'),
		'search_items' => __('Search member'),
		'not_found' => __('No member found'),
		'not_found_in_trash' => __('No member found in the Trash'),
		'parent_item_colon' => '',
		'menu_name' => __( 'Team Management' ),

		);

	$args = array(
		'hierarchical' => true,
		'labels' => $labels,
		'description' => 'Holds our members and team specific data',
		'public' => true,
		'menu_position' => 5,
		'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
		'has_archive' => true,
		'publicly_queryable' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'team'),

		);

//for taxonomy label
	$label = array(
		'name' => __('Team-Category', 'post type general name'),
		'singular_name' => __('Team', 'post type singular name'),
		'add_new' => __('Add New member', 'team'),
		'add_new_item' => __('Add New member'),
		'edit_item' => __('Edit member'),
		'new_item' => __('Add New team'),
		'all_items' => __('All Team Members'),
		'view_item' => __('View team'),
		'search_items' => __('Search member'),
		'not_found' => __('No member found'),
		'not_found_in_trash' => __('No member found in the Trash'),
		'parent_item_colon' => '',
		'menu_name' => __( 'Team-Category' ),

		);
	$arg = array(
		'hierarchical' => true,
		'labels' => $label,
		'description' => 'Holds our works and work specific data',
		'public' => true,
		'menu_position' => 7,
		'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
		'has_archive' => true,
		'publicly_queryable' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'category'),

		);

	register_taxonomy('team-category', 'team', $arg);
	register_post_type('team', $args);

}
add_action('init', 'custom_post_team');

//DEfault Excerpt words to length 25 words
function bs_custom_excerpt_length( $length ) {
	return 25;
}
add_filter( 'excerpt_length', 'bs_custom_excerpt_length', 999 );

//What to show when doing excerpt
function new_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

//trim words using parameter
function custom_cut($text, $length) {
	$length = abs((int)$length);
	if(strlen($text) > $length) {
		$text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
	}
	return($text);
}


//dynamic select filter
function dynamic_select($choices, $args=array()) {
    // this function returns adynamic_selectn array of 
    // label => value pairs to be used in
    // a the select field
	$vacancy_title = array(" --- Enter Your Vacancy Post --- " => '');
	$arg = new WP_Query( array(
		'post_type' => 'post',
		'cat' => 5
		)                     
	);

	while ( $arg -> have_posts() ) :
		$arg -> the_post();
	$key["title"] = get_the_title();
	$vacancy_title[$key["title"]] = get_the_title();

	endwhile;
	

	return $vacancy_title;
} // end function cf7_dynamic_select_do_example1
add_filter('my-vacancy', 
	'dynamic_select', 10, 2);

//dynamic select filter for sales enquiry
function dynamic_select_product($choices, $args=array()) {
    // this function returns adynamic_selectn array of 
    // label => value pairs to be used in
    // a the select field
	$product_title = array(" --- Enter Your Product --- " => '');
	$arg = new WP_Query( array(
		'post_type' => 'product',
		)                     
	);

	while ( $arg -> have_posts() ) :
		$arg -> the_post();
	$key["title"] = get_the_title();
	$product_title[$key["title"]] = get_the_title();

	endwhile;
	

	return $product_title;
} // end function cf7_dynamic_select_do_example1
add_filter('my-product', 
	'dynamic_select_product', 10, 2);

//admin css hook
// add_action( 'admin_enqueue_scripts', 'function_name' );
// function load_custom_wp_admin_style() {
// 	wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
// 	wp_enqueue_style( 'custom_wp_admin_css' );
// }
// add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


