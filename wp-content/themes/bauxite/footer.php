<footer id="colophon" class="site-footer">
	<div class="site-info" style="background: url(<?php echo get_stylesheet_directory_uri(); ?>/images/img/footer-bg.jpg) no-repeat; background-size: cover;">
		<div class="container">
			<div class="row">
				<?php

				$rws_footer_address_enable = cs_get_option( 'rws_footer_address_enable' );
				$site_footer_address1 = cs_get_option( 'site_footer_address1' );
				$site_footer_address2 = cs_get_option( 'site_footer_address2' );
				$site_footer_address_image = cs_get_option( 'site_footer_address_image' );


				$rws_footer_phone_enable = cs_get_option( 'rws_footer_phone_enable' );
				$site_footer_phone1 = cs_get_option( 'site_footer_phone1' );
				$site_footer_phone2 = cs_get_option( 'site_footer_phone2' );
				$site_footer_phone_image = cs_get_option( 'site_footer_phone_image' );

				$rws_footer_email_enable = cs_get_option( 'rws_footer_email_enable' );
				$site_footer_email = cs_get_option( 'site_footer_email' );
				$site_footer_email_image = cs_get_option( 'site_footer_email_image' );

				$footer_map = cs_get_option( 'footer_map' );
				$footer_map = wp_get_attachment_image_src( $footer_map , 'full' );
				$footer_message = cs_get_option( 'footer_message' );

				$rws_quick_link_enable = cs_get_option( 'rws_quick_link_enable' );
				$footer_quick_link = cs_get_option( 'footer_quick_link' );				


				if ($rws_footer_address_enable or $rws_footer_email_enable or $rws_footer_phone_enable == 1) {
					?>
					<div class="col-4">
						<aside class="widget site-address">
							<header class="entry-header">
								<h4 class="entry-title">Bauxite Sales Contact</h4>
							</header>

							<ul>
								<?php if( 1 == $rws_footer_address_enable ): ?>
									<li>
										<span class="footer-icon">
											<i class="<?php echo $site_footer_address_image; ?>" aria-hidden="true"></i></span>
											<?php echo $site_footer_address1 ?>
											<?php 
											if (!empty($site_footer_address2)) {
												echo   ", "  . "<br>" .wpautop( $site_footer_address2 )   ;
											}?>
										</li>
									<?php endif; ?>
									<?php if( 1 == $rws_footer_email_enable ): ?>

										<li><a href="mailto:#"><span class="footer-icon">
											<i class="<?php echo $site_footer_email_image; ?>" aria-hidden="true"></i></span>
											<?php echo $site_footer_email ?>
										</a></li>
									<?php endif; ?>
									<?php if( 1 == $rws_footer_phone_enable ): 

									?>

									<li><a href="tel:#"><span class="footer-icon">
										<i class="<?php echo $site_footer_phone_image ?>" aria-hidden="true"></i></span>
										<?php echo $site_footer_phone1;?>
									</a></li>
								<?php endif; ?>

							</ul>

						</aside>
						<?php }?>
						<?php 
						if ($rws_quick_link_enable == 1) {?>
						<aside class="widget widget_nav_menu">
							<header class="entry-header">
								<h4 class="entry-title">Quick Links</h4>
							</header>
							<ul>
								<?php if( !empty( $footer_quick_link )):
								foreach ($footer_quick_link as $value):
									$quick_link_title = $value['quick_link_title' ];
								$quick_link_url = $value['quick_link_url' ];

								?>

								<li><a href="<?php echo $quick_link_url; ?>"><?php echo $quick_link_title; ?></a></li>

							<?php endforeach;
							endif; ?> 


						</ul>
					</aside>
					<?php }
					?>
				</div>
				<?php if (!empty($footer_map)) {?>
				<div class="col-8">
					<img src="<?php echo $footer_map[0];?>" alt="map-img"> 
				</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<!-- .site-info -->
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-6">
					<div class="legal">
						<?php
						if (has_nav_menu('footer-links')) {

							wp_nav_menu(array(
								'theme_location' => 'footer-links',
								));
						}
						?>
					</div>
				</div>
				<div class="col-6">
					<p><?php 
						//echo $footer_message . " - "; the_date('Y'); echo ".";
						echo $footer_message . " - ". date('Y'); ?></p>
					</div>
				</div>
			</div>
		</div>

	</footer>
	<!-- #colophon -->
</div>
<!-- .site-banner -->
<?php wp_footer(); ?>
</body>
</html>
