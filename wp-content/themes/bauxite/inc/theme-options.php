<?php
if ( ! defined('ABSPATH')) {die;}
// Cannot access pages directly.

/**
 * initializing the theme options array
 * @return array
 */
function rws_framework_options()
{
// ==================================================================================
// FRAMEWORK SETTINGS
// ==================================================================================

  $settings               = array(
    'menu_title'          => 'Theme Options',
        //'menu_type'       => 'add_theme_page', old version of codestar
        'menu_type'       => 'theme', // menu, submenu, options, theme, etc.
        'menu_slug'       => 'rws-framework',
        'ajax_save'       => true,
        'show_reset_all'  => false,
        'framework_title' => 'PC Framework <small>by Priemer Consulting</small>',
        );
// ===================================================================================
// FRAMEWORK OPTIONS
// ===================================================================================
  $options = array();

    // ------------------------------
    // a option section with tabs   -
    // ------------------------------


  $options[]            = array(
    'name'              => 'site_header_contact_info',
    'title'             => 'Header Settings',
    'icon'              => 'fa fa-cog',
          // begin: fields
    'fields'            => array(
      array(
        'type'          => 'notice',
        'class'         => 'info',
        'content'       => 'You can add header details here',
        ),
      array(
        'id'            => 'rws_header_address_enable',
        'type'          => 'switcher',
        'title'         => 'Show Header Address',
        ),
      array(
        'id'            => 'site_header_address1',
        'type'          => 'text',
        'title'         => 'Site Header Primary Address',
        'dependency'    => array('rws_header_address_enable', '==', 'true'),
        ),
      array(
        'id'            => 'site_header_address2',
        'type'          => 'text',
        'title'         => 'Site Header Secondary Address',
        'dependency'    => array('rws_header_address_enable', '==', 'true'),

        ),
      array(
        'id'            => 'site_header_address_image',
        'type'          => 'image',
        'title'         => 'Header Address Icon',
        'dependency'    => array('rws_header_address_enable', '==', 'true'),
        ),
      array(
        'id'            => 'rws_header_phone_enable',
        'type'          => 'switcher',
        'title'         => 'Show Header Phone Number',
        ),

      array(
        'id'            => 'site_header_phone1',
        'type'          => 'text',
        'title'         => 'Header PrimaryPhone Number',
        'dependency'    => array('rws_header_phone_enable', '==', 'true'),
        ),
      array(
        'id'            => 'site_header_phone2',
        'type'          => 'text',
        'title'         => 'Header Secondary Phone Number',
        'dependency'    => array('rws_header_phone_enable', '==', 'true'),
        ),
      array(
        'id'            => 'site_header_phone_image',
        'type'          => 'image',
        'title'         => 'Header Phone Icon',
        'dependency'    => array('rws_header_phone_enable', '==', 'true'),
        ),
      array(
        'id'            => 'rws_header_email_enable',
        'type'          => 'switcher',
        'title'         => 'Show Header Email Address',
        ),

      array(
        'id'            => 'site_header_email',
        'type'          => 'text',
        'title'         => 'Header Email Address',
        'dependency'    => array('rws_header_email_enable', '==', 'true'),
        ),
      array(
        'id'            => 'site_header_email_image',
        'type'          => 'image',
        'title'         => 'Header Email Icon',
        'dependency'    => array('rws_header_email_enable', '==', 'true'),
        ),


        ), // end: fields
      ); // end: text options

  $options[]            = array(
    'name'              => 'news_letter_section',
    'title'             => 'News Letter',
    'icon'              => 'fa fa-newspaper-o',

      // begin: fields
    'fields'            => array(
      array(
        'id'            => 'notice',
        'type'          => 'subheading',
        'content'       => __( 'Put News Letter Code Here'),
        ),
      array(
        'id'            => 'enable_news_letter',
        'type'          => 'switcher',
        'title'         => __('Enable News Letter'),
        ),

      array(
        'id'            => 'news_letter_form',
        'type'          => 'textarea',
        'title'         => 'Add your shortcode here',
        'dependency'    => array('enable_news_letter', '==', 'true'),
        'sanitize'      => false
        ),

      array(
        'id'            => 'news_letter_form_image',
        'type'          => 'image',
        'title'         => 'Background color for News letter',
        'dependency'    => array('enable_news_letter', '==', 'true'),
        ),

      ), // end: fields
    );


  $options[]            = array(
    'name'              => 'site_footer_contact_info',
    'title'             => 'Footer Settings',
    'icon'              => 'fa fa-cog',
          // begin: fields
    'fields'            => array(
      array(
        'type'          => 'notice',
        'class'         => 'info',
        'content'       => 'You can add footer details here',
        ),
      array(
        'id'            => 'rws_footer_address_enable',
        'type'          => 'switcher',
        'title'         => 'Show Footer Address',
        ),
      array(
        'id'            => 'site_footer_address1',
        'type'          => 'text',
        'title'         => 'Footer Primary Address',
        'dependency'    => array('rws_footer_address_enable', '==', 'true'),
        ),
      array(
        'id'            => 'site_footer_address2',
        'type'          => 'wysiwyg',
        'title'         => 'Footer Secondary Address',
        'dependency'    => array('rws_footer_address_enable', '==', 'true'),

        ),
      array(
        'id'            => 'site_footer_address_image',
        'type'          => 'icon',
        'title'         => 'Footer Address Icon',
        'dependency'    => array('rws_footer_address_enable', '==', 'true'),
        ),
      array(
        'id'            => 'rws_footer_phone_enable',
        'type'          => 'switcher',
        'title'         => 'Show Footer Phone Number',
        ),

      array(
        'id'            => 'site_footer_phone1',
        'type'          => 'text',
        'title'         => 'Footer Primary Phone Number',
        'dependency'    => array('rws_footer_phone_enable', '==', 'true'),
        ),
      // array(
      //   'id'      => 'site_footer_phone2',
      //   'type'     => 'text',
      //   'title'   => 'Footer Secondary Phone Number',
      //   'dependency'    => array('rws_footer_phone_enable', '==', 'true'),
      //   ),
      array(
        'id'            => 'site_footer_phone_image',
        'type'          => 'icon',
        'title'         => 'Footer Phone Icon',
        'dependency'    => array('rws_footer_phone_enable', '==', 'true'),
        ),
      array(
        'id'            => 'rws_footer_email_enable',
        'type'          => 'switcher',
        'title'         => 'Show Footer Email Address',
        ),

      array(
        'id'            => 'site_footer_email',
        'type'          => 'text',
        'title'         => 'Footer Email Address',
        'dependency'    => array('rws_footer_email_enable', '==', 'true'),
        ),
      array(
        'id'            => 'site_footer_email_image',
        'type'          => 'icon',
        'title'         => 'Footer Email Icon',
        'dependency'    => array('rws_footer_email_enable', '==', 'true'),
        ),
      array(
        'id'            => 'footer_map',
        'type'          => 'image',
        'title'         => 'Footer Map',
        ),
      array(
        'id'            => 'footer_message',
        'type'          => 'text',
        'title'         => 'Footer Message',
        ),
      array(
        'id'            => 'rws_quick_link_enable',
        'type'          => 'switcher',
        'title'         => 'Show Quick link',
        ),
      array(
        'id'             => 'footer_quick_link',
        'type'           => 'group',
        'title'          => __('Add Quick Links'),
        'button_title'   => __('Add New Link'),
        'accordion_title'=> __('Add New Quick Link'),
        'fields'         => array(

         array(
          'id'            => 'quick_link_title',
          'type'          => 'text',
          'title'         => 'Quick Link Title',
          ),
         array(
          'id'            => 'quick_link_url',
          'type'          => 'text',
          'title'         => 'Url to Quick link',
          ),
         
         ),
        'dependency'      => array('rws_quick_link_enable', '==', 'true'),
                    ), // end of slider repeater groups


        ), // end: fields
      ); // end: text options


// ------------------------------

return $options;
}

add_action('cs_framework_options', 'rws_framework_options');