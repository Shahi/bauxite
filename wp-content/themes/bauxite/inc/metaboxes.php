<?php
if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
function rws_common_metabox_section( $post_type )
{
	$common = array(
        'name'   => 'page_banner_section',
        'title'  => ucwords( $post_type ).' Slider',
        'fields' => array(
            array(
                'type'          => 'notice',
                'class'         => 'info',
                'content'       => __( 'Need to enable the option to make slider visible on this page.' ),
                ),

            array(
                'id'            => 'banner_slider_enable',
                'type'          => 'switcher',
                'title'         => __('Slider Enable'),
                ),
                    // slider repeater groups
            array(
                'id'                => 'slider_images',
                'type'              => 'group',
                'title'             => __('Add Slider Images'),
                'button_title'      => __('Add Slider Images'),
                'accordion_title'   => __('Add Slider Details with Image'),
                'fields'            => array(

                    array(
                        'id'        => 'slider_img',
                        'type'      => 'image',
                        'title'     => __('Slider Image'),
                        ),
                    array(
                        'id'         => 'slider_desc',
                        'type'       => 'textarea',
                        'title'      => __('Slider Description'),
                        'attributes' => array(
                            'placeholder' => 'Slider Description',
                            ),
                        ),
                    array(
                        'id'        => 'common_slider_button_enable',
                        'type'      => 'switcher',
                        'title'     => __('Slider Button Enable'),
                        'default'   => false
                        ),
                    array(
                        'id'        => 'slider_button_label',
                        'type'      => 'text',
                        'title'     => __('Slider Button Label'),
                        'dependency' => array('common_slider_button_enable', '==', 'true'),
                        ),
                    array(
                        'id'        => 'slider_button_url',
                        'type'      => 'text',
                        'title'     => __('Slider Button URL'),
                        'dependency' => array('common_slider_button_enable', '==', 'true'),
                        ),
                    ),
                'dependency'      => array('banner_slider_enable', '==', 'true'),
                    ), // end of slider repeater groups

            ),

        

        );

    return $common;
}

function rws_common_meta( $post_type )
{
    if($post_type == 'page' or $post_type == 'post' or $post_type == 'product' or $post_type == 'team') {
        $options = array(
        	'id'        => 'common_heading',
        	'title'     => 'Common '.ucwords( $post_type ).' Options',
        	'post_type' => $post_type,
        	'context'   => 'normal',
        	'priority'  => 'high',
        	'sections'  => array(
              rws_common_metabox_section( $post_type ),              
              ),
           );

//==============  metabox for contact breadcrumb start =====================/
        $options['sections'][] = array(
            'name'      => 'rws_enable_contact_breadcrumb_section',
            'title'     => __('Enable Breadcrumb'),
            'fields'    => array(

              array(
                'id'        => 'common_breadcrumb_enable',
                'type'      => 'switcher',
                'title'     => __('Enable Breadcrumb'),
                ),
              array(
                'id'        => 'common_breadcrumb_background',
                'type'      => 'image',
                'title'     => __('Breadcrumb Background Image'),
                'dependency'=> array('common_breadcrumb_enable', '==', 'true'),
                ),
              ),
            );
    //============== metabox for contact breadcrumb ends =============================/


    }
    return $options;
}

/**
 *
 * @return array
 */
function rws_metaboxes()
{

    $options[] = array();

    $options[]      = rws_common_meta ( 'page' );
    $options[]      = rws_common_meta ( 'post' );
    $options[]      = rws_common_meta ( 'product' );
    $options[]      = rws_common_meta ( 'team' );


    // $options[]      = rws_common_meta ( 'post' );

    //==============  for homepage starts ========================//
    $options[]   = array(
        'id'            => 'rws_homepage_section',
        'title'         => 'Other Content Homepage',
        'post_type'     => 'page',
        'context'       => 'normal',
        'priority'      => 'high',
        'sections'      => array(
            /* homepage Our welcome start*/
            array(
                'name'  => 'homepage_welcome_section',
                'title' => 'Welcome Section',
                'fields'=> array(
                    array(
                        'id'            => 'rws_welcome_enable',
                        'type'          => 'switcher',
                        'title'         => 'welcome Section Enable',
                        ),

                    array(
                        'id'            => 'hp_welcome_title',
                        'type'          => 'Wysiwyg',
                        'title'         => 'Welcome Message Title',
                        
                        'dependency'    => array('rws_welcome_enable', '==', 'true'),
                        ),
                    array(
                        'id'            => 'hp_welcome_desc',
                        'type'          => 'Wysiwyg',
                        'title'         => 'Welcome Message Content',
                        'dependency'    => array('rws_welcome_enable', '==', 'true'),
                        ),
                    array(
                        'id'            => 'hp_welcome_image',
                        'type'          => 'image',
                        'title'         => 'Welcome Image',
                        'before'        => 'Try uploading 650 X 550 for best result',
                        'dependency'    => array('rws_welcome_enable', '==', 'true'),
                        ),
                    array(
                        'id'            => 'hp_welcome_btn_enable',
                        'type'          => 'switcher',
                        'title'         => 'Read More Button Enable',
                        'dependency'    => array('rws_welcome_enable', '==', 'true'),
                        ),
                    array(
                        'id'            => 'hp_welcome_btn_label',
                        'type'          => 'text',
                        'title'         => 'Read More Label',
                        'dependency'    => array('hp_welcome_btn_enable|rws_welcome_enable', '==|==', 'true|true'),
                        ),
                    array(
                        'id'            => 'hp_welcome_btn_url',
                        'type'          => 'text',
                        'title'         => 'Read More URL',
                        'dependency'    => array('hp_welcome_btn_enable|rws_welcome_enable', '==|==', 'true|true'),
                        ),
                    )
                ),
            /* homepage Our welcome ends*/

            /* homepage Our counter start*/
            array(
                'name'  => 'homepage_counter_section',
                'title' => 'Counter Section',
                'fields'=> array(
                    array(
                        'id'            => 'hp_counter_enable',
                        'type'          => 'switcher',
                        'title'         => __('Show Counter Enable'),
                        ),
                    array(
                        'id'            => 'hp_counter_background',
                        'type'          => 'image',
                        'title'         => __('Counter Background Image'),
                        'dependency'    => array('hp_counter_enable', '==', 'true'),

                        ),

                    /*site in brief repeater groups start */
                    array(
                        'id'                => 'hp_counter_group',
                        'type'              => 'group',
                        'title'             => __('Add Counter Information'),
                        'button_title'      => __('Add new counter'),
                        'accordion_title'   => __('Add Counter Detail'),
                        'fields'            => array(
                            array(
                                'id'        => 'hp_counter_title',
                                'type'      => 'text',
                                'title'     => __('Counter Title'),
                                ),
                            array(
                                'id'         => 'hp_counter_amount',
                                'type'       => 'text',
                                'title'      => __('Counter value'),
                                'attributes' => array(
                                    'placeholder' => '150',
                                    ),
                                ),
                            array(
                                'id'         => 'hp_counter_image',
                                'type'       => 'image',
                                'title'      => __('Counter Image'),
                                ),

                            ),
                        'dependency'      => array('hp_counter_enable', '==', 'true'),
                        ), 
                    )
                ),
            /* homepage counter end */

            /* homepage information section start */
            array(
                'name'  => 'homepage_information_section',
                'title' => 'Information Section',
                'fields'=> array(
                    array(
                        'id'            => 'hp_site_information_enable',
                        'type'          => 'switcher',
                        'title'         => __(' Site information Enable'),
                        ),
                    array(
                        'id'            => 'hp_site_information_side_image',
                        'type'          => 'Image',
                        'title'         => __('Site Information Side Image'),
                        'dependency'    => array('hp_site_information_enable', '==', 'true'),

                        ),
                    array(
                        'id'            => 'hp_site_information_bottom_image',
                        'type'          => 'Image',
                        'title'         => __('Site Information Bottom Image'),
                        'dependency'    => array('hp_site_information_enable', '==', 'true'),

                        ),
                    array(
                        'id'            => 'hp_site_mission_enable',
                        'type'          => 'switcher',
                        'title'         => __(' Site Mission Enable'),
                        'dependency'    => array('hp_site_information_enable|hp_site_information_enable', '==|==', 'true|true'),
                        ),
                    array(
                        'id'            => 'hp_site_mission_title',
                        'type'          => 'wysiwyg',
                        'title'         => __('Site Mission Title'),
                        'dependency'    => array('hp_site_mission_enable|hp_site_information_enable', '==|==', 'true|true'),

                        ),
                    array(
                        'id'            => 'hp_site_mission_content',
                        'type'          => 'wysiwyg',
                        'title'         => __('Site Mission content'),
                        'attributes'    => array(
                            'placeholder' => 'Site Description',
                            ),
                        'dependency'    => array('hp_site_mission_enable|hp_site_information_enable', '==|==', 'true|true'),

                        ),
                    array(
                        'id'            => 'hp_site_mission_button_enable',
                        'type'          => 'switcher',
                        'title'         => __('Site Mission Button Enable'),
                        'dependency'    => array('hp_site_mission_enable|hp_site_information_enable', '==|==', 'true|true'),

                        ),
                    array(
                        'id'            => 'hp_site_mission_button_label',
                        'type'          => 'text',
                        'title'         => __('Site Mission Button Label'),
                        'dependency'    => array('hp_site_mission_button_enable|hp_site_mission_enable', '==|==', 'true|true'),
                        ),
                    array(
                        'id'            => 'hp_site_mission_button_url',
                        'type'          => 'text',
                        'title'         => __('Site Mission Button URL'),
                        'dependency'    => array('hp_site_mission_button_enable|hp_site_mission_enable', '==|==', 'true|true'),                        
                        ),
                    array(
                        'id'            => 'hp_site_vision_enable',
                        'type'          => 'switcher',
                        'title'         => __(' Site Vision Enable'),
                        'dependency'    => array('hp_site_information_enable|hp_site_information_enable', '==|==', 'true|true'),

                        ),
                    array(
                        'id'            => 'hp_site_vision_title',
                        'type'          => 'wysiwyg',
                        'title'         => __('Site Vision Title'),
                        'dependency'    => array('hp_site_vision_enable|hp_site_information_enable', '==|==', 'true|true'),

                        ),
                    array(
                        'id'            => 'hp_site_vision_content',
                        'type'          => 'wysiwyg',
                        'title'         => __('Site Vision content'),
                        'attributes'    => array(
                            'placeholder' => 'Site Description',
                            ),
                        'dependency'    => array('hp_site_vision_enable|hp_site_information_enable', '==|==', 'true|true'),

                        ),
                    array(
                        'id'            => 'hp_site_vision_button_enable',
                        'type'          => 'switcher',
                        'title'         => __('Site Vision Button Enable'),
                        'dependency'    => array('hp_site_vision_enable|hp_site_information_enable', '==|==', 'true|true'),

                        ),
                    array(
                        'id'            => 'hp_site_vision_button_label',
                        'type'          => 'text',
                        'title'         => __('Site Vision Button Label'),
                        'dependency'    => array('hp_site_vision_button_enable|hp_site_vision_enable', '==|==', 'true|true'),
                        ),
                    array(
                        'id'            => 'hp_site_vision_button_url',
                        'type'          => 'text',
                        'title'         => __('Site Vision Button URL'),
                        'dependency'    => array('hp_site_vision_button_enable|hp_site_vision_enable', '==|==', 'true|true'),
                        ), 
                    array(
                        'id'            => 'hp_site_values_enable',
                        'type'          => 'switcher',
                        'title'         => __(' Site Values Enable'),
                        'dependency'    => array('hp_site_information_enable|hp_site_information_enable', '==|==', 'true|true'),

                        ),
                    array(
                        'id'            => 'hp_site_values_title',
                        'type'          => 'wysiwyg',
                        'title'         => __('Site Values Title'),
                        'dependency'    => array('hp_site_values_enable|hp_site_information_enable', '==|==', 'true|true'),

                        ),
                    array(
                        'id'            => 'hp_site_values_content',
                        'type'          => 'wysiwyg',
                        'title'         => __('Site Values content'),
                        'attributes'    => array(
                            'placeholder' => 'Site Description',
                            ),
                        'dependency'    => array('hp_site_values_enable|hp_site_information_enable', '==|==', 'true|true'),

                        ),
                    array(
                        'id'            => 'hp_site_values_button_enable',
                        'type'          => 'switcher',
                        'title'         => __('Site Values Button Enable'),
                        'dependency'    => array('hp_site_values_enable|hp_site_information_enable', '==|==', 'true|true'),

                        ),
                    array(
                        'id'            => 'hp_site_values_button_label',
                        'type'          => 'text',
                        'title'         => __('Site Values Button Label'),
                        'dependency'    => array('hp_site_values_button_enable|hp_site_values_enable', '==|==', 'true|true'),
                        ),
                    array(
                        'id'            => 'hp_site_values_button_url',
                        'type'          => 'text',
                        'title'         => __('Site Values URL'),
                        'dependency'    => array('hp_site_values_button_enable|hp_site_values_enable', '==|==', 'true|true'),
                        ),                     
                    ),                    
),

/* Homepage Latest News Section Start */

array(
    'name'  => 'homepage_latest_news_section',
    'title' => 'Latest News Section',
    'fields'=> array(
        array(
            'id'            => 'rws_latest_news_enable',
            'type'          => 'switcher',
            'title'         => 'Latest News Section Enable',
            ),

        array(
            'id'            => 'hp_latest_news_title',
            'type'          => 'wysiwyg',
            'title'         => 'Latest News Title',
            'dependency'    => array('rws_latest_news_enable', '==', 'true'),
            ),
        array(
            'id'            => 'hp_latest_news_desc',
            'type'          => 'wysiwyg',
            'title'         => 'Latest News Content',
            'dependency'    => array('rws_latest_news_enable', '==', 'true'),
            ),

        array(
            'id'            => 'hp_latest_news_button_enable',
            'type'          => 'switcher',
            'title'         => 'Latest News Button Enable',
            'dependency'    => array('rws_latest_news_enable', '==', 'true'),
            ),
        array(
            'id'            => 'hp_latest_news_button_label',
            'type'          => 'text',
            'title'         => __('Latest news Button Label'),
            'dependency'    => array('hp_latest_news_button_enable|rws_latest_news_enable', '==|==', 'true|true'),
            ),
        array(
            'id'            => 'hp_latest_news_button_url',
            'type'          => 'text',
            'title'         => __('Latest news Button URL'),
            'dependency'    => array('hp_latest_news_button_enable|rws_latest_news_enable', '==|==', 'true|true'),
            ),
        )
    ),

),
);
/* Homepage Latest News Section Ends */


//==============  metabox for Home page news letter start =====================/
$options[] = array(
    'id'            => 'rws_homepage_news_letter_section',
    'title'         => 'News Letter',
    'post_type'     => 'page',
    'context'       => 'side',
    'priority'      => 'low',
    'sections'      => array(
      array(
          'name'    => 'homepage_enable_news_letter_section',
          'title'   => __('Enable News Letter'),
          'fields'  => array(
              array(
                  'id'      => 'homepage_news_letter_enable',
                  'title'   => __('Enable News Letter'),
                  'type'    => 'switcher',
                  'default' => true
                  ),

              ),
          ),
      ),
    );
    //============== metabox for homepage news letter ends =============================/
//==============  metabox for Home page ends =====================/
/* Home Page ends*/

    //==============  metabox for contact page starts =====================/
$options[]   = array(
    'id'            => 'rws_contact_section',
    'title'         => 'Other Content Contact',
    'post_type'     => 'page',
    'context'       => 'normal',
    'priority'      => 'high',
    'sections'      => array(
        /* contactpage Contact Form start*/
        array(
            'name'  => 'contactpage_contact_form',
            'title' => 'Contact Form Section',
            'fields'=> array(
                array(
                    'id'            => 'rws_general_form_enable',
                    'type'          => 'switcher',
                    'title'         => 'General Contact Form Enable',
                    ),
                array(
                    'id'            => 'cp_general_form_title',
                    'type'          => 'text',
                    'title'         => 'Section Title',
                    'dependency'    => array('rws_general_form_enable', '==', 'true'),
                    ),

                array(
                    'id'            => 'cp_general_form_shortcode',
                    'type'          => 'textarea',
                    'title'         => 'General Form Shortcode',
                    'desc'          => 'Create a form in Contact Form 7 then use shortcode here.',
                    'dependency'    => array('rws_general_form_enable', '==', 'true'),
                    ),
                array(
                    'id'            => 'rws_enquiry_form_enable',
                    'type'          => 'switcher',
                    'title'         => 'Enquiry Contact Form Enable',
                    ),
                array(
                    'id'            => 'cp_enquiry_form_title',
                    'type'          => 'text',
                    'title'         => 'Section Title',
                    'dependency'    => array('rws_enquiry_form_enable', '==', 'true'),
                    ),
                array(
                    'id'            => 'cp_enquiry_form_shortcode',
                    'type'          => 'textarea',
                    'title'         => 'Enquiry Form Shortcode',
                    'desc'          => 'Create a form in Contact Form 7 then use shortcode here.',
                    'dependency'    => array('rws_enquiry_form_enable', '==', 'true'),
                    ),
                array(
                    'id'            => 'rws_contact_persons_enable',
                    'type'          => 'switcher',
                    'title'         => 'Display Sales Persons',
                    'dependency'    => array('rws_enquiry_form_enable', '==', 'true'),
                    ),
                array(
                    'id'            => 'cp_sales_person_title',
                    'type'          => 'text',
                    'title'         => 'Sales Person Section Title',
                    'dependency'    => array('rws_contact_persons_enable|rws_enquiry_form_enable', '==|==', 'true|true'),
                    ),
                array(
                    'id'            => 'cp_contact_person1_name',
                    'type'          => 'text',
                    'title'         => '1st Sales Person Name',
                    'dependency'    => array('rws_contact_persons_enable|rws_enquiry_form_enable', '==|==', 'true|true'),
                    ),
                array(
                    'id'            => 'cp_contact_person_phone1',
                    'type'          => 'text',
                    'title'         => 'Sales Person Phone',
                    'dependency'    => array('rws_contact_persons_enable|rws_enquiry_form_enable', '==|==', 'true|true'),
                    ),
                array(
                    'id'            => 'cp_contact_person2_name',
                    'type'          => 'text',
                    'title'         => '2nd Sales Person',
                    'dependency'    => array('rws_contact_persons_enable|rws_enquiry_form_enable', '==|==', 'true|true'),
                    ),
                array(
                    'id'            => 'cp_contact_person_phone2',
                    'type'          => 'text',
                    'title'         => 'Sales Person Phone',
                    'dependency'    => array('rws_contact_persons_enable|rws_enquiry_form_enable', '==|==', 'true|true'),
                    ),

                )
            ),
/* contactpage Contact Form ends*/

/* contactpage Quick Contact start*/
array(
    'name'  => 'contact_page_contact_details',
    'title' => 'Contact Details Section',
    'fields'=> array(
        array(
            'id'        => 'cp_contact_details',
            'type'      => 'switcher',
            'title'     => 'Display Address',
            ),
        array(
           'id'           => 'cp_contact_details_title',
           'type'         => 'text',
           'title'        => __('Title For Contact Details'),
           'dependency'   => array('cp_contact_details', '==', 'true'),
           ),
        array(
           'id'           => 'cp_contact_country_icon',
           'type'         => 'icon',
           'title'        => __('Choose icon for address'),
           'dependency'   => array('cp_contact_details', '==', 'true'),
           ),
        array(
           'id'           => 'cp_contact_country',
           'type'         => 'text',
           'title'        => __('Country'),
           'dependency'   => array('cp_contact_details', '==', 'true'),

           ),
        array(
           'id'           => 'cp_contact_address1',
           'type'         => 'text',
           'title'        => __('Address 1'),
           'dependency'   => array('cp_contact_details', '==', 'true'),

           ),
        array(
           'id'           => 'cp_contact_address2',
           'type'         => 'text',
           'title'        => __('Address 2'),
           'dependency'   => array('cp_contact_details', '==', 'true'),
           ),
        array(
            'id'        => 'cp_contact_po_box_enable',
            'type'      => 'switcher',
            'title'     => 'Display P.O. Box Number',
            ),
        array(
           'id'           => 'cp_contact_po_box_number',
           'type'         => 'text',
           'title'        => __('P.O. Box Number'),
           'dependency'   => array('cp_contact_po_box_enable', '==', 'true'),

           ),
        array(
            'id'        => 'cp_contact_website_enable',
            'type'      => 'switcher',
            'title'     => 'Display Website',
            ),
        array(
           'id'           => 'cp_contact_website_icon',
           'type'         => 'icon',
           'title'        => __('Icon For Website'),
           'dependency'   => array('cp_contact_website_enable', '==', 'true'),

           ),
        array(
           'id'           => 'cp_contact_website',
           'type'         => 'text',
           'title'        => __('Website'),
           'dependency'   => array('cp_contact_website_enable', '==', 'true'),

           ),
        array(
            'id'        => 'cp_contact_email_enable',
            'type'      => 'switcher',
            'title'     => 'Display Email',
            ),
        array(
           'id'           => 'cp_contact_email_icon',
           'type'         => 'icon',
           'title'        => __('Email Icon'),
           'dependency'   => array('cp_contact_email_enable', '==', 'true'),

           ),
        array(
           'id'           => 'cp_contact_email',
           'type'         => 'text',
           'title'        => __('Email'),
           'dependency'   => array('cp_contact_email_enable', '==', 'true'),

           ),
        array(
            'id'        => 'cp_contact_phone_enable',
            'type'      => 'switcher',
            'title'     => 'Display Phone Details',
            ),
        array(
           'id'           => 'cp_contact_phone_icon',
           'type'         => 'icon',
           'title'        => __('Phone 1'),
           'dependency'   => array('cp_contact_phone_enable', '==', 'true'),

           ),
        array(
           'id'           => 'cp_contact_phone1',
           'type'         => 'text',
           'title'        => __('Phone 1'),
           'dependency'   => array('cp_contact_phone_enable', '==', 'true'),

           ),

        array(
           'id'           => 'cp_contact_phone2',
           'type'         => 'text',
           'title'        => __('Phone 2'),
           'dependency'   => array('cp_contact_phone_enable', '==', 'true'),

           ),

        )
),

            ),//end section
);
    //============== metabox for contact page ends ==================//
/* Contact Page ends*/

    //============== metabox for operation page start ==================//
$options[]   = array(
  'id'                    => 'rws_operation_section',
  'title'                 => 'Other Content Operation',
  'post_type'             => 'page',
  'context'               => 'normal',
  'priority'              => 'high',
  'sections'              => array(
      array(
          'name'          => 'op_mining_lease_section',
          'title'         => 'Mining Lease Section',
          'fields'        => array(

            array(
              'id'        => 'op_mining_lease_title',
              'type'      => 'text',
              'title'     => __('Title'),
              ),
            array(
              'id'        => 'op_mining_lease_content',
              'type'      => 'wysiwyg',
              'title'     => __('Content'),
              ),
            array(
              'id'        => 'op_mining_lease_button_enable',
              'type'      => 'switcher',
              'title'     => __('Enable Link'),
              ),
            array(
              'id'        => 'op_mining_lease_label',
              'type'      => 'text',
              'title'     => __('Label'),
              'dependency'=> array('op_mining_lease_button_enable', '==', 'true'),
              ),
            array(
              'id'        => 'op_mining_lease_url',
              'type'      => 'text',
              'title'     => __('Label'),
              'dependency'=> array('op_mining_lease_button_enable', '==', 'true'),
              ),
            array(
              'id'        => 'op_mining_lease_icon',
              'type'      => 'icon',
              'title'     => __('Icon'),
              'dependency'=> array('op_mining_lease_button_enable', '==', 'true'),
              ),
            ),
          ), 
      array(
          'name'          => 'op_production_chain_section',
          'title'         => 'Production Chain Section',
          'fields'        => array(
             array(
              'id'        => 'op_production_chain_title',
              'type'      => 'text',
              'title'     => __('Title'),
              ),
             array(
                 'id'       => 'op_production_chain_content',
                 'type'     => 'wysiwyg',
                 'title'    => __('Content'),
                 ),
             ),
          ), 
      array(
          'name'          => 'op_exploration_section',
          'title'         => '1. Exploration Section',
          'fields'        => array(
             array(
              'id'        => 'op_exploration_title',
              'type'      => 'text',
              'title'     => __('Title'),
              ),
             array(
                 'id'       => 'op_exploration_content',
                 'type'     => 'wysiwyg',
                 'title'    => __('Content'),
                 ),
             array(
                 'id'       => 'op_exploration_image_enable',
                 'type'     => 'switcher',
                 'title'    => __('Display Image At Last?'),
                 ),
             array(
                 'id'        => 'op_exploration_image',
                 'type'      => 'gallery',
                 'title'     => __('Image'),
                 'dependency'=> array('op_exploration_image_enable', '==', 'true'),
                 ),
             array(
                 'id'        => 'op_exploration_button_enable',
                 'type'      => 'switcher',
                 'title'     => __('Enable Link'),
                 ),
             array(
                 'id'        => 'op_exploration_label',
                 'type'      => 'text',
                 'title'     => __('Label'),
                 'dependency'=> array('op_exploration_button_enable', '==', 'true'),

                 ),

             array(
                 'id'        => 'op_exploration_url',
                 'type'      => 'text',
                 'title'     => __('Url'),
                 'dependency'=> array('op_exploration_button_enable', '==', 'true'),

                 ),
             array(
                 'id'        => 'op_exploration_icon',
                 'type'      => 'icon',
                 'title'     => __('Icon'),
                 'dependency'=> array('op_exploration_button_enable', '==', 'true'),

                 ),
             ),
          ), 
      array(
          'name'  => 'op_open_pit_mining_section',
          'title' => '2. Open Pit Mining Section',
          'fields'=> array(
             array(
              'id'        => 'op_open_pit_mining_title',
              'type'      => 'text',
              'title'     => __('Title'),
              ),
             array(
                 'id'        => 'op_open_pit_mining_content',
                 'type'      => 'wysiwyg',
                 'title'     => __('Content'),
                 ),
             array(
                 'id'        => 'op_open_pit_mining_image_enable',
                 'type'      => 'switcher',
                 'title'     => __('Display Image At Last?'),
                 ),
             array(
                 'id'        => 'op_open_pit_mining_image',
                 'type'      => 'gallery',
                 'title'     => __('Image'),
                 'dependency'=> array('op_open_pit_mining_image_enable', '==', 'true'),
                 ),
             array(
                 'id'        => 'op_open_pit_mining_button_enable',
                 'type'      => 'switcher',
                 'title'     => __('Enable Link'),
                 ),
             array(
                 'id'        => 'op_open_pit_mining_label',
                 'type'      => 'text',
                 'title'     => __('Label'),
                 'dependency'=> array('op_open_pit_mining_button_enable', '==', 'true'),

                 ),

             array(
                 'id'        => 'op_open_pit_mining_url',
                 'type'      => 'text',
                 'title'     => __('Url'),
                 'dependency'=> array('op_open_pit_mining_button_enable', '==', 'true'),

                 ),
             array(
                 'id'        => 'op_open_pit_mining_icon',
                 'type'      => 'icon',
                 'title'     => __('Icon'),
                 'dependency'=> array('op_open_pit_mining_button_enable', '==', 'true'),

                 ),
             ),
          ), 
      array(
          'name'  => 'op_benefication_section',
          'title' => '3. Benefication Section',
          'fields'=> array(
           array(
              'id'        => 'op_benefication_title',
              'type'      => 'text',
              'title'     => __('Title'),
              ),
           array(
               'id'      => 'op_benefication_content',
               'type'    => 'wysiwyg',
               'title'   => __('Content'),
               ),
           array(
             'id'        => 'op_benefication_image_enable',
             'type'      => 'switcher',
             'title'     => __('Display Image At Last?'),
             ),
           array(
             'id'        => 'op_benefication_image',
             'type'      => 'gallery',
             'title'     => __('Image'),
             'dependency'=> array('op_benefication_image_enable', '==', 'true'),
             ),
           array(
               'id'        => 'op_benefication_button_enable',
               'type'      => 'switcher',
               'title'     => __('Enable Link'),
               ),
           array(
               'id'        => 'op_benefication_label',
               'type'      => 'text',
               'title'     => __('Label'),
               'dependency'=> array('op_benefication_button_enable', '==', 'true'),

               ),

           array(
               'id'        => 'op_benefication_url',
               'type'      => 'text',
               'title'     => __('Url'),
               'dependency'=> array('op_benefication_button_enable', '==', 'true'),

               ),
           array(
               'id'        => 'op_benefication_icon',
               'type'      => 'icon',
               'title'     => __('Icon'),
               'dependency'=> array('op_benefication_button_enable', '==', 'true'),

               ),
           ),
          ), 
      array(
          'name'  => 'op_haulage_section',
          'title' => '4. Haulage Section',
          'fields'=> array(
           array(
              'id'        => 'op_haulage_title',
              'type'      => 'text',
              'title'     => __('Title'),
              ),
           array(
               'id'      => 'op_haulage_content',
               'type'    => 'wysiwyg',
               'title'   => __('Content'),
               ),
           array(
             'id'        => 'op_haulage_image_enable',
             'type'      => 'switcher',
             'title'     => __('Display Image At Last?'),
             ),
           array(
             'id'        => 'op_haulage_image',
             'type'      => 'gallery',
             'title'     => __('Image'),
             'dependency'=> array('op_haulage_image_enable', '==', 'true'),
             ),
           array(
               'id'        => 'op_haulage_button_enable',
               'type'      => 'switcher',
               'title'     => __('Enable Link'),
               ),
           array(
               'id'        => 'op_haulage_label',
               'type'      => 'text',
               'title'     => __('Label'),
               'dependency'=> array('op_haulage_button_enable', '==', 'true'),

               ),

           array(
               'id'        => 'op_haulage_url',
               'type'      => 'text',
               'title'     => __('Url'),
               'dependency'=> array('op_haulage_button_enable', '==', 'true'),

               ), 
           array(
               'id'        => 'op_haulage_icon',
               'type'      => 'icon',
               'title'     => __('Icon'),
               'dependency'=> array('op_haulage_button_enable', '==', 'true'),

               ),
           ),
          ), 
      array(
          'name'  => 'op_stock_piling_section',
          'title' => '5. Stock Piling Section',
          'fields'=> array(
           array(
              'id'        => 'op_stock_piling_title',
              'type'      => 'text',
              'title'     => __('Title'),
              ),
           array(
               'id'      => 'op_stock_piling_content',
               'type'    => 'wysiwyg',
               'title'   => __('Content'),
               ),
           array(
             'id'        => 'op_stock_piling_image_enable',
             'type'      => 'switcher',
             'title'     => __('Display Image At Last?'),
             ),
           array(
             'id'        => 'op_stock_piling_image',
             'type'      => 'gallery',
             'title'     => __('Image'),
             'dependency'=> array('op_stock_piling_image_enable', '==', 'true'),
             ),
           array(
               'id'        => 'op_stock_piling_button_enable',
               'type'      => 'switcher',
               'title'     => __('Enable Link'),
               ),
           array(
               'id'        => 'op_stock_piling_label',
               'type'      => 'text',
               'title'     => __('Label'),
               'dependency'=> array('op_stock_piling_button_enable', '==', 'true'),

               ),

           array(
               'id'        => 'op_stock_piling_url',
               'type'      => 'text',
               'title'     => __('Url'),
               'dependency'=> array('op_stock_piling_button_enable', '==', 'true'),

               ), 
           array(
               'id'        => 'op_stock_piling_icon',
               'type'      => 'icon',
               'title'     => __('Icon'),
               'dependency'=> array('op_stock_piling_button_enable', '==', 'true'),

               ),
           ),
          ), 
      array(
          'name'  => 'op_drying_section',
          'title' => '6. Drying Section',
          'fields'=> array(
           array(
              'id'        => 'op_drying_title',
              'type'      => 'text',
              'title'     => __('Title'),
              ),
           array(
             'id'      => 'op_drying_content',
             'type'    => 'wysiwyg',
             'title'   => __('Content'),
             ),
           array(
             'id'        => 'op_drying_image_enable',
             'type'      => 'switcher',
             'title'     => __('Display Image At Last?'),
             ),
           array(
             'id'        => 'op_drying_image',
             'type'      => 'gallery',
             'title'     => __('Image'),
             'dependency'=> array('op_drying_image_enable', '==', 'true'),
             ),
           array(
               'id'        => 'op_drying_button_enable',
               'type'      => 'switcher',
               'title'     => __('Enable Link'),
               ),
           array(
               'id'        => 'op_drying_label',
               'type'      => 'text',
               'title'     => __('Label'),
               'dependency'=> array('op_drying_button_enable', '==', 'true'),

               ),

           array(
               'id'        => 'op_drying_url',
               'type'      => 'text',
               'title'     => __('Url'),
               'dependency'=> array('op_drying_button_enable', '==', 'true'),

               ),
           array(
               'id'        => 'op_drying_icon',
               'type'      => 'icon',
               'title'     => __('Icon'),
               'dependency'=> array('op_drying_button_enable', '==', 'true'),

               ),
           ),
          ), 
      array(
          'name'  => 'op_transhiping_section',
          'title' => '7. Transhipping Section',
          'fields'=> array(
           array(
              'id'        => 'op_transhiping_title',
              'type'      => 'text',
              'title'     => __('Title'),
              ),
           array(
               'id'        => 'op_transhiping_content',
               'type'      => 'wysiwyg',
               'title'     => __('Content'),
               ),
           array(
             'id'        => 'op_transhiping_image_enable',
             'type'      => 'switcher',
             'title'     => __('Display Image At Last?'),
             ),
           array(
             'id'        => 'op_transhiping_image',
             'type'      => 'gallery',
             'title'     => __('Image'),
             'dependency'=> array('op_transhiping_image_enable', '==', 'true'),
             ),
           array(
               'id'        => 'op_transhiping_button_enable',
               'type'      => 'switcher',
               'title'     => __('Enable Link'),
               ),
           array(
               'id'        => 'op_transhiping_label',
               'type'      => 'text',
               'title'     => __('Label'),
               'dependency'=> array('op_transhiping_button_enable', '==', 'true'),

               ),

           array(
               'id'        => 'op_transhiping_url',
               'type'      => 'text',
               'title'     => __('Url'),
               'dependency'=> array('op_transhiping_button_enable', '==', 'true'),

               ),
           array(
               'id'        => 'op_transhiping_icon',
               'type'      => 'icon',
               'title'     => __('Icon'),
               'dependency'=> array('op_transhiping_button_enable', '==', 'true'),

               ),
           ),
          ), 
      array(
          'name'  => 'op_production_section',
          'title' => 'Production Section',
          'fields'=> array(
             array(
              'id'        => 'op_production_title',
              'type'      => 'text',
              'title'     => __('Title'),
              ),
             array(
                'id'        => 'op_production_content',
                'type'      => 'wysiwyg',
                'title'     => __('Content'),
                ),
             array(
               'id'        => 'op_production_button_enable',
               'type'      => 'switcher',
               'title'     => __('Enable Link'),
               ),
             array(
                'id'        => 'op_production_label',
                'type'      => 'text',
                'title'     => __('Label'),
                'dependency'=> array('op_production_button_enable', '==', 'true'),

                ),
             array(
                'id'        => 'op_production_url',
                'type'      => 'text',
                'title'     => __('URL'),
                'dependency'=> array('op_production_button_enable', '==', 'true'),

                ),  
             array(
                'id'        => 'op_production_icon',
                'type'      => 'icon',
                'title'     => __('Icon'),
                'dependency'=> array('op_production_button_enable', '==', 'true'),

                ),

             ),
          ), 
      array(
          'name'  => 'op_resource_and_reserve_section',
          'title' => 'Resource and Reserve Section',
          'fields'=> array(
            array(
              'id'        => 'op_resource_and_reserve_title',
              'type'      => 'text',
              'title'     => __('Title'),
              ),
            array(
                'id'        => 'op_resource_and_reserve_content',
                'type'      => 'wysiwyg',
                'title'     => __('Content'),
                'before'    => '<div class="custom-text">',
                'after'    =>'</div>'
                ),
            array(
                'id'        => 'op_resource_and_reserve_button_enable',
                'type'      => 'switcher',
                'title'     => __(''),
                ),
            array(
                'id'        => 'op_resource_and_reserve_label',
                'type'      => 'text',
                'title'     => __('Label'),
                'dependency'=> array('op_resource_and_reserve_button_enable', '==', 'true'),

                ),
            array(
                'id'        => 'op_resource_and_reserve_url',
                'type'      => 'text',
                'title'     => __('Url'),
                'dependency'=> array('op_resource_and_reserve_button_enable', '==', 'true'),

                ),
            array(
                'id'        => 'op_resource_and_reserve_icon',
                'type'      => 'icon',
                'title'     => __('Icon'),
                'dependency'=> array('op_resource_and_reserve_button_enable', '==', 'true'),

                ),

            ),
          ), 
      ),
);

$options[]   = array(
    'id'            => 'rws_operation_mining_news_section',
    'title'         => 'Enable Banner For this Page?',
    'post_type'     => 'page',
    'context'       => 'side',
    'priority'      => 'low',
    'sections'      => array(
        array(
            'name'  => 'op_mining_news_letter_section',
            'title' => 'News Letter Section',
            'fields'=> array(
                array(
                    'id'        => 'op_mining_news_letter_enable',
                    'type'      => 'switcher',
                    'title'     => __('Enable News Letter'),
                    'default'   => true
                    ),

                ),
            ), 

        ),
    );

$options[]   = array(
    'id'            => 'rws_operation_precious_section',
    'title'         => 'Other Content Operation',
    'post_type'     => 'page',
    'context'       => 'normal',
    'priority'      => 'high',
    'sections'      => array(
        array(
            'name'  => 'rws_operation_precious_exploration_section',
            'title' => 'Exploration Permits (EL01/2012 & EL46/2012) ',
            'fields'=> array(
                array(
                    'id'        => 'op_precious_exploration_permits_title',
                    'type'      => 'text',
                    'title'     => __('Title'),
                    ),
                array(
                    'id'        => 'op_precious_exploration_permits_content',
                    'type'      => 'wysiwyg',
                    'title'     => __('Content'),
                    ),
                array(
                    'id'        => 'op_precious_exploration_permits_image',
                    'type'      => 'gallery',
                    'title'     => __('Images'),
                    ),
                array(
                    'id'        => 'op_precious_exploration_permits_button_enable',
                    'type'      => 'switcher',
                    'title'     => __('Enable Button'),
                    ),
                array(
                    'id'        => 'op_precious_exploration_permits_button_label',
                    'type'      => 'text',
                    'title'     => __('Label'),
                    'dependency'=> array('op_precious_exploration_permits_button_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'op_precious_exploration_permits_button_icon',
                    'type'      => 'icon',
                    'title'     => __('Icon'),
                    'dependency'=> array('op_precious_exploration_permits_button_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'op_precious_exploration_permits_button_url',
                    'type'      => 'text',
                    'title'     => __('Link'),
                    'dependency'=> array('op_precious_exploration_permits_button_enable', '==', 'true'),
                    ),
                ),
            ), 
        array(
            'name'  => 'rws_operation_precious_section',
            'title' => 'Operations',
            'fields'=> array(
                array(
                    'id'        => 'op_precious_operations_title',
                    'type'      => 'text',
                    'title'     => __('Title'),
                    ),
                ),
            ),
        array(
            'name'  => 'rws_operation_precious_section1',
            'title' => 'Operations Point 1',
            'fields'=> array(
                array(
                    'id'        => 'op_precious_operations_tab1_title',
                    'type'      => 'text',
                    'title'     => __('Title'),
                    'desc'      => '(Optional Field)'

                    ),
                array(
                    'id'        => 'op_precious_operations_tab1_content',
                    'type'      => 'wysiwyg',
                    'title'     => __('Content'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab1_image',
                    'type'      => 'gallery',
                    'title'     => __('Images'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab1_button_enable',
                    'type'      => 'switcher',
                    'title'     => __('Enable Button'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab1_button_label',
                    'type'      => 'text',
                    'title'     => __('Label'),
                    'dependency'=> array('op_precious_operations_tab1_button_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab1_button_icon',
                    'type'      => 'icon',
                    'title'     => __('Icon'),
                    'dependency'=> array('op_precious_operations_tab1_button_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab1_button_url',
                    'type'      => 'text',
                    'title'     => __('Link'),
                    'dependency'=> array('op_precious_operations_tab1_button_enable', '==', 'true'),
                    ),
                ),
            ), 
        array(
            'name'  => 'rws_operation_precious_section2',
            'title' => 'Operations Point 2',
            'fields'=> array(
                array(
                    'id'        => 'op_precious_operations_tab2_title',
                    'type'      => 'text',
                    'title'     => __('Title'),
                    'desc'      => '(Optional Field)'
                    ),
                array(
                    'id'        => 'op_precious_operations_tab2_content',
                    'type'      => 'wysiwyg',
                    'title'     => __('Content'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab2_image',
                    'type'      => 'gallery',
                    'title'     => __('Images'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab2_button_enable',
                    'type'      => 'switcher',
                    'title'     => __('Enable Button'),
                    ),

                array(
                    'id'        => 'op_precious_operations_tab2_button_label',
                    'type'      => 'text',
                    'title'     => __('Label'),
                    'dependency'=> array('op_precious_tab2_button_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab2_button_icon',
                    'type'      => 'icon',
                    'title'     => __('Icon'),
                    'dependency'=> array('op_precious_tab2_button_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab2_button_url',
                    'type'      => 'text',
                    'title'     => __('Link'),
                    'dependency'=> array('op_precious_tab2_button_enable', '==', 'true'),
                    ),
                ),
            ), 
        array(
            'name'  => 'rws_operation_precious_section3',
            'title' => 'Operations  Point 3',
            'fields'=> array(
                array(
                    'id'        => 'op_precious_operations_tab3_title',
                    'type'      => 'text',
                    'title'     => __('Title'),
                    'desc'      => '(Optional Field)'

                    ),
                array(
                    'id'        => 'op_precious_operations_tab3_content',
                    'type'      => 'wysiwyg',
                    'title'     => __('Content'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab3_image',
                    'type'      => 'gallery',
                    'title'     => __('Images'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab3_button_enable',
                    'type'      => 'switcher',
                    'title'     => __('Enable Button'),
                    ),

                array(
                    'id'        => 'op_precious_operations_tab3_button_label',
                    'type'      => 'text',
                    'title'     => __('Label'),
                    'dependency'=> array('op_precious_operations_tab3_button_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab3_button_icon',
                    'type'      => 'icon',
                    'title'     => __('ICon'),
                    'dependency'=> array('op_precious_operations_tab3_button_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab3_button_url',
                    'type'      => 'text',
                    'title'     => __('Link'),
                    'dependency'=> array('op_precious_operations_tab3_button_enable', '==', 'true'),
                    ),
                ),
            ), 
        array(
            'name'  => 'rws_operation_precious_section4',
            'title' => 'Operations  Point 4',
            'fields'=> array(
                array(
                    'id'        => 'op_precious_operations_tab4_title',
                    'type'      => 'text',
                    'title'     => __('Title'),
                    'desc'      => '(Optional Field)'

                    ),
                array(
                    'id'        => 'op_precious_operations_tab4_content',
                    'type'      => 'wysiwyg',
                    'title'     => __('Content'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab4_image',
                    'type'      => 'gallery',
                    'title'     => __('Images'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab4_button_enable',
                    'type'      => 'switcher',
                    'title'     => __('Enable Button'),
                    ),

                array(
                    'id'        => 'op_precious_operations_tab4_button_label',
                    'type'      => 'text',
                    'title'     => __('Label'),
                    'dependency'=> array('op_precious_operations_tab4_button_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab4_button_icon',
                    'type'      => 'icon',
                    'title'     => __('ICon'),
                    'dependency'=> array('op_precious_operations_tab4_button_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'op_precious_operations_tab4_button_url',
                    'type'      => 'text',
                    'title'     => __('Link'),
                    'dependency'=> array('op_precious_operations_tab4_button_enable', '==', 'true'),
                    ),
                ),
            ), 
        ),
);

$options[]   = array(
    'id'            => 'rws_operation_precious_news_section',
    'title'         => 'Enable Banner For this Page?',
    'post_type'     => 'page',
    'context'       => 'side',
    'priority'      => 'low',
    'sections'      => array(
        array(
            'name'  => 'op_precious_news_letter_section',
            'title' => 'News Letter Section',
            'fields'=> array(
                array(
                    'id'        => 'op_precious_news_letter_enable',
                    'type'      => 'switcher',
                    'title'     => __('Enable News Letter'),
                    'default'   => true
                    ),
                
                ),
            ), 
        
        ),
    );


    //============== metabox for operation page Ends ==================//
/* Operation Page ends*/


//==============  metabox for About page starts =====================/
$options[]   = array(
    'id'            => 'rws_about_section',
    'title'         => 'Other Content About Us',
    'post_type'     => 'page',
    'context'       => 'normal',
    'priority'      => 'high',
    'sections'      => array(
        array(
            'name'  => 'about_history_section',
            'title' => 'Company History',
            'fields'=> array(
                array(
                    'id'        => 'about_history_enable',
                    'type'      => 'switcher',
                    'title'     => __('Enable History Information'),
                    ),
                array(
                    'id'        => 'about_history_title',
                    'type'      => 'text',
                    'title'     => __('History Information Title'),
                    'dependency'=> array('about_history_enable', '==', 'true'),

                    ),
                array(
                    'id'        => 'about_history_content',
                    'type'      => 'wysiwyg',
                    'title'     => __('History Information Content'),
                    'dependency'=> array('about_history_enable', '==', 'true'),

                    ),
                array(
                    'id'        => 'about_history_image',
                    'type'      => 'image',
                    'title'     => __('History Information image'),
                    'dependency'=> array('about_history_enable', '==', 'true'),

                    ),
                ),
            ), 
        array(
            'name'  => 'company_Structure_section',
            'title' => 'Company Structure Section',
            'fields'=> array(
                array(
                    'id'        => 'about_company_structure_enable',
                    'type'      => 'switcher',
                    'title'     => __('Company Structure Image'),
                    ),
                array(
                    'id'        => 'about_company_structure_image',
                    'type'      => 'upload',
                    'title'     => __('Company Structure Image'),
                    'dependency' => array('about_company_structure_enable', '==', 'true'),
                    ),
                ),
            ), 
        array(
            'name'  => 'about_location_section',
            'title' => 'Location and Contact Section',
            'fields'=> array(
                array(
                    'id'        => 'about_location_enable',
                    'type'      => 'switcher',
                    'title'     => __('Display Location Map Enable'),
                    ),
                array(
                    'id'        => 'about_company_location_image',
                    'type'      => 'image',
                    'title'     => __('Company Map Image'),
                    // 'desc' => 'iframe should have width="100%" height="610" frameborder="0" for better result',
                    'dependency' => array('about_location_enable', '==', 'true'),
                    // 'sanitize' => false,
                    ),                
                
                array(
                    'id'        => 'about_location_details_enable',
                    'type'      => 'switcher',
                    'title'     => __('Display Location Details Enable'),
                    ),
                array(
                    'id'        => 'about_location_title',
                    'type'      => 'text',
                    'title'     => __('Location Title'),
                    'dependency' => array('about_location_details_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'about_location_content',
                    'type'      => 'wysiwyg',
                    'title'     => __('Location Content'),
                    'dependency' => array('about_location_details_enable', '==', 'true'),
                    ),
                array(
                    'id'        => 'about_contact_title',
                    'type'      => 'text',
                    'title'     => __('Contact Title'),
                    'dependency'=> array('about_location_details_enable', '==', 'true'),
                    ),  
                array(
                    'id'        => 'about_contact_address1',
                    'type'      => 'text',
                    'title'     => __('Company Address 1'),
                    'dependency'=> array('about_location_details_enable', '==', 'true'),
                    ),   
                array(
                    'id'        => 'about_contact_address2',
                    'type'      => 'text',
                    'title'     => __('Company Address 2'),
                    'dependency'=> array('about_location_details_enable', '==', 'true'),
                    ),    
                array(
                    'id'        => 'about_contact_email',
                    'type'      => 'text',
                    'title'     => __('Company Email Address'),
                    'dependency'=> array('about_location_details_enable', '==', 'true'),
                    ),   
                array(
                    'id'        => 'about_contact_phone',
                    'type'      => 'text',
                    'title'     => __('Company Phone Number'),
                    'dependency'=> array('about_location_details_enable', '==', 'true'),
                    ),  

                ),
            ), 
        ),
);
//==============  metabox for About page news letter start =====================/
$options[] = array(
    'id'        => 'rws_about_news_letter_section',
    'title'     => 'Enable News Letter For This Page?',
    'post_type'     => 'page',
    'context'       => 'side',
    'priority'      => 'low',
    'sections'      => array(
      array(
          'name'  => 'about_enable_news_letter_section',
          'title'     => __('Enable News Letter'),
          'fields'=> array(
              array(
                  'id' => 'about_news_letter_enable',
                  'type'  => 'switcher',
                  'default' => true
                  ),
              ),
          ),
      ),
    );
//==============  metabox for About page news letter end =====================/

//==============  metabox for About page  start =====================/
$options[]   = array(
    'id'            => 'rws_cpt_team_section',
    'title'         => 'Social Connect',
    'post_type'     => 'team',
    'context'       => 'side',
    'priority'      => 'high',
    'sections'      => array(       
        array(
            'name'  => 'about_team_section',
            'title' => 'Social Connect',
            'fields'=> array(

                array(
                    'id'        => 'about_team_post',
                    'type'      => 'text',
                    'title'     => __('Member Post'),
                    ),
                array(
                    'id'        => 'about_team_fb_contact',
                    'type'      => 'text',
                    'title'     => __('Member Facebook Link'),
                    ),
                array(
                    'id'        => 'about_team_instagram_contact',
                    'type'      => 'text',
                    'title'     => __('Member Instagram Link'),
                    ),
                array(
                    'id'        => 'about_team_twitter_contact',
                    'type'      => 'text',
                    'title'     => __('Member Twitter Link'),
                    ),
                array(
                    'id'        => 'about_team_Linked_in_contact',
                    'type'      => 'text',
                    'title'     => __('Member Linked-in Link'),
                    ),
                ), 
            ),


        ),
    );
//==============  metabox for About page  end =====================/
/* About Page ends*/

/* Policy Page ends*/
//==============  metabox for Career page end =====================/

$options[]   = array(
    'id'            => 'rws_career_section',
    'title'         => 'Other Content Career',
    'post_type'     => 'page',
    'context'       => 'normal',
    'priority'      => 'high',
    'sections'      => array(
        array(
            'name'  => 'careerp_work_section',
            'title' => 'Working in SMHL',
            'fields'=> array(
                array(
                    'id'        => 'careerp_work_enable',
                    'type'      => 'switcher',
                    'title'     => __('Working In SMHL Section Enable'),
                    ),
                array(
                    'id'        => 'careerp_work_title',
                    'type'      => 'text',
                    'title'     => __('Working In SMHL Section Title'),
                    'dependency' => array('careerp_work_enable', '==', 'true'),
                    ),  
                array(
                    'id'        => 'careerp_work_content',
                    'type'      => 'wysiwyg',
                    'title'     => __('Working In SMHL Section Content'),
                    'dependency' => array('careerp_work_enable', '==', 'true'),

                    ),
                array(
                    'id'        => 'careerp_work_image',
                    'type'      => 'image',
                    'title'     => __('Working In SMHL Section Image'),
                    'dependency' => array('careerp_work_enable', '==', 'true'),
                    ),
                ),
            ), 
        array(
            'name'  => 'careerp_intership_section',
            'title' => 'Intership',
            'fields'=> array(
               array(
                'id'        => 'careerp_intership_enable',
                'type'      => 'switcher',
                'title'     => __('Intership Section Enable'),
                ),
               array(
                'id'        => 'careerp_intership_title',
                'type'      => 'text',
                'title'     => __('Intership Section Title'),
                'dependency'=> array('careerp_intership_enable', '==', 'true'),
                ),
               array(
                'id'        => 'careerp_intership_content',
                'type'      => 'wysiwyg',
                'title'     => __('Intership Section Content'),
                'dependency'=> array('careerp_intership_enable', '==', 'true'),
                ),
               array(
                'id'        => 'careerp_intership_image',
                'type'      => 'image',
                'title'     => __('Intership Section Image'),
                'dependency'=> array('careerp_intership_enable', '==', 'true'),
                ),
               ),
            ),
        array(
            'name'  => 'careerp_oppprtunity_section',
            'title' => 'Opportunity Section',
            'fields'=> array(
               array(
                'id'        => 'careerp_opportunity_section_enable',
                'type'      => 'switcher',
                'title'     => __('Opportunity Section Enable'),
                'default'   => true
                ),
               ),
            ), 
        array(
            'name'  => 'careerp_apply_form_section',
            'title' => 'Apply Form Section',
            'fields'=> array(
               array(
                'id'        => 'careerp_apply_form_enable',
                'type'      => 'switcher',
                'title'     => __('Apply Form Section Enable'),
                ),
               array(
                'id'        => 'careerp_apply_form',
                'type'      => 'textarea',
                'title'     => __('Apply Form Shortcode'),
                'sanitize' => false,
                'dependency' => array('careerp_apply_form_enable', '==', 'true'),
                ),

               ),
            ), 

        ),
    );
//==============  metabox for Career Page end =====================/
//==============  metabox for Career page news letter start =====================/
$options[] = array(
    'id'          => 'rws_cp_news_letter_section',
    'title'       => 'Enable News Letter For This Page?',
    'post_type'   => 'page',
    'context'     => 'side',
    'priority'    => 'low',
    'sections'    => array(
      array(
          'name'  => 'np_enable_news_letter_section',
          'title' => __('Enable News Letter'),
          'fields'=> array(
              array(
                  'id'      => 'cp_news_letter_enable',
                  'type'    => 'switcher',
                  'default' => true
                  ),
              ),
          ),
      ),
    );
//==============  metabox for Career page news letter end =====================/


//==============  metabox for Career page vacancy start =====================/
$options[] = array(
    'id'        => 'rws_cp_vacancy_detail_section',
    'title'     => 'Add Few Details',
    'post_type'     => 'post',
    'context'       => 'side',
    'priority'      => 'high',
    'sections'      => array(
      array(
          'name'    => 'rws_cp_vacancy_detail_section',
          'title'   => __('Vacancy Details'),
          'fields'  => array(
              array(
                  'id'      => 'rws_cp_vacancy_position_section',
                  'type'    => 'text',
                  'title'   => __('Position'),
                  ),
              array(
                  'id'      => 'rws_cp_vacancy_report_section',
                  'type'    => 'text',
                  'title'   => __('Report'),
                  ),
              array(
                  'id'      => 'rws_cp_vacancy_contract_section',
                  'type'    => 'text',
                  'title'   => __('Contract'),
                  ),
              array(
                  'id'      => 'rws_cp_vacancy_salary_section',
                  'type'    => 'text',
                  'title'   => __('Salary'),
                  ),

              ),
          ),
      ),
    );
//==============  metabox for Career page vacancy end =====================/

//==============  metabox for News page news letter start =====================/
$options[]      = array(
    'id'        => 'rws_np_news_letter_section',
    'title'     => 'Enable News Letter For This Page?',
    'post_type'     => 'page',
    'context'       => 'side',
    'priority'      => 'low',
    'sections'      => array(
      array(
          'name'    => 'np_enable_news_letter_section',
          'title'   => __('Enable News Letter'),
          'fields'  => array(
              array(
                  'id'      => 'np_news_letter_enable',
                  'type'    => 'switcher',
                  'default' => true
                  ),
              ),
          ),
      ),
    );
//==============  metabox for news page news letter end =====================/


//==============  metabox for Sustainable page  start =====================/
$options[]   = array(
    'id'            => 'rws_sustainable_section',
    'title'         => 'Other Content Sustainable',
    'post_type'     => 'page',
    'context'       => 'normal',
    'priority'      => 'high',
    'sections'      => array(
        array(
            'name'  => 'sustainable_list_section',
            'title' => 'Section',
            'fields'=> array(

                array(
                  'id'                => 'sp_listing_group',
                  'type'              => 'group',
                  'title'             => __('Add Sustainable List'),
                  'button_title'      => __('Add new List'),
                  'accordion_title'   => __('Add'),
                  'fields'            => array(
                      array(
                          'id'        => 'sp_listing_title',
                          'type'      => 'text',
                          'title'     => __('Sustainable List Title'),
                          ),
                      array(
                          'id'        => 'sp_listing_content',
                          'type'      => 'textarea',
                          'title'     => __('Sustainable List Content'),
                          ),
                      array(
                          'id'        => 'sp_listing_gallery',
                          'type'      => 'gallery',
                          'title'     => __('Images'),
                          ),
                      

                      array(
                        'id'        => 'sp_enable_pdf_button',
                        'type'      => 'switcher',
                        'title'     => __('Enable PDF link'),
                        ),
                      array(
                          'id'        => 'sp_pdf_link_label',
                          'type'      => 'text',
                          'title'     => __('Label For PDF Link'),
                          'dependency'      => array('sp_enable_pdf_button', '==', 'true'),
                          ),
                      array(
                          'id'        => 'sp_pdf_link_url',
                          'type'      => 'text',
                          'title'     => __('Url For PDF Link'),
                          'dependency'      => array('sp_enable_pdf_button', '==', 'true'),

                          ),
                      array(
                          'id'        => 'sp_pdf_link_icon',
                          'type'      => 'image',
                          'title'     => __('Icon For PDF Link'),
                          'dependency'      => array('sp_enable_pdf_button', '==', 'true'),
                          ),
                      ),
                  ),
                ),
            ),
        ),
    );
//==============  metabox for Sustainable page  end =====================/

//==============  metabox for  Sustainable News page news letter start =====================/
$options[]      = array(
    'id'        => 'rws_sp_news_letter_section',
    'title'     => 'Enable News Letter For This Page?',
    'post_type'     => 'page',
    'context'       => 'side',
    'priority'      => 'low',
    'sections'      => array(
      array(
          'name'    => 'sp_enable_news_letter_section',
          'title'   => __('Enable News Letter'),
          'fields'  => array(
              array(
                  'id'      => 'sp_news_letter_enable',
                  'type'    => 'switcher',
                  'default' => true
                  ),
              ),
          ),
      ),
    );
//==============  metabox for Sustainable news page news letter end =====================/
//==============  metabox for product and sales news letter start =====================/

$options[]      = array(
    'id'        => 'rws_pands_section',
    'title'     => 'Enable News Letter For This Page?',
    'post_type'     => 'page',
    'context'       => 'side',
    'priority'      => 'low',
    'sections'      => array(
      array(
          'name'    => 'ps_news_letter_enable',
          'title'   => __('Enable News Letter'),
          'fields'  => array(
              array(
                  'id'      => 'ps_news_letter_enable',
                  'type'    => 'switcher',
                  'default' => true
                  ),
              ),
          ),
      ),
    );


return $options;
}

add_action('cs_metabox_options', 'rws_metaboxes');