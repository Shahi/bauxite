<?php
get_header(); 

?>
<div id="content" class="site-content">
	<main id="main" class="site-main">

		<section class="default-section single-product-section">
			<div class="container">
				<div class="row">
					<div id="primary" class="col-9 content-area">
						<?php
						if ( have_posts() ):
							while ( have_posts() ) : the_post();

						?>
						<?php the_title('<h2>','</h2>'); ?>
						<?php 
						// if(has_post_thumbnail()) {
						// 	the_post_thumbnail();
						// }
						the_content(); ?>
						<?php
						endwhile; 
						endif;
						?>
					</div>
					<!-- #primary -->

				</div>
			</div>
		</section>
	</main>
</div>
<?php
get_footer (); 