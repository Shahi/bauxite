<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bs_bauxite');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*NsDJ-c+1,2J?>W@-%|Cy]$t,}#ukySbdA@[W[l+(kW,8ibpW.fwGGOxIDEXN<|+');
define('SECURE_AUTH_KEY',  'IO*40 @cE2u~hX+Z5qcj08;tAvC+Oe8zfD;w{gK-+FK`s}UjV6AT@F-QyBTG8:l&');
define('LOGGED_IN_KEY',    '$3B3n(KD*CtE;x0IfT3IP8O_>_*rMIoo#bj]N5MZx%_`Vp?(5tm>sX]DEHf)5XiB');
define('NONCE_KEY',        '<ZkN~E8XB[hZ+6gBs+tg/)AR}/jN{|8PXmw);#rZZe~y_0(w9Y+(TzL%^c:,=AE,');
define('AUTH_SALT',        ']@:<+HGt9<}dIv+&r@.lmo&T;8;/y#?>,,6SSjAC/DNXZmz;$N1nEB,&{n[R&]eF');
define('SECURE_AUTH_SALT', '0}QkU%1+`v~AF,1/[riW0XK=DMNDC5uqlQ8QE!t]E&OzmuW`MD|pF+&S?_9@x-Wr');
define('LOGGED_IN_SALT',   'V?e3bwI(3g7.^8z+CUJFu&.|S`TlYX.s@W<(o|f/i^ruEUW&G5D4hi)nsJ=g8qmK');
define('NONCE_SALT',       'SHKqlwH2Arj&ghG@U+9j%wpo3!^<!Al@@ytR^SaS+:t<({<]jgge9-V%fn_-t*KS');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('WP_MEMORY_LIMIT', '64MB');
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
